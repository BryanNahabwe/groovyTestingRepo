package ImportData

import fuzzycsv.FuzzyCSVTable

class RegexExpressions {
    static void main(def args) {
        def url = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ImportData\\katakwi.csv")
        def zomboCsv = FuzzyCSVTable.parseCsv(url.text).toMapList()
        def zomboVillageData = zomboCsv.each {
            it.each { k, v ->
                it[k] = v.toString().tokenize(" ")*.toLowerCase()*.capitalize().join(" ")
            }
        }

        def toFuzzy = FuzzyCSVTable.toCSV(zomboVillageData).printTable()
    }
}
