import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql
import sun.text.resources.cldr.yav.FormatData_yav

import java.text.DecimalFormat

import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class TestTest {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def queryBusinessPartners = """
                                    SELECT bpf.name_partner, bp.partner_id
                                    from _387_xxx_business_partner bpf
                                             inner join business_partner bp on bpf.openxdata_form_data_id =bp.openxdata_form_data_id
                                    order by bpf.name_partner
                                    """
        // MILESTONE -------------- No. of farmers registered
        def queryFarmersRegistered = """
                                    select name_partner, partner_id, count(*) as total,
                                           'No. of farmers registered' as name
                                    from _393_xxx_farmer_registration_form
                                    group by partner_id
                                    """
//        def farmersRegistered = AppHolder.withMisSqlNonTx { rows(queryFarmersRegistered) }
        def mapLocationData = []
        def business_partners_to_fuzzy = FuzzyCSVTable.toCSV(sql, queryBusinessPartners).printTable()
        def farmersRegistered = FuzzyCSVTable.toCSV(sql, queryFarmersRegistered).toMapList()

        if (farmersRegistered) {
            def business_partners_to_fuzzy_inside = business_partners_to_fuzzy.delete('name', 'total')
            def farmersRegistered_to_fuzzy = FuzzyCSVTable.toCSV(farmersRegistered)
                    .fullJoin(business_partners_to_fuzzy_inside, 'partner_id')
                    .modify { set { it.total = 0 } where { it.total in [null] } }
                    .modify { set { it.name = 'No. of farmers registered' } where { it.name in [null] } }.toMapList()
            farmersRegistered_to_fuzzy.each { mapLocationData << it }
        } else {
            def partnersWithEmptyValues = business_partners_to_fuzzy.delete('name','total')
            def ifEmpty = partnersWithEmptyValues.addColumn(fx('total') { return 0 })
                    .addColumn(fx('name') { return 'No. of farmers registered' }).toMapList()
            ifEmpty.each { mapLocationData << it }
        }

        def ghghg = FuzzyCSVTable.toCSV(mapLocationData).printTable()

    }
}
