package TestSupplyChain

import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonSlurper
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class SupplyChain {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "", 'com.mysql.jdbc.Driver')
        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.indicator == "Carry out capacity building of communities through mentoring and coaching of the of the different groups"
        }).printTable()


        def query = """ 
                    select name_of_activity,partner,count(*) as actual_value
                    from `_359_xxx_activity_form`
                    where name_of_activity = 'mentor'
                    group by partner;
                    """


        def queryData = FuzzyCSVTable.toCSV(sql, query.toString())
        def sum_total = (queryData.sum { it.actual_value } != null ) ? queryData.sum { it.actual_value } : 0

        def score_total = (targetData.sum { it.score } != null ) ? targetData.sum { it.score } : 0
        println(score_total)

        def addTargetValue = queryData.addColumn(fx ('target_value'){
            return score_total as Integer
        }).printTable()


        def total_targets = (addTargetValue.sum { it.target_value } == null ) ? score_total : addTargetValue.toMapList().sum { it.target_value }
        println(total_targets)


        def modifyDataToExport = addTargetValue.modify {
            set {
                it.name_of_activity = "Carry out capacity building of communities through mentoring and coaching of the of the different groups"
            }
            where {
                it.name_of_activity in ['mentor']
            }
        }

        //Export Data
        def exportCsvData = []
        exportCsvData << ["Activity", "Partner", "Actual Value", "Target Value"]
        modifyDataToExport.each { d ->
            exportCsvData << [d.name_of_activity, d.partner, d.actual_value, d.target_value]
        }



        def collectData
        def targetScores = []
        def actualScores = []
        def categories  = []
        addTargetValue.each {
            def target_value = it.target_value as Integer
            targetScores << target_value
            actualScores << it.actual_value
            categories << it.partner
        }
        collectData  = [categories : categories,series: [[name : 'Target', data: targetScores], [name : 'Scores',data : actualScores]]]
        //def runningTotal = 0
        def innerraduis = 0
        def raduis = 0

        targetData.addColumn(fx('raduis'){
            if (it.idx() == 1) {
                raduis = 62
            } else {
                raduis += 16
            }
            return raduis
        },fx('innerRadius') {
            if (it.idx() == 1) {
                innerraduis = 47
            } else {
                innerraduis += 16
            }
            return innerraduis
        })
        def finalResults = []
        def finalTargets
        targetData.toMapList().each { item ->
            def indicator = item.indicator
            def target_year = item.target
            def partnerData = collectData
            def data = ["radius"     : item.raduis + "%",
                        "innerRadius": item.innerRadius + "%",
                        "y"          : (total_targets == 0 || sum_total == 0) ? 0 : (sum_total as Integer) / (total_targets as Integer) * 100,
                        "actual"     : sum_total,
                        "target"     : total_targets]
            finalResults << ['indicator': indicator, 'targets': target_year, 'data': data,'partnerData': partnerData,'exportCsvData':exportCsvData]
        }
        finalTargets = ["sum": 0, "finalTargets": finalResults ]
        println(finalTargets)
    }
}
