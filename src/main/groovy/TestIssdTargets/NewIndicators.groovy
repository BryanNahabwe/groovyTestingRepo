package TestIssdTargets

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class NewIndicators {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Additional agricultural production of grain equivalent as a result of using quality seed (MT)"
        }).printTable()

        def query = """ 
                        select
                               p.name,
                               p.`__code` as code,
                               p.zone,
                               p.district,
                               replace(p.season,'_','') as season,
                               substring(p.season, 2, 4)                                        as year,
                               ch.crop,
                               (case
                                  when ch.crop = 'rice' then rice_yield
                                  when ch.crop = 'bush' then bushbean_yield
                                  when ch.crop = 'climb' then climb_yield
                                  when ch.crop = 'groundnut' then groundnut_yield
                                  when ch.crop = 'cassava' then cassava_yield
                                  when ch.crop = 'simsim' then simsim_yield
                                  when ch.crop = 'soybean' then soybean_yield
                                  when ch.crop = 'sorghum' then sorghum_yield
                                  when ch.crop = 'fingermillet' then fingermillet_yield
                                  when ch.crop = 'greengram' then greengram_yield
                                  when ch.crop = 'cowpea' then cowpea_yield
                                  when ch.crop = 'pigeon' then pigeonpea_yield
                                   end   ) as yield_difference
                        from `_16_xxx_yield_verification_form` p
                               inner join (select id, SUBSTRING_INDEX(SUBSTRING_INDEX(`_16_xxx_yield_verification_form`.crops, ' ', numbers.n),
                                                                      ' ', -1) as crop
                                           from (select 1 n
                                                 union all
                                                 select 2
                                                 union all select 3
                                                 union all
                                                 select 4
                                                 union all select 5
                                                 union all select 6
                                                 union all select 7
                                                 union all select 8
                                                 union all select 9
                                                 union all select 10
                                                 union all select 11
                                                 union all select 12
                                                 union all select 13
                                                 union all select 14
                                                 union all select 15
                                                 union all select 16) numbers
                                                  INNER JOIN `_16_xxx_yield_verification_form` on CHAR_LENGTH(`_16_xxx_yield_verification_form`.crops)
                                                                                                     -
                                                                                                   CHAR_LENGTH(REPLACE(`_16_xxx_yield_verification_form`.crops, ' ', '')) >=
                                                                                                   numbers.n - 1
                                           order by crops, n) ch on p.Id = ch.Id
                        where stage_of_demo = 'analysis'
                        """

        def query2 = """
                        select
                               p.name as name,
                               p.`__code` as code,
                               lpf.zone,
                               p.district,
                               replace(p.season,'_','') as season,
                               substring(p.season,2,4)              as year,
                               p.crop as crop,
                               replace(concat_ws('', f.variety_rice, f.variety_beans, f.variety_climb, f.variety_groundnut, f.variety_cassava,
                                                 f.variety_simsim, f.variety_soybean,
                                                 f.variety_sorghum, f.variety_irish_potato, f.variety_maize, f.variety_sweet_potato, f.variety_finger_millet,
                                                 f.variety_green_gram, f.variety_pasture,
                                                 f.variety_cowpea, f.variety_pigeon),'_',' ') as variety,
                               case p.crop
                                 when 'cassava' then f.sell_prod_bag*40
                                 when 'irishpotato' then f.sell_prod_bag*80
                                 when 'groundnut' then f.sell_prod_bag*42
                                 when 'sweetpotato' then f.sell_prod_bag*30
                                 else f.sell_prod_kg
                                   end as quantity
                        from `_21_xxx_sales_form` p
                               inner join `_21_sales_` f on p.Id = f.parentId
                               inner join `_3_xxx_lsb_profile_form` lpf on p.`__code` = lpf.`__code`
                        union all
                        select name,
                               `__code`                                     as code,
                               (case
                                 when zone = 'South West (Ankole)' then 'ankole'
                                 when zone = 'Western Highlands (Rwenzori)' then 'rwenzori'
                                 else lower(zone)
                                 end) as zone,
                               district,
                               lower(replace(season, '_', ''))                    as season,
                                case
         when locate('_', season) then substring(replace(season, '_', ''), 1, 4)
         else substring(season, 1, 4)
           end                                      as year,
                               case 
                                 when crop = 'Bush beans' then 'beans'
                                 when crop = 'Climb beans' then 'climb'
                                 when crop = 'Climbing beans' then 'climb'
                                 when crop = 'Finger millet' then 'fingermillet'
                                 when crop = 'Pasture' then 'pasture'
                                 when crop = 'Cowpeas' then 'cowpea'
                                 when crop = 'Cassava' then 'cassava'
                                 when crop = 'Sweet potato' then 'sweetpotato'
                                 when crop = 'Simsim' then 'simsim'
                                 when crop = 'Pigeon pea' then 'pigeon'
                                 when crop = 'Pigeon Pea' then 'pigeon'
                                 when crop = 'Green gram' then 'greengram'
                                 when crop = 'Groundnut' then 'groundnut'
                                 when crop = 'Potato' then 'irishpotato'
                                 when crop = 'Rice' then 'rice'
                                 when crop = 'Sweetpotato' then 'sweetpotato'
                                 when crop = 'Soybean' then 'soybean'
                                 when crop = 'Sorghum' then 'sorghum'
                                 
                                 else crop
                                   end as crop,
                                lower(replace(concat_ws('', variety_rice, variety_beans, variety_groundnuts, variety_cassava,
                                     variety_simsim, variety_soybean,
                                     variety_sorghum, variety_irish_potato, variety_sweet_potato, variety_finger_millet,
                                     variety_green_gram, variety_pasture,
                                     variety_cowpea), '_', ' ')) as variety,
                               quantity_sold as quantity
                        from `_55_xxx_historical_production_data`;
                    """


        def query3 = """
select ch.crop as crop,
       (case
          when ch.crop = 'rice' then rice_seedrate
          when ch.crop = 'beans' then bean_seedrate
          when ch.crop = 'bush' then bean_seedrate
          when ch.crop = 'climb' then bean_seedrate
          when ch.crop = 'groundnut' then groundnut_seedrate
          when ch.crop = 'cassava' then cassava_seedrate
          when ch.crop = 'simsim' then simsim_seedrate
          when ch.crop = 'soybean' then soybean_seedrate
          when ch.crop = 'sorghum' then sorghum_seedrate
          when ch.crop = 'irishpotato' then irishpotato_seedrate
          when ch.crop = 'sweetpotato' then sweetpotato_seedrate
          when ch.crop = 'fingermillet' then millet_seedrate
          when ch.crop = 'greengram' then greengram_seedrate
          when ch.crop = 'pasture' then pasture_seedrate
          when ch.crop = 'cowpea' then cowpea_seedrate
          when ch.crop = 'pigeon' then pigeon_seedrate
        end   ) as seedrate

from `_54_xxx_logframe_indicators_form` p
inner join (select id, SUBSTRING_INDEX(SUBSTRING_INDEX(`_54_xxx_logframe_indicators_form`.crop, ' ', numbers.n),
                                       ' ', -1) as crop
            from (select 1 n
                  union all
                  select 2
                  union all select 3
                  union all
                  select 4
                  union all select 5
                  union all select 6
                  union all select 7
                  union all select 8
                  union all select 9
                  union all select 10
                  union all select 11
                  union all select 12
                  union all select 13
                  union all select 14
                  union all select 15
                  union all select 16) numbers
                   INNER JOIN `_54_xxx_logframe_indicators_form` on CHAR_LENGTH(`_54_xxx_logframe_indicators_form`.crop)
                                                                      -
                                                                    CHAR_LENGTH(REPLACE(`_54_xxx_logframe_indicators_form`.crop, ' ', '')) >=
                                                                    numbers.n - 1
            order by crop, n) ch on p.Id = ch.Id

"""

        // Crop Group CSV
        def cropGroupCsv = new ReadCropGroup()
        def cropGroupData = cropGroupCsv.getCropGroup()


        def fuzzyData1 = FuzzyCSVTable.toCSV(sql, query).printTable()
        def fuzzyData2 = FuzzyCSVTable.toCSV(sql, query2)
        def fuzzyData3 = FuzzyCSVTable.toCSV(sql, query3)
        def joinAreaPlantedTables = fuzzyData2.join(fuzzyData3,'crop')
        def addCropGroup = joinAreaPlantedTables.join(cropGroupData,'crop')
        def calc = addCropGroup.addColumn(fx('area_planted') {
            (it.quantity == null || it.quantity == 0) ? 0 : (it.quantity as Float) / (it.seedrate as Float)
        })
        def filterNulls = calc.filter {it.area_planted != 0 && it.area_planted != null }
        def areaPlantedPerCropGroup = filterNulls.autoAggregate('year','group',sum('area_planted').az('area_planted')).printTable()


        def joinPerCropGroup = fuzzyData1.join(cropGroupData,'crop')
        def yieldDifferencePerCropGroup = joinPerCropGroup.autoAggregate('year','group',sum('yield_difference').az('yield_difference')).printTable()





    }
}
