package TestSimoshi

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class TestSchoolDates {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/simoshimis', 'root', "", 'com.mysql.jdbc.Driver')

        def querySchools = """
                            select school_name,DATE_FORMAT(min(use_date), '%Y-%m-%d') as date
                            from `_15_xxx_kitchen_monitoring_form`
                            where school_name is  not null and use_date is not null
                            group by school_name
                            order by  school_name
                            """
        def schoolsData = []
        def school = FuzzyCSVTable.toCSV(sql, querySchools)
        school.each {
            def eachSchool = it.school_name
            def schoolOpDateValue = it.date
            println(eachSchool + " : " + schoolOpDateValue)
        }
    }
}
