package TestIssdTargets

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.count
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.get

class lsb_partner_support  {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Amount of food produced that prevents and treats vitamin A deficiencies in Uganda (MT)"
        }).printTable()


        def query = """
select
       p.name as name,
       p.`__code` as code,
       lpf.zone,
       p.district,
       replace(p.season,'_','') as season,
       substring(p.season,2,4)              as year,
       p.crop as crop,
       replace(concat_ws('', f.variety_rice, f.variety_beans, f.variety_climb, f.variety_groundnut, f.variety_cassava,
                         f.variety_simsim, f.variety_soybean,
                         f.variety_sorghum, f.variety_irish_potato, f.variety_maize, f.variety_sweet_potato, f.variety_finger_millet,
                         f.variety_green_gram, f.variety_pasture,
                         f.variety_cowpea, f.variety_pigeon),'_',' ') as variety,
       case p.crop
         when 'cassava' then f.sell_prod_bag*40
         when 'irishpotato' then f.sell_prod_bag*80
         when 'groundnut' then f.sell_prod_bag*42
         when 'sweetpotato' then f.sell_prod_bag*30
         else f.sell_prod_kg
           end as quantity
from `_21_xxx_sales_form` p
       inner join `_21_sales_` f on p.Id = f.parentId
       inner join `_3_xxx_lsb_profile_form` lpf on p.`__code` = lpf.`__code`
union all
select name,
       `__code`                                     as code,
       zone,
       district,
       replace(season, '_', '')                    as season,
       case
         when locate('_', season) then substring(replace(season, '_', ''), 1, 4)
         else substring(season, 1, 4)
           end                                      as year,
       case
         when crop = 'Bush beans' then 'beans'
         when crop = 'Climb beans' then 'climb'
         when crop = 'Cassava' then 'cassava'
         when crop = 'Simsim' then 'simsim'
         when crop = 'Groundnut' then 'groundnut'
         when crop = 'Potato' then 'potato'
         when crop = 'Rice' then 'rice'
         when crop = 'Sweetpotato' then 'sweetpotato'
         when crop = 'Soybean' then 'soybean'
         else crop
           end as crop,
       lower(replace(concat_ws('', variety_rice, variety_beans, variety_groundnuts, variety_cassava,
                         variety_simsim, variety_soybean,
                         variety_sorghum, variety_irish_potato, variety_sweet_potato, variety_finger_millet,
                         variety_green_gram, variety_pasture,
                         variety_cowpea), '_', ' ')) as variety,
       quantity_sold as quantity
from `_55_xxx_historical_production_data`;
"""

//Read CSV files

        // Seed Rates
        def seedCsv = new ReadSeedRates()
        def seedData = seedCsv.getSeedRates()

        // Vitamin A

        def vitaminCsv = new ReadVitaminA()
        def vitaminData = vitaminCsv.getVitaminA().printTable()


        // Yield Rates

        def yieldCsv = new ReadYields()
        def yieldData = yieldCsv.getYields()

        def fuuzyData = FuzzyCSVTable.toCSV(sql, query).printTable()

        def qua_seed_sold_iron_rich  = fuuzyData.join(vitaminData,'variety').printTable()
        def seed_rate_beans = qua_seed_sold_iron_rich.join(seedData,'crop')
        def yield_acre_beans  = seed_rate_beans.join(yieldData,'crop')

        def calc = yield_acre_beans.addColumn(fx('total') {
            (it.quantity == 0 || it.rate == 0 || it.yield == 0 ) ? 0 : ((it.quantity as Float) * (it.rate as Float) * (it.yield as Float) / 1000)
        }).printTable()

        def joinByYears = calc.autoAggregate('year',sum('total').az('total'))
        def targets = targetData.join(joinByYears,'year').printTable()

        def runningTotal = 0
        def innerRadius = 0
        def raduis = 0

        targets.addColumn(fx('running_tt') {
            if (it.idx() == 0) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        }, fx('raduis') {
            if (it.idx() == 1) {
                raduis = 62
            } else {
                raduis += 16
            }
            return raduis
        }, fx('innerRadius') {
            if (it.idx() == 1) {
                innerRadius = 47
            } else {
                innerRadius += 16
            }
            return innerRadius
        })

        def finalResults = []
        def finalTargets
        targets.toMapList().each { item ->
            def outcome = item.Outcome
            def indicator = item.Indicator
            def target_year =  item.year
            def data = ["radius"     : item.raduis + "%",
                        "innerRadius": item.innerRadius + "%",
                        "y"          : ( item.target_value as Integer == 0 ) ? 0 : ((item.running_tt as Integer) / (item.target_value as Integer)) * 100,
                        "actual"     : item.running_tt,
                        "target"     : item.target_value as Integer]
            finalResults << ['outcome': outcome, 'indicator': indicator, 'targets': target_year, 'data': data]
        }

        println(finalResults)

    }
}
