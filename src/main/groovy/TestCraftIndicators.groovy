import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestCraftIndicators {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def queryData = """
                        select *
                        from craft_indicators_table
                        where indicator LIKE '%Average income per  small holder farmers%'
                          and business_champion LIKE '%%'
                          and country LIKE '%%'
                          and gender LIKE '%%'
                          and crop LIKE '%%'
                        """
        def queryCurrency = """
                            select country, (rate + 0.0) as rate
                            from craft_currency_update
                            """

        def queryPpln = """
                        select name_partner,count(distinct farmer_id) as total
                        from craft_farmers_reached
                        where name_partner != ''
                        group by name_partner;
                        """

        def querySamplePpln = """
                        select replace(name_partner, '_', ' ') as business_champion, period, count(distinct Id) as total_surveyed
                        from _414_xxx_farmer_questionnaire
                        group by replace(name_partner, '_', ' '), period
                        """
        def ppln_data = FuzzyCSVTable.toCSV(sql, querySamplePpln).toMapList().collect {
            [business_champion: it.business_champion,
             period           : it.period.toString(),
             total_surveyed   : it.total_surveyed
            ]
        }

        def totalPpln = FuzzyCSVTable.toCSV(sql, queryPpln).toMapList()

        def finalData = []
        // Convert each farmers income into euro
        def total_reached = 0
        def dataToFuzzy = FuzzyCSVTable.toCSV(sql , queryData).join(FuzzyCSVTable.toCSV(sql, queryCurrency), 'country')
                .addColumn(fx('income_euro') {
                    it.total * it.rate
                }).toMapList()
        def eachPartnerData = []
        // Group farmers according to their business champions
        dataToFuzzy.groupBy { business -> business.business_champion }.collect {
            def name_partner = it.key
            def total_farmers_reached_per_partner = 0
            def totalValue = totalPpln.find { it['name_partner'] == name_partner }?.get("total") as Integer
            if (totalValue) {
                total_farmers_reached_per_partner = totalValue
            }
            else {
                total_farmers_reached_per_partner = 0
            }
            total_reached = total_reached + total_farmers_reached_per_partner
            // Get Total farmers reached per business champion
            def valuesToFuzzy = FuzzyCSVTable.toCSV(it.value)
            def samplePplnData = ppln_data
            def samplePpln = FuzzyCSVTable.toCSV(samplePplnData).printTable()
            // Group farmers by period and get the average income per period
            // Multiply the average income per period with the total number of farmers reached in the same business champion
            def autoAggregatedData = valuesToFuzzy.autoAggregate('business_champion', 'period', sum('income_euro').az('total_indicator')).printTable()
                    .join(samplePpln, 'business_champion', 'period')
                    .toMapList()
            autoAggregatedData.each {
                eachPartnerData << it
            }
        }
        println(eachPartnerData)
        // Return final data by sorting the period to start with Baseline
        finalData = FuzzyCSVTable.toCSV(eachPartnerData).printTable().autoAggregate('period',
                sum('total_indicator').az('total_indicator'),
                sum('total_surveyed').az('total_surveyed'))
                .addColumn(fx('total_reached') {
                    return total_reached
                })
                .addColumn(fx('total') {
                    def calculation = (((it.total_indicator ? it.total_indicator : 0) / (it.total_surveyed ? it.total_surveyed : 0)) * (it.total_reached ? it.total_reached : 0))
                    return calculation
                })
                .addColumn(fx('flag') {
                    return 'average'
                })
                .sort { "$it.period" }.printTable().toMapList()
        println(finalData)
    }
}
