package ISSD

import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql


import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx


class TestIssd {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')



        def query = """
select
       p.name as name,
       p.`__code` as code,
       lpf.zone,
       p.district,
       replace(p.season,'_',' ') as season,
       p.crop as crop,
       replace(concat_ws('', f.variety_rice, f.variety_beans, f.variety_climb, f.variety_groundnut, f.variety_cassava,
                         f.variety_simsim, f.variety_soybean,
                         f.variety_sorghum, f.variety_irish_potato, f.variety_maize, f.variety_sweet_potato, f.variety_finger_millet,
                         f.variety_green_gram, f.variety_pasture,
                         f.variety_cowpea, f.variety_pigeon),'_',' ') as variety,
       case p.crop
         when 'cassava' then f.sell_prod_bag*40
         when 'irishpotato' then f.sell_prod_bag*80
         when 'groundnut' then f.sell_prod_bag*42
         when 'sweetpotato' then f.sell_prod_bag*30
         else f.sell_prod_kg
           end as quantity
from `_21_xxx_sales_form` p
       inner join `_21_sales_` f on p.Id = f.parentId
       inner join `_3_xxx_lsb_profile_form` lpf on p.`__code` = lpf.`__code`
"""

//Read CSV files

        // Seed Rates
        def seedCsv = new ReadSeedRates()
        def seedData = seedCsv.getSeedRates().printTable()


        //  Iron Rich
        def ironRichCsv = new ReadIronRich()
        def ironRichData = ironRichCsv.getIronRich().printTable()

        // Yield Rates

        def yieldCsv = new ReadYields()
        def yieldData = yieldCsv.getYields().printTable()

        def fuuzyData = FuzzyCSVTable.toCSV(sql, query).printTable()

        def qua_seed_sold_iron_rich  = fuuzyData.join(ironRichData,'variety')
        def seed_rate_beans = qua_seed_sold_iron_rich.join(seedData,'crop')
        def yield_acre_beans  = seed_rate_beans.join(yieldData,'crop')

        def calc = yield_acre_beans.addColumn(fx('total') {
            (it.quantity == 0 || it.rate == 0 || it.yield == 0 ) ? 0 : (((it.quantity as Float) * (it.rate as Float) * (it.yield as Float)) / 1000)
        }).printTable()

        def sum_total = calc.sum { it.total }
    }
}
