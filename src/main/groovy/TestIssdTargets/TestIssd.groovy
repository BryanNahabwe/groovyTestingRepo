package TestIssdTargets
import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.count
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestIssd {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')


        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Difference in productivity from using home saved seed and quality seed for roots and tubers (yield benefit quality seed in Kg/hectare)"
        })

        def query = """
select
       p.name,
       p.`__code` as code,
       p.zone,
       p.district,
       p.season,
       substring(p.season, 2, 4)                                        as year,
       ch.crop,
       (case
          when ch.crop = 'rice' then rice_yield
          when ch.crop = 'bush' then bushbean_yield
          when ch.crop = 'climb' then climb_yield
          when ch.crop = 'groundnut' then groundnut_yield
          when ch.crop = 'cassava' then cassava_yield
          when ch.crop = 'simsim' then simsim_yield
          when ch.crop = 'soybean' then soybean_yield
          when ch.crop = 'sorghum' then sorghum_yield
          when ch.crop = 'fingermillet' then fingermillet_yield
          when ch.crop = 'greengram' then greengram_yield
          when ch.crop = 'cowpea' then cowpea_yield
          when ch.crop = 'pigeon' then pigeonpea_yield
           end   ) as yield_difference
from `_16_xxx_yield_verification_form` p
       inner join (select id, SUBSTRING_INDEX(SUBSTRING_INDEX(`_16_xxx_yield_verification_form`.crops, ' ', numbers.n),
                                              ' ', -1) as crop
                   from (select 1 n
                         union all
                         select 2
                         union all select 3
                         union all
                         select 4
                         union all select 5
                         union all select 6
                         union all select 7
                         union all select 8
                         union all select 9
                         union all select 10
                         union all select 11
                         union all select 12
                         union all select 13
                         union all select 14
                         union all select 15
                         union all select 16) numbers
                          INNER JOIN `_16_xxx_yield_verification_form` on CHAR_LENGTH(`_16_xxx_yield_verification_form`.crops)
                                                                             -
                                                                           CHAR_LENGTH(REPLACE(`_16_xxx_yield_verification_form`.crops, ' ', '')) >=
                                                                           numbers.n - 1
                   order by crops, n) ch on p.Id = ch.Id
where stage_of_demo = 'analysis'
"""

//Read CSV files

        // Crop Group CSV
        def cropGroupCsv = new ReadCropGroup()
        def cropGroupData = cropGroupCsv.getCropGroup()


        def fuuzyData = FuzzyCSVTable.toCSV(sql, query).printTable()
        def crop_group = fuuzyData.join(cropGroupData, 'crop').printTable()
        def filterGroup = crop_group.filter { it.group == 'tubers' }.printTable()
        def filterZeros = filterGroup.filter { it.yield_difference != 0 && it.yield_difference != null }.printTable()

        def groupBySeason = filterZeros.autoAggregate('zone','district','season','year', sum('yield_difference').az('total_season'), count('yield_difference').az('count'))
        def averageRecords = groupBySeason.addColumn(fx('total') {
            (it.total_season == 0 || it.count == 0 || it.total_season == 'null' || it.count == 'null') ? 0 : (((it.total_season as Float) / (it.count as Float)) * 2.47)
        },fx('name') { return 'null'},fx('code'){ return 'null'},fx('crop') { return 'null'},fx('variety') {return 'null'})

        def joinByYears = averageRecords.autoAggregate('year', sum('total').az('total'))
        def targets = targetData.join(joinByYears, 'year')

        def runningTotal = 0
        def innerRadius = 0
        def raduis = 0

        targets.addColumn(fx('running_tt') {
            if (it.idx() == 0) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        }, fx('raduis') {
            if (it.idx() == 1) {
                raduis = 62
            } else {
                raduis += 16
            }
            return raduis
        }, fx('innerRadius') {
            if (it.idx() == 1) {
                innerRadius = 47
            } else {
                innerRadius += 16
            }
            return innerRadius
        })

        def finalResults = []
        def finalTargets
        targets.toMapList().each { item ->
            def outcome = item.Outcome
            def indicator = item.Indicator
            def target_year = item.year
            def data = ["radius": item.raduis + "%",
                        "innerRadius": item.innerRadius + "%",
                        "y": (item.target_value as Integer == 0) ? 0 : ((item.running_tt as Integer) / (item.target_value as Integer)) * 100,
                        "actual": item.running_tt,
                        "target": item.target_value as Integer]
            finalResults << ['outcome': outcome, 'indicator': indicator, 'targets': target_year, 'data': data]
        }
        def filterZerosInBubble = averageRecords.filter{it.total != 0}
        def bubbleData = filterZerosInBubble.addColumn(fx('indicatorGroup') {
            return "Smallholder farmers increased productivity from use of quality seed for crop production"
        },
                fx('indicator') {
                    return "Difference in productivity from using home saved seed and quality seed for roots and tubers (yield benefit quality seed in Kg/hectare)"
                }).toMapList()
        finalTargets = ["trendData": bubbleData,"finalTargets": finalResults]

    }
}
