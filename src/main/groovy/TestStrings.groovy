class TestStrings {
    static void main(def args){
        def a = [[name:"bryan",crop:"sim,ground,nuts"],[name:"nahabwe",crop:"fresh,honey,lemon"]]
        def data = a.collect {
            def c = it.crop.toString()
            def b = replaceLongName(c)
            println(b)
            [
                    name: it.name,
                    crop: b
            ]
        }

        println(data)

    }
    static def replaceLongName(crop){
        def split = crop.toString().split(',').toList()
        def newArray = []
        def newCrop
        def changeNames = split.collect {
            def oldCrop = it

            switch (oldCrop){
                case ~/.*ground.*/:
                    newCrop = oldCrop.replace('ground','Groundnut')
                    break
                case ~/.*nuts.*/:
                    newCrop = oldCrop.replace('nuts','Nuts')
                    break
                default:
                    newCrop = oldCrop
                    break
            }
        }

        newArray << newCrop
        def asJoined = changeNames.join(",")
        return  asJoined
    }
}
