package Nutec

import fuzzycsv.FuzzyCSVTable

import java.text.DecimalFormat

import static fuzzycsv.FuzzyStaticApi.*

import groovy.sql.Sql

class FarmerRecordBook {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.jdbc.Driver')

        def query = """
                    select `__code`                    as code,
                           farmer_name,
                           date as date,
                           district,
                           partner,
                           crop_grown as crop,
                           case
                             when date(date) between date('2018-08-01') and date('2018-12-31') then 'season_a_2019'
                             when date(date) between date('2019-01-01') and date('2019-03-31') then 'season_b_2019'
                             else 'no season'
                             end as period,
                           case
                             when date(date) between date('2018-08-01') and date('2018-12-31') then '2019'
                             when date(date) between date('2019-01-01') and date('2019-03-31') then '2019'
                             else 'no year'
                             end as year,
                           (sum(ifnull(sunflower22, 0)) +
                            sum(ifnull(soybeans22, 0)) +
                            sum(ifnull(groundnuts22, 0)) +
                            sum(ifnull(sorghum22, 0)) +
                            sum(ifnull(rice22, 0)) +
                            sum(ifnull(beans22, 0)) +
                            sum(ifnull(cassava22, 0)) +
                            sum(ifnull(chia22, 0)) +
                            sum(ifnull(maize22, 0)) +
                            sum(ifnull(simsim22, 0)) +
                            sum(ifnull(cotton22, 0)) +
                            sum(ifnull(tobacco22, 0))) as income
                    from `_179_xxx_farmers_record_book`
                     group by  farmer_name, `__code`, district, parish ,partner,crop_grown,date,agent,name_interviewer,unique_id
                    order by farmer_name
                    """

        def query1 = """
                        select(sum(ifnull(sunflower_buyers_males, 0)) +
                               sum(ifnull(sunflower_buyers_females, 0)) +
                               sum(ifnull(soy_bought_male, 0)) +
                               sum(ifnull(soy_bought_female, 0)) +
                               sum(ifnull(sorghum_bought_male, 0)) +
                               sum(ifnull(sorghum_bought_female, 0)) +
                               sum(ifnull(cotton_access_male, 0)) +
                               sum(ifnull(cotton_access_female, 0)) +
                               sum(ifnull(sesame_bought_male, 0)) +
                               sum(ifnull(sesame_bought_female, 0)) +
                               sum(ifnull(sunflowergrain_buyers_females, 0)) +
                               sum(ifnull(sunflowergrain_buyers_males, 0)) +
                               sum(ifnull(seed_cotton__female, 0)) +
                               sum(ifnull(seed_cotton_male, 0)))              as sum_tt
                        from `_181_xxx_kpi_monitoringtool` 
                                            union all
                        select
                               sum(ifnull(non_youth, 0))  as sum_tt
                        from `_182_xxx_lograme_trakers_form` 
                        """


        def fuzzy = FuzzyCSVTable.toCSV(sql, query)
        def fuzzy1 = FuzzyCSVTable.toCSV(sql, query1)
        def filterOut = fuzzy.filter { it.period != 'no season' }
        def autoSum = filterOut.autoAggregate('farmer_name', 'code', 'year', 'period', sum('income').az('total_income'))
        def distinctFarmers = autoSum.pivot('period', 'total_income', 'farmer_name', 'code', 'year')
        def replaceNulls = distinctFarmers.modify {
            set {
                it.season_a_2019 = 0
            }
            where {
                it.season_a_2019 in [null]
            }
        }.modify {
            set {
                it.season_b_2019 = 0
            }
            where {
                it.season_b_2019 in [null]
            }
        }

        def sumaplusb = replaceNulls.addColumn(fx('total_sum') {
            return (it.season_a_2019 + it.season_b_2019)
        }, fx('baseline_income') { return 0 })

        def changeInIncome = sumaplusb.addColumn(fx('change_in_income') { return (it.total_sum - it.baseline_income) })
        def filterFarmerswithzero = changeInIncome.filter { it.change_in_income > 0 }
        def farmerswithincome = filterFarmerswithzero.count { it.code }
        def totalfarmers = distinctFarmers.count { it.code }
        def sum_totals = fuzzy1.sum { it.sum_tt }
        def df = new DecimalFormat("#")
        def round =  (df.format(sum_totals)) as int
        println(round)
        println(farmerswithincome)
        println(totalfarmers)
        def calc = ((farmerswithincome / totalfarmers) * round) + 7235
        def df1 = new DecimalFormat("###,##0")
        def value = df1.format(calc)
        println(value)

////        def testing = fuzzy.printTable()
//        def filterOut = fuzzy.filter { it.period != 'no season' }
//        def autoSum = filterOut.autoAggregate('farmer_name', 'code', 'year', 'district', 'period', sum('income').az('total_income'))
//        def distinctFarmers = autoSum.pivot('period', 'total_income', 'farmer_name', 'code', 'year', 'district')
//        def replaceNulls = distinctFarmers.modify {
//            set {
//                it.season_a_2019 = 0
//            }
//            where {
//                it.season_a_2019 in [null]
//            }
//        }.modify {
//            set {
//                it.season_b_2019 = 0
//            }
//            where {
//                it.season_b_2019 in [null]
//            }
//        }
//
//        def sumaplusb = replaceNulls.addColumn(fx('total_sum') {
//            return (it.season_a_2019 + it.season_b_2019)
//        }, fx('baseline_income') { return 0 })
//
//        def changeInIncome = sumaplusb.addColumn(fx('change_in_income') { return (it.total_sum - it.baseline_income) })
//        def filterFarmerswithzero = changeInIncome.filter { it.change_in_income > 0 }
//        def allFarmersDistrict = filterFarmerswithzero.autoAggregate('district', count('code').az('no_of_farmers')).printTable()
//        def totalfarmers = distinctFarmers.count { it.code }
//        def sum_totals = fuzzy1.sum { it.sum_tt }
//        def perDistrictFarmerIncome = allFarmersDistrict.addColumn(fx('total') { return (it.no_of_farmers / totalfarmers) * sum_totals }).printTable()

    }
}
