package TestYoba

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import java.text.DecimalFormat

import static fuzzycsv.FuzzyStaticApi.sum

class ParentQuestionarie {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "", 'com.mysql.jdbc.Driver')
        def allData = []
        def df = new DecimalFormat("###,##0.0")
        def query =
                """
                select
                       pf.parent_name,
                       pf.gender_hhh,
                       pf.age,
                       pf.total_number_living_hh,
                       pf.females_below5,
                       pf.females_6_13,
                       pf.females_14_59,
                       pf.females_60_and_more,
                       pf.males_below5,
                       pf.males_6_13,
                       pf.males_14_59,
                       pf.males_60_and_more,
                       pf.education_hhh,
                       pf.toilet,
                       pf.drinking_water_source,
                       pf.make_water_safe_drinking,
                       jt.type
                from `_350_xxx_parent_form` pf
                   inner join (select replace(cd.school_name, '_', ' ') as school_name,
                                      cd.child_name,
                                      replace(sr.type, '_', ' ')        as type,
                                      cd.parentId
                               from `_350_child_details` cd
                                      inner join `_356_xxx_school_register` sr
                                        on replace(cd.school_name, '_', ' ') = sr.school_name) jt on pf.Id = jt.parentId
                """

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query)

        // % MALE HEADED HOUSEHOLDS
        def filterMilkGroup = fuzzyData.filter { it.type == 'milk school' }.printTable()
        def filterYoghurtGroup = fuzzyData.filter { it.type == 'yoghurt school' }

        def countAllMilk = filterMilkGroup.count { it.parent_name }
        def countAllYoghurt = filterYoghurtGroup.count { it.parent_name }

        def countMaleMilk = filterMilkGroup.filter { it.gender_hhh == 'male' }.count { it.parent_name }
        def countMaleYoghurt = filterYoghurtGroup.filter { it.gender_hhh == 'male' }.count { it.parent_name }

        def milkMalePercent = (countMaleMilk / countAllMilk) * 100
        def yoghurtMalePercent = (countMaleYoghurt / countAllYoghurt) * 100
        def malePercentText = "% Male headed households"
        def firstArray = [firstText: malePercentText,yoghurt: yoghurtMalePercent, milk: milkMalePercent]
        allData << firstArray

        // AVERAGE AGE OF THE HOUSEHOLD
        def milkAverageAge = filterMilkGroup.sum { (it.age == null)? 0 : it.age }  / countAllMilk
        def yoghurtAverageAge = filterYoghurtGroup.sum { (it.age == null)? 0 : it.age } / countAllYoghurt
        def averageAgeText = 'Average age of the household head'
        def secondArray = [firstText: averageAgeText,yoghurt: yoghurtAverageAge, milk: milkAverageAge]
        allData << secondArray


        //PERCENTAGE HOUSEHOLD HEAD NOT HAVING FORMAL EDUCATION
        def countEducationNoFormalMilk = filterMilkGroup.filter { it.education_hhh == 'no_formal_education' }.count { it.parent_name }
        def countEducationNoFormalYoghurt = filterYoghurtGroup.filter { it.education_hhh == 'no_formal_education' }.count { it.parent_name }
        def milkEducationNoFormalPercent = (countEducationNoFormalMilk / countAllMilk) * 100
        def yoghurtEducationNoFormalPercent = (countEducationNoFormalYoghurt / countAllYoghurt) * 100
        def educationNoFormalPercentText = "Percentage household head not having formal education"
        def array12 = [firstText: educationNoFormalPercentText,yoghurt: df.format(yoghurtEducationNoFormalPercent), milk: df.format(milkEducationNoFormalPercent)]
        allData << array12

        //PERCENTAGE HOUSEHOLD HEAD  HAVING PRIMARY LEVEL
        def countEducationPrimaryLevelMilk = filterMilkGroup.filter { it.education_hhh == 'primary' }.count { it.parent_name }
        def countEducationPrimaryLevelYoghurt = filterYoghurtGroup.filter { it.education_hhh == 'primary' }.count { it.parent_name }
        def milkEducationPrimaryLevelPercent = (countEducationPrimaryLevelMilk / countAllMilk) * 100
        def yoghurtEducationPrimaryLevelPercent = (countEducationPrimaryLevelYoghurt / countAllYoghurt) * 100
        def educationPrimaryLevelPercentText = "Percentage household heads having primary level"
        def array13 = [firstText: educationPrimaryLevelPercentText,yoghurt: df.format(yoghurtEducationPrimaryLevelPercent), milk: df.format(milkEducationPrimaryLevelPercent)]
        allData << array13

        //PERCENTAGE HOUSEHOLD HEAD  HAVING SECONDARY LEVEL
        def countEducationSecondaryLevelMilk = filterMilkGroup.filter { it.education_hhh == 'secondary' }.count { it.parent_name }
        def countEducationSecondaryLevelYoghurt = filterYoghurtGroup.filter { it.education_hhh == 'secondary' }.count { it.parent_name }
        def milkEducationSecondaryLevelPercent = (countEducationSecondaryLevelMilk / countAllMilk) * 100
        def yoghurtEducationSecondaryLevelPercent = (countEducationSecondaryLevelYoghurt / countAllYoghurt) * 100
        def educationSecondaryLevelPercentText = "Percentage household heads having secondary level"
        def array14 = [firstText: educationSecondaryLevelPercentText,yoghurt: df.format(yoghurtEducationSecondaryLevelPercent), milk: df.format(milkEducationSecondaryLevelPercent)]
        allData << array14

        //PERCENTAGE HOUSEHOLD HEAD  HAVING TERTIARY LEVEL
        def countEducationTertiaryLevelMilk = filterMilkGroup.filter { it.education_hhh == 'tertiary' }.count { it.parent_name }
        def countEducationTertiaryLevelYoghurt = filterYoghurtGroup.filter { it.education_hhh == 'tertiary' }.count { it.parent_name }
        def milkEducationTertiaryLevelPercent = (countEducationTertiaryLevelMilk / countAllMilk) * 100
        def yoghurtEducationTertiaryLevelPercent = (countEducationTertiaryLevelYoghurt / countAllYoghurt) * 100
        def educationTertiaryLevelPercentText = "Percentage household heads having tertiary level"
        def array15 = [firstText: educationTertiaryLevelPercentText,yoghurt: df.format(yoghurtEducationTertiaryLevelPercent), milk: df.format(milkEducationTertiaryLevelPercent)]
        allData << array15

        //PERCENTAGE HOUSEHOLDS WITH TOILET FACILITY
        def countToiletFacilityMilk = filterMilkGroup.filter { it.toilet == 'yes' }.count { it.parent_name }
        def countToiletFacilityYoghurt = filterYoghurtGroup.filter { it.toilet == 'yes' }.count { it.parent_name }
        def milkToiletFacilityPercent = (countToiletFacilityMilk / countAllMilk) * 100
        def yoghurtToiletFacilityPercent = (countToiletFacilityYoghurt / countAllYoghurt) * 100
        def toiletFacilityPercentText = "Percentage households with toilet facility"
        def array16 = [firstText: toiletFacilityPercentText,yoghurt: df.format(yoghurtToiletFacilityPercent), milk: df.format(milkToiletFacilityPercent)]
        allData << array16

        //PERCENTAGE WATER OBTAINED FROM PROTECTED WELL OR SPRING
        def countWellOrSpringMilk = filterMilkGroup.filter { it.drinking_water_source == 'protected_well_or_spring' }.count { it.parent_name }
        def countWellOrSpringYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'protected_well_or_spring' }.count { it.parent_name }
        def milkWellOrSpringPercent = (countWellOrSpringMilk / countAllMilk) * 100
        def yoghurtWellOrSpringPercent = (countWellOrSpringYoghurt / countAllYoghurt) * 100
        def wellOrSpringPercentText = "Percentage water obtained from protected well or spring"
        def array17 = [firstText: wellOrSpringPercentText,yoghurt: df.format(yoghurtWellOrSpringPercent), milk: df.format(milkWellOrSpringPercent)]
        allData << array17

        //PERCENTAGE WATER OBTAINED FROM BOREHOLE
        def countBoreholeMilk = filterMilkGroup.filter { it.drinking_water_source == 'borehole' }.count { it.parent_name }
        def countBoreholeYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'borehole' }.count { it.parent_name }
        def milkBoreholePercent = (countBoreholeMilk / countAllMilk) * 100
        def yoghurtBoreholePercent = (countBoreholeYoghurt / countAllYoghurt) * 100
        def boreholePercentText = "Percentage water obtained from borehole"
        def array18 = [firstText: boreholePercentText,yoghurt: df.format(yoghurtBoreholePercent), milk: df.format(milkBoreholePercent)]
        allData << array18

        //PERCENTAGE WATER OBTAINED FROM OPEN WELL OR SPRING
        def countOpenWellOrSpringMilk = filterMilkGroup.filter { it.drinking_water_source == 'open_spring_or_well' }.count { it.parent_name }
        def countOpenWellOrSpringYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'open_spring_or_well' }.count { it.parent_name }
        def milkOpenWellOrSpringPercent = (countOpenWellOrSpringMilk / countAllMilk) * 100
        def yoghurtOpenWellOrSpringPercent = (countOpenWellOrSpringYoghurt / countAllYoghurt) * 100
        def openWellOrSpringPercentText = "Percentage water obtained from borehole"
        def array19 = [firstText: openWellOrSpringPercentText,yoghurt: df.format(yoghurtOpenWellOrSpringPercent), milk: df.format(milkOpenWellOrSpringPercent)]
        allData << array19

        //PERCENTAGE WATER OBTAINED FROM SURFACE
        def countSurfaceMilk = filterMilkGroup.filter { it.drinking_water_source == 'surface_water' }.count { it.parent_name }
        def countSurfaceYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'surface_water' }.count { it.parent_name }
        def milkSurfacePercent = (countSurfaceMilk / countAllMilk) * 100
        def yoghurtSurfacePercent = (countSurfaceYoghurt / countAllYoghurt) * 100
        def surfacePercentText = "Percentage water obtained from borehole"
        def array20 = [firstText: surfacePercentText,yoghurt: df.format(yoghurtSurfacePercent), milk: df.format(milkSurfacePercent)]
        allData << array20

        //PERCENTAGE WATER OBTAINED FROM RAIN WATER
        def countRainWaterMilk = filterMilkGroup.filter { it.drinking_water_source == 'rain_water' }.count { it.parent_name }
        def countRainWaterYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'rain_water' }.count { it.parent_name }
        def milkRainWaterPercent = (countRainWaterMilk / countAllMilk) * 100
        def yoghurtRainWaterPercent = (countRainWaterYoghurt / countAllYoghurt) * 100
        def rainWaterPercentText = "Percentage water obtained from borehole"
        def array21 = [firstText: rainWaterPercentText,yoghurt: df.format(yoghurtRainWaterPercent), milk: df.format(milkRainWaterPercent)]
        allData << array21

        //PERCENTAGE WATER OBTAINED FROM PIPPED WATER
        def countPipedWaterMilk = filterMilkGroup.filter { it.drinking_water_source == 'piped_water' }.count { it.parent_name }
        def countPipedWaterYoghurt = filterYoghurtGroup.filter { it.drinking_water_source == 'piped_water' }.count { it.parent_name }
        def milkPipedWaterPercent = (countPipedWaterMilk / countAllMilk) * 100
        def yoghurtPipedWaterPercent = (countPipedWaterYoghurt / countAllYoghurt) * 100
        def pipedWaterPercentText = "Percentage water obtained from borehole"
        def array22 = [firstText: pipedWaterPercentText,yoghurt: df.format(yoghurtPipedWaterPercent), milk: df.format(milkPipedWaterPercent)]
        allData << array22

        //PERCENTAGE BOILING WATER
        def countBoiledWaterMilk = filterMilkGroup.filter { it.make_water_safe_drinking == 'boil' }.count { it.parent_name }
        def countBoiledWaterYoghurt = filterYoghurtGroup.filter { it.make_water_safe_drinking == 'boil' }.count { it.parent_name }
        def milkBoiledWaterPercent = (countBoiledWaterMilk / countAllMilk) * 100
        def yoghurtBoiledWaterPercent = (countBoiledWaterYoghurt / countAllYoghurt) * 100
        def boiledWaterPercentText = "Percentage boiling water"
        def array23 = [firstText: boiledWaterPercentText,yoghurt: df.format(yoghurtBoiledWaterPercent), milk: df.format(milkBoiledWaterPercent)]
        allData << array23


        //PERCENTAGE ADDING BLEACH OR CHROLINE
        def countBleachOrChrolineMilk = filterMilkGroup.filter { it.make_water_safe_drinking == 'add_bleach_or_chroline' }.count { it.parent_name }
        def countBleachOrChrolineYoghurt = filterYoghurtGroup.filter { it.make_water_safe_drinking == 'add_bleach_or_chroline' }.count { it.parent_name }
        def milkBleachOrChrolinePercent = (countBleachOrChrolineMilk / countAllMilk) * 100
        def yoghurtBleachOrChrolinePercent = (countBleachOrChrolineYoghurt / countAllYoghurt) * 100
        def bleachOrChrolinePercentText = "Percentage adding bleach or chlorine"
        def array24 = [firstText: bleachOrChrolinePercentText,yoghurt: df.format(yoghurtBleachOrChrolinePercent), milk: df.format(milkBleachOrChrolinePercent)]
        allData << array24

        //PERCENTAGE STRAINING WITH A CLOTH
        def countClothMilk = filterMilkGroup.filter { it.make_water_safe_drinking == 'strain_with_cloth' }.count { it.parent_name }
        def countClothYoghurt = filterYoghurtGroup.filter { it.make_water_safe_drinking == 'add_bleach_or_chroline' }.count { it.parent_name }
        def milkClothPercent = (countClothMilk / countAllMilk) * 100
        def yoghurtClothPercent = (countClothYoghurt / countAllYoghurt) * 100
        def clothPercentText = "Percentage straining with a cloth"
        def array25 = [firstText: clothPercentText,yoghurt: df.format(yoghurtClothPercent), milk: df.format(milkClothPercent)]
        allData << array25





        println(allData)
    }
}
//// AVERAGE NUMBER OF PEOPLE LIVING IN THE HOUSEHOLD
//def milkAverageNoHH = filterMilkGroup.sum { (it.total_number_living_hh == null)? 0 : it.total_number_living_hh }  / countAllMilk
//def yoghurtAverageNoHH = filterYoghurtGroup.sum { (it.total_number_living_hh == null)? 0 : it.total_number_living_hh } / countAllYoghurt
//def averageHHText = 'Average number of people living in the household'
//def thirdArray = [firstText: averageHHText,yoghurt: df.format(yoghurtAverageNoHH), milk: df.format(milkAverageNoHH)]
//allData << thirdArray
//
//// AVERAGE NUMBER OF FEMALES < 5
//def milkAverageFemaleBelow5 = filterMilkGroup.sum { (it.females_below5 == null)? 0 : it.females_below5 }  / countAllMilk
//def yoghurtAverageFemaleBelow5 = filterYoghurtGroup.sum { (it.females_below5 == null)? 0 : it.females_below5 } / countAllYoghurt
//def averageFemaleBelow5Text = 'Average number of females <5'
//def fourthArray = [firstText: averageFemaleBelow5Text,yoghurt: df.format(yoghurtAverageFemaleBelow5), milk: df.format(milkAverageFemaleBelow5)]
//allData << fourthArray
//
//// AVERAGE NUMBER OF MALES < 5
//def milkAverageMalesBelow5 = filterMilkGroup.sum { (it.males_below5 == null)? 0 : it.males_below5 }  / countAllMilk
//def yoghurtAverageMalesBelow5 = filterYoghurtGroup.sum { (it.males_below5 == null)? 0 : it.males_below5 } / countAllYoghurt
//def averageMalesBelow5Text = 'Average number of males <5'
//def fifthArray = [firstText: averageMalesBelow5Text,yoghurt: df.format(yoghurtAverageMalesBelow5), milk: df.format(milkAverageMalesBelow5)]
//allData << fifthArray
//
//// AVERAGE NUMBER OF FEMALES 6 - 13
//def milkAverageFemales613 = filterMilkGroup.sum { (it.females_6_13 == null)? 0 : it.females_6_13 }  / countAllMilk
//def yoghurtAverageFemales613 = filterYoghurtGroup.sum { (it.females_6_13 == null)? 0 : it.females_6_13 } / countAllYoghurt
//def averageFemales613Text = 'Average number of females 6-13'
//def sixthArray = [firstText: averageFemales613Text,yoghurt: df.format(yoghurtAverageFemales613), milk: df.format(milkAverageFemales613)]
//allData << sixthArray
//
//// AVERAGE NUMBER OF MALES 6 - 13
//def milkAverageMales613 = filterMilkGroup.sum { (it.males_6_13 == null)? 0 : it.males_6_13 }  / countAllMilk
//def yoghurtAverageMales613 = filterYoghurtGroup.sum { (it.males_6_13 == null)? 0 : it.males_6_13 } / countAllYoghurt
//def averageMales613Text = 'Average number of males 6-13'
//def seventhArray = [firstText: averageMales613Text,yoghurt: df.format(yoghurtAverageMales613), milk: df.format(milkAverageMales613)]
//allData << seventhArray
//
//// AVERAGE NUMBER OF FEMALES 14 - 59
//def milkAverageFemales1459 = filterMilkGroup.sum { (it.females_14_59 == null)? 0 : it.females_14_59 }  / countAllMilk
//def yoghurtAverageFemales1459 = filterYoghurtGroup.sum { (it.females_14_59 == null)? 0 : it.females_14_59 } / countAllYoghurt
//def averageFemales1459Text = 'Average number of females 14-59'
//def eigthArray = [firstText: averageFemales1459Text,yoghurt: df.format(yoghurtAverageFemales1459), milk: df.format(milkAverageFemales1459)]
//allData << eigthArray
//
//// AVERAGE NUMBER OF MALES 14 - 59
//def milkAverageMales1459 = filterMilkGroup.sum { (it.males_14_59 == null)? 0 : it.males_14_59 }  / countAllMilk
//def yoghurtAverageMales1459 = filterYoghurtGroup.sum { (it.males_14_59 == null)? 0 : it.males_14_59 } / countAllYoghurt
//def averageMales1459Text = 'Average number of males 14-59'
//def ninthArray = [firstText: averageMales1459Text,yoghurt: df.format(yoghurtAverageMales1459), milk: df.format(milkAverageMales1459)]
//allData << ninthArray
//
//// AVERAGE NUMBER OF FEMALES 60 AND MORE
//def milkAverageFemales60More= filterMilkGroup.sum { (it.females_60_and_more == null)? 0 : it.females_60_and_more }  / countAllMilk
//def yoghurtAverageFemales60More = filterYoghurtGroup.sum { (it.females_60_and_more == null)? 0 : it.females_60_and_more } / countAllYoghurt
//def averageFemales60MoreText = 'Average number of females 60+'
//def tenthArray = [firstText: averageFemales60MoreText,yoghurt: df.format(yoghurtAverageFemales60More), milk: df.format(milkAverageFemales60More)]
//allData << tenthArray
//
//// AVERAGE NUMBER OF MALES 60 AND MORE
//def milkAverageMales60More = filterMilkGroup.sum { (it.males_60_and_more == null)? 0 : it.males_60_and_more }  / countAllMilk
//def yoghurtAverageMales60More = filterYoghurtGroup.sum { (it.males_60_and_more == null)? 0 : it.males_60_and_more } / countAllYoghurt
//def averageMales60MoreText = 'Average number of males 60+'
//def array11 = [firstText: averageMales60MoreText,yoghurt: df.format(yoghurtAverageMales60More), milk: df.format(milkAverageMales60More)]
//allData << array11
