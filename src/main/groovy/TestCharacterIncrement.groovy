class TestCharacterIncrement {
    static void main(def args) {

        println(next('pbn'))
    }
    public static String next(String s) {
        def length = s.length();
        def c = s.charAt(length - 1)

        if(c == 'z'.toCharacter())
            return length > 1 ? next(s.substring(0, length - 1)) + 'a' : "aa";

        return s.substring(0, length - 1) + ++c;
    }
}
