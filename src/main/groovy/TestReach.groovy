import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*
import groovy.sql.Sql

import static fuzzycsv.RecordFx.fx

class TestReach {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/reachmis', 'root', "", 'com.mysql.jdbc.Driver')

        def queryPotatoSeasonA = """select
  ((case when acarage_potato_a is null then 0
    else  convert(acarage_potato_a, decimal(10,2)) end)) as sum_acre,
  ((case when potato_prod_a is null then 0
    else potato_prod_a end)) as sum_prod
from `_34_xxx_annual_household_survey_tool`
where crop_grown = 'potato'"""

        def queryPotatoSeasonB = """select
  ((case when acarage_potato_b is null then 0
    else convert(acarage_potato_b, decimal(10,2)) end)) as sum_acre,
  ((case when potato_prod_b is null then 0
    else potato_prod_b end)) as sum_prod
from `_34_xxx_annual_household_survey_tool`
where crop_grown  = 'potato' """

        def potatoSeasonAData = FuzzyCSVTable.toCSV(sql, queryPotatoSeasonA).printTable()
        def countPotatoRecordsSeasonA = potatoSeasonAData.autoAggregate(count('sum_prod').az('count'))
        def filteredPotatoSeasonA= potatoSeasonAData.addColumn(fx('perAcre') {(it.sum_acre == 0 || it.sum_prod == 0) ? 0 : (it.sum_prod) / it.sum_acre})
        def sumPotatoRecordsSeasonA = filteredPotatoSeasonA.autoAggregate(sum('perAcre').az('sum'))
        def sum_total_rice_season_a = sumPotatoRecordsSeasonA.sum{it.sum} / countPotatoRecordsSeasonA.sum{it.count}


        def potatoSeasonBData = FuzzyCSVTable.toCSV(sql, queryPotatoSeasonB).printTable()
        def countPotatoRecordsSeasonB = potatoSeasonBData.autoAggregate(count('sum_prod').az('count')).toMapList()
        def filteredPotatoSeasonB= potatoSeasonBData.addColumn(fx('perAcre') {(it.sum_acre == 0 || it.sum_prod == 0) ? 0 : (it.sum_prod) / it.sum_acre})
        def sumPotatoRecordsSeasonB = filteredPotatoSeasonB.autoAggregate(sum('perAcre').az('sum')).toMapList()
        def sum_total_rice_season_b = sumPotatoRecordsSeasonB.sum{it.sum} / countPotatoRecordsSeasonB.sum{it.count}


        def sum_total = (sum_total_rice_season_a + sum_total_rice_season_b) / 2

        println(sum_total)


    }
}
