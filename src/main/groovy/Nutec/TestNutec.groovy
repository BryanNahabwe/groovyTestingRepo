package Nutec


import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import java.text.DecimalFormat
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestNutec {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.jdbc.Driver')
        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Number of indirect beneficiaries (household members) with increased resilience"
        })
        def query = """
                        select intervention                                                                            as inter,
                               agribusiness                                                                            as name,
                               district_a                                                                              as district,
                               substring_index(reporting_year, '_', -1)                                                as year,
                               reporting_quarter                                                                       as quarter_year,
                               concat_ws('_', reporting_quarter, reporting_year)                                       as period,
                               (((sum(ifnull(sunflowergrain_buyers_females, 0)) + sum(ifnull(sunflowergrain_buyers_males, 0))) +(0.5 * (sum(ifnull(sunflower_buyers_females, 0)) + sum(ifnull(sunflower_buyers_males, 0))))) +
                                (sum(ifnull(sunflower_soybean_seed_buy_total, 0))) +
                               ((sum(ifnull(namche_buyers_females, 0)) + sum(ifnull(namche_buyers_males, 0))) +(0.5 * (sum(ifnull(rice_grain_sell_females, 0)) + sum(ifnull(rice_grain_sell_males, 0))))) +
                                ((sum(ifnull(feed_females, 0)) + sum(ifnull(feed_buyers_males, 0))) +(0.5 * (sum(ifnull(feed_sell_rawmaterialsupply_females, 0)) + sum(ifnull(feed_sell_rawmaterialsupply__males, 0))))) +
                                (sum(ifnull(farmers_bags_buying, 0))) +
                                (sum(ifnull(farmers_using_tractors, 0))) +
                                ((sum(ifnull(buyers_females_mar, 0)) + sum(ifnull(buyers_males_mar, 0))) +(0.5 * (sum(ifnull(crop_purchased_female_mar, 0)) + sum(ifnull(crop_purchased_male_mar, 0))))) +
                                (sum(ifnull(host_2, 0)) + sum(ifnull(refugees_2, 0))) +
                                ((sum(ifnull(bought_inorganic_female, 0)) + sum(ifnull(bought_inorganic_male, 0))) +(0.25 * ((sum(ifnull(bought_maize_female, 0)) + sum(ifnull(bought_maize_male, 0))) +
                                                                                                                             (sum(ifnull(bought_chia_seed_female, 0)) + sum(ifnull(bought_rice_seed_male, 0)))+
                                                                                                                             (sum(ifnull(bought_chia_seed_crop_female, 0)) + sum(ifnull(bought_chia_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_sorghum_seed_crop_female, 0)) + sum(ifnull(bought_sorghum_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_millet_seed_crop_female, 0)) + sum(ifnull(bought_millet_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_other_farm_inputs_female, 0)) + sum(ifnull(bought_other_farm_inputs_male, 0)))))) +
                                ((sum(ifnull(farmers_buying_cassava_female, 0)) + sum(ifnull(farmers_buying_cassava_male, 0))) +(0.5 * (sum(ifnull(farmers_buying_sorghum_marcassava_female, 0)) + sum(ifnull(farmers_buying_sorghum_marcassava_male, 0))))) +
                                ((sum(ifnull(chia_seed_purchased_female, 0)) + sum(ifnull(chia_seed_purchased_male, 0))) +(0.5 * (sum(ifnull(gooseberry_seed_purchased, 0)) + sum(ifnull(gooseberry_seed_purchased_female, 0))))) +
                                ((sum(ifnull(stevia_buyers_females, 0)) + sum(ifnull(stevia_buyers_males, 0))) +(0.5 * (sum(ifnull(stevia_fresh_females, 0)) + sum(ifnull(stevia_fresh_males, 0)))))   ) as total
                        from `_189_xxx_kpi_monitoringtool`
                        group by agribusiness, `__code`, district_a, reporting_year, reporting_quarter, intervention,unique_id
                        order by intervention
                        """

        def query1 =  """
                        select intervention as inter,
                               partner                                             as name,
                               district,
                               substring_index(year, '_', -1)                      as year,
                               quater as quarter_year,
                               concat_ws('_', quater, year)                        as period,
                               sum(ifnull(non_youth, 0))      as total
                        from `_182_xxx_lograme_trakers_form`
                        group by intervention,partner,district,year,quater,unique_id
                     """

        def query2 = """
                        select distinct option_id  as inter ,option_text as intervention
                        from `_182_intervention`
                        where option_text is not null
                        union all 
                        select distinct option_id,option_text
                        from `_189_intervention`
                        where option_text is not null
                        """

        def date = new Date()
        def current_year = date[Calendar.YEAR].toString()


        def fuzzyData2 = FuzzyCSVTable.toCSV(sql, query2).copy().appendEmptyRecord().modify {
            set { it.inter = 'M4R Offtake'; it.intervention = 'M4R Offtake' }; where { it.inter in [null] }
        }
        def fuzzyData = FuzzyCSVTable.toCSV(sql, query.toString()).join(fuzzyData2, 'inter')
        def fuzzyData1 = FuzzyCSVTable.toCSV(sql, query1.toString()).join(fuzzyData2, 'inter')
        def groupPerIntervention = fuzzyData.autoAggregate('intervention', 'year', sum('total').az('total'))
        def groupByDistrictAndIntervention = fuzzyData.autoAggregate('district', 'intervention', 'year', sum('total').az('total'))
                .pivot('year', 'total', 'district', 'intervention').modify {set { it.base = 0 } where{ it.base in [null]}}
                .modify {set { it.'2019' = 0 } where{ it.'2019' in [null]}}
                .addColumn(fx('total') {
            if (current_year == '2019') {
                return (it.'2019' - it.base) * 4.3
            } else if (current_year == '2020') {
                return (it.'2020' - it.base) * 4.3
            } else if (current_year == '2021') {
                return (it.'2021' - it.base) * 4.3
            }
            if (current_year == '2022') {
                return (it.'2022' - it.base) * 4.3
            } else {
                return 0
            }
        })
        def group1 = groupByDistrictAndIntervention.autoAggregate('district', sum('total').az('total')).printTable()
        def group2 = fuzzyData1.autoAggregate('district', sum('total').az('total'))
        def groupByDistrict = (group1 + group2).filter {
            it.district != null
        }.autoAggregate('district', sum('total').az('total')).toMapList()

        //def groupByDistrict = addInterventions.autoAggregate('district', sum('total').az('total')).toMapList()
        def icon = "fa fa-users"
        def color = "bg-navy"
        def title = "Number of indirect beneficiaries (household members) with increased resilience"
        def sum_total = (groupByDistrict.sum { it.total } != null) ? groupByDistrict.sum { it.total } : 0

        def filterOutBaselines = groupPerIntervention.filter { it.year == 'base' }.addColumn(fx('baseline') {return it.total}).delete('total','year')
        def newGroup1 = groupPerIntervention.filter {it.year != 'base'}.join(filterOutBaselines,'intervention').addColumn(fx('new_total'){return (it.total - it.baseline) * 4.3})
                .delete('total','baseline','baseline').renameHeader('new_total','total')
        def newGroup2 = fuzzyData1.autoAggregate('intervention','year',sum('total').az('total'))
                .modify {set { it.intervention = 'Expanding the Village Agent Model in Northern Uganda'} where { it.intervention in ['Expanding the Village Agent Model in NU']}}
        def combine_both_groups = (newGroup1 +  newGroup2).autoAggregate('year','intervention',sum('total').az('total'))
        def targets = targetData.fullJoin(combine_both_groups, 'year')
        def modifyTargets = targets.modify {
            set {
                it.total = 0
                it.intervention = 'null'
            }
            where {
                it.total in [null]
            }
        }.filter{ it.year != '2016'}.filter { it.year != 'base'}.toMapList()

    }
}
