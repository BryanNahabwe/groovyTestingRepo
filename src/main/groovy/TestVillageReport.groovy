import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class TestVillageReport {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.jdbc.Driver')
         def subcounty = "aduku"
        def village = ""
        def parish = "aboko"
        def district = "apac"
        def query1 = """SELECT
                              bs.village AS village,count(*) as hhs_at_baseline,
                              SUM(IF(latrine='yes',1,0)) as hhs_with_latrine,
                              SUM(IF(latrine='no',1,0)) as hhs_without_latrine,
                              SUM(if(latrine='no',bs.male_hh+bs.female_hh,0)) as population,
                              q.hhs_followed_up,q.new_latrines,q.under_construction,
                              q.farmers,bs.district,bs.subcounty,bs.parish,
                              l.triggered as triggered,
                              l.Odf_verified as odf_verified,
                              l.odf_certified as odf_certified
                            FROM
                              _17_xxx_baseline_survey_form bs
                              LEFT JOIN(
                                 SELECT
                                   f.village AS village,count(DISTINCT f.__code) as hhs_followed_up,
                                   g.cons as under_construction,g.new_l as new_latrines,
                                   g.farmers as farmers,f.parish,f.district,f.subcounty
                                 FROM
                                   _19_xxx_household_followup_form_ f
                                   INNER JOIN (
                                      Select village,sum(if(t.under_c=1,1,0)) as cons, sum(if(t.new_l=1,1,0)) as new_l,
                                                     GROUP_CONCAT(t.max_date_code) as farmers
                                      from (
                                            SELECT
                                                 date_entry,
                                                 _19_xxx_household_followup_form_.__code                                       as max_date_code,
                                                 village,
                                                 IF(latrine_status = 'a_latrine_is_in_the_process_of_being_constructed', 1, 0) as under_c,
                                                 IF(latrine_status = 'latrine', 1, 0)                                          as new_l,
                                                 subcounty,
                                                 parish,
                                                 district
                                               FROM _19_xxx_household_followup_form_
                                                 INNER JOIN
                                                 (
                                                   SELECT
                                                     Max(date_entry) date,
                                                     __code
                                                   FROM _19_xxx_household_followup_form_
                                                   GROUP BY `__code`
                                                 ) AS t2
                                                   ON _19_xxx_household_followup_form_.__code = t2.`__code`
                                                      AND _19_xxx_household_followup_form_.date_entry = t2.date
                                        
                                               WHERE _19_xxx_household_followup_form_.__code is not null
                                               GROUP BY max_date_code
                                      
                                      )t
                            
                                      GROUP BY t.village,t.subcounty,t.parish,t.village,t.district)g on g.village= f.village
                                 GROUP BY f.village,f.district,f.parish,f.subcounty
                               )q on q.village= bs.village and q.parish= bs.parish and q.subcounty= bs.subcounty
                            LEFT JOIN(
                                 SELECT DISTINCT
                                   m.village,m.district,m.subcounty,m.parish,
                                   IF(m.activity='trigger','yes','no') as triggered,
                                   t.Odf_v as Odf_verified,p.odf_c as odf_certified
                                 from _21_xxx_clts_and_odf_verification_form m
                                   INNER JOIN (
                                      SELECT DISTINCT
                                        m.village,m.district,m.subcounty,m.parish,
                                        IF(m.activity='verify_odf_villages','yes','no') as Odf_v
                                      from _21_xxx_clts_and_odf_verification_form m)t on t.district=m.district AND t.subcounty = m.subcounty and
                                             t.parish= m.parish and t.village= m.village 
                                   INNER JOIN (
                                          SELECT DISTINCT
                                            m.village,m.district,m.subcounty,m.parish,
                                            IF(m.activity='certify_odf_villages','yes','no') as odf_c
                                          from _21_xxx_clts_and_odf_verification_form m)p on t.district=m.district
                                           AND p.subcounty = m.subcounty and p.parish= m.parish and p.village= m.village
                                 WHERE m.district = '${district}' AND m.subcounty  = '${subcounty}' and m.parish = '${parish}')
                                       l  on l.village= bs.village and l.parish= bs.parish and l.subcounty= bs.subcounty
                            WHERE bs.district = '${district}' and bs.subcounty   = '${subcounty}' and bs.parish  = '${parish}'
                            GROUP BY bs.village,bs.district,bs.parish,bs.subcounty """.toString()

        println(query1)

        def data = FuzzyCSVTable.toCSV(sql,query1).printTable()
    }
}
