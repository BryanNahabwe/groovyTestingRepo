package Nutec

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx

class Proportion {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.cj.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Proportion of business models established (Palladium)/ investments (AgDevCo) supported by NU-TEC that have a systemic effect"
        }).modify {set {it.year_month = '16-Sep'} where {it.year_month in ['Sep-16']}}
                .modify {set {it.year_month = '17-Mar'} where {it.year_month in ['Mar-17']}}
                .modify {set {it.year_month = '18-Mar'} where {it.year_month in ['Mar-18']}}
                .modify {set {it.year_month = '19-Mar'} where {it.year_month in ['Mar-19']}}
                .modify {set {it.year_month = '20-Mar'} where {it.year_month in ['Mar-20']}}
                .modify {set {it.year_month = '21-Mar'} where {it.year_month in ['Mar-21']}}
                .modify {set {it.year_month = '21-Dec'} where {it.year_month in ['Dec-21']}}

        def query = """
select evidence,
       intervention                                 as inter,
       case
         when reporting_year = '_2016' then '16-Sep'
         when reporting_year = '_2017' then '17-Mar'
         when reporting_year = '_2018' then '18-Mar'
         when reporting_year = '_2019' then '19-Mar'
         when reporting_year = '_2020' then '20-Mar'
         when reporting_year = '_2021' then '21-Mar'
         when reporting_year = '_2022' then '21-Dec'
         when reporting_year = 'base' then 'base'
         else 'no year month' end                   as year_months,
       replace(reporting_year, '_', '')             as year,
       sum(case when evidence = 'yes' then 1 else 0 end) as count
from _188_xxx_systemic_change_form
group by intervention,reporting_year
                        """
        def query1 = """   
                        select distinct option_id  as inter ,
                                        case
                  when option_text = 'Expanding the Village Agent Model in NU' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'expanding the village agent model in nu' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'M4R Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'organic producer out hyphen grower scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Organic Producer Out-grower Scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Private provision of on-farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Multiplication and distribution of improved seeds' then 'Multiplication and Distribution of Improved Seeds'
                  when option_text = 'private provision of on hyphen farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'stevia production for increased employment opportunities especially for women in northern Uganda' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Cassava' then 'M4R Commercial cassava production'
                  when option_text = 'Stevia Women Outgrower Pilot' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Offtake' then 'M4R Seed and Offtake'
                  when option_text = 'M4R Seed' then 'M4R Seed and Offtake'
                                          else option_text end as intervention
                        from `_188_intervention`
                        where option_text is not null
                            """

        def query2 = """  select distinct  intervention  from `_185_xxx_agribusiness_partner` """

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query.toString())
        def fuzzyData2 = FuzzyCSVTable.toCSV(sql,query2.toString())
        def countInterventions = fuzzyData2.count { it.intervention }
        def fuzzyData1 = FuzzyCSVTable.toCSV(sql,query1).copy().appendEmptyRecord().modify { set {it.inter='M4R Offtake'; it.intervention='M4R Seed and Offtake'}; where {it.inter in [null]}}

        def joinTables = fuzzyData.join(fuzzyData1,'inter').addColumn(fx('total_inter'){return countInterventions})
                .addColumn(fx ('percent'){ return (it.count/it.total_inter)*100 })

        def aggregateYears = joinTables.autoAggregate('year','intervention',sum('percent').az('total'))
        def targets = targetData.fullJoin(aggregateYears,'year')
        def modifyTargets = targets.modify {
            set {
                it.total = 0
                it.intervention = 'null'
            }
            where {
                it.total in [null]
            }
        }.filter{ it.year != '2016'}.filter { it.year != 'base'}.printTable().toMapList()
        //        // Intervention Calculation
        def filterInterventionPerYear = []
        def groupPerIntervention1 = joinTables.autoAggregate('intervention', 'year_months', sum('percent').az('total'))
                .sort { it.intervention }.renameHeader('year_months', 'year_month')
        def newInterList = groupPerIntervention1.toMapList().collect { it.intervention }.unique()

        def combineAllInterventions = []
        newInterList << 'All Interventions'
        newInterList.each { inter ->
            def eachIntervention = inter.toString()
            def createSampleMap = [[year_month: '16-Sep'], [year_month: '17-Mar'],
                                   [year_month: '18-Mar'], [year_month: '19-Mar'],
                                   [year_month: '20-Mar'], [year_month: '21-Mar'],
                                   [year_month: '21-Dec']]
            createSampleMap.eachWithIndex { record, index ->
                record.put('intervention', eachIntervention)
                combineAllInterventions << record
            }
        }

        def changeInterMapToFuzzy = FuzzyCSVTable.toCSV(combineAllInterventions).join(targetData, 'year_month').delete('Outcome', 'year')
        def mergeAllInterventions = changeInterMapToFuzzy.fullJoin(groupPerIntervention1, 'year_month', 'intervention').modify {
            set { it.total = 0 } where { it.total in [null] }
        }

        def aggregateAllInterventions = mergeAllInterventions.autoAggregate('year_month', sum('total').az('alltotal'))
        def addColumnInterventions = aggregateAllInterventions.addColumn(fx('intervention') {
            return 'All Interventions'
        })
        def joinAllInterventionData = mergeAllInterventions.fullJoin(addColumnInterventions,'year_month','intervention')
                .modify {set{it.total = it.alltotal} where{it.intervention in ['All Interventions']}}
                .addColumn(fx('indicator_index') {return 7 }).delete('alltotal').toMapList()
    }
}
