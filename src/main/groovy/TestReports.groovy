import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql
import static fuzzycsv.FuzzyStaticApi.*

class TestReports {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.jdbc.Driver')

        def query = """
                                    select municipality,form,
                                           if(hand_wash_type = 'no hand washing facility' or hand_wash_type = 'no_hand_wash', 1, 0) as count
                                    from `town_analysis_view`
                                    order by municipality;
                                """

        def townData = FuzzyCSVTable.toCSV(sql, query)

        def count = (((townData.sum { it.count } / townData.size()) * 100) as double).round(2)

        def groupByMunicipalityForm = townData.addColumn(fx('tths'){ return 1 }).
                autoAggregate('municipality', 'form', sum('count').az('count'),sum('tths').az('tths'))
                .addColumn(fx('percentage') {
                    return (((it.count / it.tths) * 100) as double).round(2)
                })

        def pivotTable = groupByMunicipalityForm.pivot('form', 'percentage', 'municipality')
                .modify { set { it.household = 0 } where { it.household in [null] } }
                .modify { set { it.followup = 0 } where { it.followup in [null] } }

        def finalResult = pivotTable.addColumn(fx('data') {
            return [[from: 0, to: it.household, color: '#666'],
                    [from: it.household, to: it.household + it.followup, color: '#999'],
                    [from: it.household + it.followup, to: 100, color: '#bbb']]
        })
                .addColumn(fx('y') { return count })
                .addColumn(fx('indicator') {
                    return "Households without hand washing facilities"
                }).printTable().toMapList()

    }
}
