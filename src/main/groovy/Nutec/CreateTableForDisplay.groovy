package Nutec

import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql

import java.text.DecimalFormat

import static fuzzycsv.FuzzyStaticApi.*

class CreateTableForDisplay {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.cj.jdbc.Driver')

        def query = """
                    select indicator_index,indicator,intervention,`year_month`,round(total,0) as total,round(target_value,0) as target_value
                    from nutec_intervention_target_data
                    where intervention = 'All Interventions'
                    order by `year_month`
                    """
        def df = new DecimalFormat("###,##0")
        def fuzzy = FuzzyCSVTable.toCSV(sql,query).sort {it.year_month}
        def addAnotherColumn = fuzzy.addColumn(RecordFx.fx ('new_total'){
            if (it.indicator == 'Proportion of business models established (Palladium)/ investments (AgDevCo) supported by NU-TEC that have a systemic effect'){
                def total_value = df.format(it.total)
                def target_value = df.format(it.target_value)
                return "<span style='font-weight:600;'>${total_value}%/<span style='color: #0A9788;'>${target_value}%</span></span>"
            }
            else{
                def total_value = df.format(it.total)
                def target_value = df.format(it.target_value)
                return "<span style='font-weight:600;'>${total_value}/<span style='color: #0A9788;'>${target_value}</span></span>"
            }

        })
        def autoaggrete = fuzzy.filter {it.year_month == '21-Dec'}.addColumn(fx ('percentage'){
            return ((it.total / it.target_value)* 100).round(0)
        }).autoAggregate('indicator',sum('percentage').az('percentage'))
        def pivotTable = addAnotherColumn.pivot('year_month','new_total','indicator_index','indicator','intervention')
                .join(autoaggrete,'indicator').sort {it.indicator_index}.printTable().toMapList()
    }
}
