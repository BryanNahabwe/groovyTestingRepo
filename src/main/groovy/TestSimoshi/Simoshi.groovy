package TestSimoshi

import fuzzycsv.FuzzyCSV
import groovy.json.JsonOutput
import groovy.sql.Sql
import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class Simoshi {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/simoshimis', 'root', "", 'com.mysql.jdbc.Driver')
        def oodate = "2016-01-01"
        def queryDate = """
                    select replace(school_name,"'","''") as school_name,use_date
                    from `_15_xxx_kitchen_monitoring_form`
                    where use_date is not null
                    group by school_name
                    having use_date >= '${oodate}'
                    order by school_name
                    """.toString()
        def schoolDate = FuzzyCSVTable.toCSV(sql, queryDate)
        def schoolValue = 'Gangu Muslim'

        def schoolArray = []
        def filterGroup
        if (schoolValue == 'all') {
            schoolDate.each {
                schoolArray << it.school_name as String
            }
            def firstJoin = schoolArray.join(",")
            def split = firstJoin.split(',').toList()
            def addCommas = split.collect { "'$it'" }
            filterGroup = addCommas.join(",")

        } else {
            def split = schoolValue.split(',').toList()
            def addCommas = split.collect { "'$it'" }
            filterGroup = addCommas.join(",")
        }

        def type = ""

        def query = """
                    select
                      concat_ws('', t.schol_name, t.school_name) as school_name,
                      `__code`                                   as code,
                      day as dayStudents,
                      boarding as boardingStudents,
                      teacher_no as teacher,
                      non_teacher_no as non_teacher,
                      year(date_of_visit)                        as year,
                      replace(term, '_', ' ')                    as term,
                      (case
                       when (day != 0 && boarding != 0)
                         then 'day/boarding'
                       when day != 0 && boarding = 0
                         then 'day'
                       when day = 0 && boarding != 0
                         then 'boarding'
                       else ''
                       end)                                      as type
                    from `_13_xxx_school_questionnaire` t
                      inner join
                      (select
                         concat_ws('', schol_name, school_name) as school_name2,
                         max(openxdata_form_data_date_created)  as maxdate
                       from `_13_xxx_school_questionnaire`
                       group by concat_ws('', schol_name, school_name), term
                      ) t1
                        on concat_ws('', t.schol_name, t.school_name) = t1.school_name2 and t.openxdata_form_data_date_created = t1.maxdate
                    where concat_ws('', t.schol_name, t.school_name) in (${filterGroup})
                    """.toString()
        def schooldata = FuzzyCSVTable.toCSV(sql, query).printTable()
        def queryOpDate = """
                            select DATE_FORMAT(min(use_date), "%Y-%m-%d") as date
                            from `_15_xxx_kitchen_monitoring_form`
                            where school_name in (${filterGroup})
                            """.toString()

        def schoolOpDate = FuzzyCSVTable.toCSV(sql, queryOpDate)
        def schoolOpdatte = schoolOpDate['date'][0]
        println(schoolOpdatte)
        //Declare Variables
        def finalResult = []
        def term1Days
        def term2Days
        def term3Days
        def term1MonthRange = [0, 1, 2, 3]
        def term2MonthRange = [4, 5, 6, 7]
        def term3MonthRange = [8, 9, 10, 11]
        def term1StartDate = '01-01'
        def term1EndDate = '04-30'
        def term2StartDate = '05-01'
        def term2EndDate = '08-31'
        def term3StartDate = '09-01'
        def term3EndDate = '12-31'
        def term
        def daysInLastTerm
        def daysInCurrentTerm
        def opDate = schoolOpdatte
        def currDate = new Date()


        //Get the number of days per term
        term1Days = Date.parse('MM-dd', term1EndDate.toString()) - Date.parse('MM-dd', term1StartDate.toString())
        term2Days = Date.parse('MM-dd', term2EndDate.toString()) - Date.parse('MM-dd', term2StartDate.toString())
        term3Days = Date.parse('MM-dd', term3EndDate.toString()) - Date.parse('MM-dd', term3StartDate.toString())


//Get years of starting operational and current year
        //Determine the current year
        def currentYear = currDate[Calendar.YEAR]

        //Determine the start year
        def startYear = Date.parse('yyyy-MM-dd', opDate.toString())[Calendar.YEAR]

        //Force the dates not to exceed the last date fo the term
        def newEndDateCurrentTerm3 = currentYear + '-' + term3EndDate
        def newEndDateLastTerm3 = startYear + '-' + term3EndDate
        def term3endscurrent = Date.parse('yyyy-MM-dd', newEndDateCurrentTerm3)
        if (currDate > term3endscurrent) {
            currDate = term3endscurrent
        }
        if (opDate > newEndDateLastTerm3) {
            opDate = newEndDateLastTerm3
        }

//Get the first results using the the operational date
        if (startYear != currentYear) {
            //Get the month number to get determine the start term of the operational
            def getMonthNumber = Date.parse('yyyy-MM-dd', opDate.toString()).month

            //Get the first term of operational
            if (term1MonthRange.contains(getMonthNumber)) {
                term = 'term 1'
            } else if (term2MonthRange.contains(getMonthNumber)) {
                term = 'term 2'
            } else if (term3MonthRange.contains(getMonthNumber)) {
                term = 'term 3'
            } else {
                term = ''
            }

            //Determine the Remaining days in the term of start date

            if (getMonthNumber <= 3) {
                def newStartDate = startYear + '-' + term1StartDate
                def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInLastTerm = (term1Days - daysUsed) / term1Days
            } else if (getMonthNumber >= 4 && getMonthNumber <= 7) {
                def newStartDate = startYear + '-' + term2StartDate
                def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInLastTerm = (term2Days - daysUsed) / term2Days
            } else if (getMonthNumber >= 8) {
                def newStartDate = startYear + '-' + term3StartDate
                def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInLastTerm = (term3Days - daysUsed) /term3Days
            } else {
                daysInLastTerm = 'Nothing'
            }
            //Results per term per year
            def result1 = [year: startYear, term: term, days: daysInLastTerm, date: opDate]
            finalResult << result1

//Determine the last results

            //Get the month number to get determine the start term of the operational
            def getCurrentMonthNumber = currDate.month

            //Get the first term of operational
            if (term1MonthRange.contains(getCurrentMonthNumber)) {
                term = 'term 1'
            } else if (term2MonthRange.contains(getCurrentMonthNumber)) {
                term = 'term 2'
            } else if (term3MonthRange.contains(getCurrentMonthNumber)) {
                term = 'term 3'
            } else {
                term = ''
            }

            //Determine the Remaining days in the term of start date
            if (getCurrentMonthNumber <= 3) {
                def newStartDate = currentYear + '-' + term1StartDate
                def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInCurrentTerm = daysUsed / term1Days
            } else if (getCurrentMonthNumber >= 4 && getCurrentMonthNumber <= 7) {
                def newStartDate = currentYear + '-' + term2StartDate
                def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInCurrentTerm = daysUsed / term2Days
            } else if (getCurrentMonthNumber >= 8) {
                def newStartDate = currentYear + '-' + term3StartDate
                def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
                daysInCurrentTerm = daysUsed /term3Days
            } else {
                daysInCurrentTerm = 'Nothing'
            }

            //Results per term per year

            def result2 = [year: currentYear, term: term, days: daysInCurrentTerm, date: currDate.format("yyyy-MM-dd")]
            finalResult << result2

//Determine the terms and years in between
            def betweenYears = (currentYear - startYear) - 1
            for (def i = betweenYears; i > 0; i--) {
                def allTerms = ['term 1': (term1Days / term1Days), 'term 2': (term2Days / term2Days), 'term 3': (term3Days / term3Days)]
                def yearCounter = currentYear - i
                allTerms.each { k, v ->
                    def date
                    switch (k) {
                        case 'term 1':
                            def newTerm1Ends = yearCounter + '-' + term1EndDate
                            date = newTerm1Ends
                            break
                        case 'term 2':
                            def newTerm2Ends = yearCounter + '-' + term2EndDate
                            date = newTerm2Ends
                            break
                        case 'term 3':
                            def newTerm3Ends = yearCounter + '-' + term3EndDate
                            date = newTerm3Ends
                            break
                        default:
                            date = "No term has ended"
                            break
                    }
                    def results3 = [year: yearCounter, term: k, days: v, date: date]
                    finalResult << results3
                }
            }

//Determine the terms with in the start date and the current date
            //Other terms from the starting date in the same year
            def endOfTerm3StartYear = startYear + '-' + term3EndDate
            def endOfTerm2StartYear = startYear + '-' + term2EndDate
            def daysBetween = (Date.parse('yyyy-MM-dd', endOfTerm3StartYear.toString()) - Date.parse('yyyy-MM-dd', opDate.toString())) - 1
            if (daysBetween > term3Days) {
                finalResult << [year: startYear, term: 'term 3', days: (term3Days / term3Days), date: endOfTerm3StartYear]
            }

            if ((daysBetween - term3Days) > term2Days) {
                finalResult << [year: startYear, term: 'term 2', days: (term2Days/term2Days), date: endOfTerm2StartYear]
            }

            //Other terms before the current date in the same year
            def startOfCurrentYearTerm
            startOfCurrentYearTerm = Date.parse('yyyy-MM-dd', (currentYear + '-' + term1StartDate).toString())
            def endOfTerm1CurrentYear = currentYear + '-' + term1EndDate
            def endOfTerm2CurrentYear = currentYear + '-' + term2EndDate
            def daysWorkingInCurrentYear = (currDate - startOfCurrentYearTerm) - 1


            if (daysWorkingInCurrentYear > term1Days) {
                finalResult << [year: currentYear, term: 'term 1', days: (term1Days/term1Days), date: endOfTerm1CurrentYear]
            }

            if ((daysWorkingInCurrentYear - term1Days) > term2Days) {
                finalResult << [year: currentYear, term: 'term 2', days: (term2Days/term2Days), date: endOfTerm2CurrentYear]
            }

        } else {
            def newEndDateTerm1 = currentYear + '-' + term1EndDate
            def newEndDateTerm2 = currentYear + '-' + term2EndDate
            def newStartDateTerm2 = currentYear + '-' + term2StartDate
            def newStartDateTerm3 = currentYear + '-' + term3StartDate
            def term1ends = Date.parse('yyyy-MM-dd', newEndDateTerm1.toString())
            def term2ends = Date.parse('yyyy-MM-dd', newEndDateTerm2.toString())
            def term2starts = Date.parse('yyyy-MM-dd', newStartDateTerm2.toString())
            def term3starts = Date.parse('yyyy-MM-dd', newStartDateTerm3.toString())
            def startDate = Date.parse('yyyy-MM-dd', opDate.toString())

            if (term1ends >= startDate) {
                def daystaken = (term1ends - startDate) / term1Days
                finalResult << [year: currentYear, term: 'term 1', days: daystaken, date: opDate]
            }
            if (term2starts <= startDate && term2ends >= startDate) {
                def daystaken = (term2ends - startDate) / term2Days
                finalResult << [year: currentYear, term: 'term 2', days: daystaken, date: opDate]
            }
            if (term2starts > startDate && term2ends >= startDate) {
                def firstDiff = (term1ends - startDate)
                def daystaken = ((term2ends - startDate) - firstDiff - 1) / term2Days
                finalResult << [year: currentYear, term: 'term 2', days: daystaken, date: newEndDateTerm2]
            }
            if (term3starts <= startDate && currDate >= startDate) {
                def daystaken = (currDate - startDate) / term3Days
                finalResult << [year: currentYear, term: 'term 3', days: daystaken, date: currDate.format("yyyy-MM-dd")]
            }
            if (term3starts > startDate && currDate >= startDate) {
                def firstDiff = term1ends - startDate
                def secondDiff = term2ends - term2starts
                def daystaken = ((currDate - startDate) - firstDiff - secondDiff - 2) / term3Days
                finalResult << [year: currentYear, term: 'term 3', days: daystaken, date: currDate.format("yyyy-MM-dd")]
            }
        }

//Combine all the results
        def returnData
        def toFuzzy = FuzzyCSVTable.toCSV(finalResult)
        def filterZeros = toFuzzy.filter { it.days != 0 }
        def firstSortedData = filterZeros.sort { "$it.year $it.term" }
        def joinOpDate = schooldata.fullJoin(schoolDate,'school_name')
        def filterAll = joinOpDate.filter { it.dayStudents != null && it.boardingStudents != null }.printTable()
        def schoolInfo = filterAll

        if (schoolValue == 'all') {
            def joinAllRecords = []
            def output = schoolInfo.toMapList().groupBy { it.school_name }.each { t ->
                def tempArray
                tempArray = t.value
                def perSchool = FuzzyCSVTable.toCSV(tempArray)
                def perSchoolRecords = perSchoolModifier(perSchool, firstSortedData)
                returnData =calculateCarbon(perSchoolRecords,type,opDate)
                returnData.each {
                    joinAllRecords << it
                }

            }
            def chtoArray = FuzzyCSVTable.toCSV(joinAllRecords)
        } else {
            def perSchoolRecords = perSchoolModifier(schoolInfo, firstSortedData)
            returnData = calculateCarbon(perSchoolRecords,type,opDate)
        }
    }

    static def getPrev(ArrayList<Map> data, int currentRow, def column) {
        if (currentRow == 0)
            return data.get(currentRow)."$column"

        if (data.get(currentRow)."$column" != 0) {
            return data.get(currentRow)."$column"
        }
        getPrev(data, currentRow - 1, column)

    }

    static def perSchoolModifier(perSchool, firstSortedData) {
        def schoolType = perSchool.filter { return it.type != null }['type'][0]
        def schoolName = perSchool.filter { return it.school_name != null }['school_name'][0]
        def schoolCode = perSchool.filter { return it.code != null }['code'][0]
        def schoolOperationDate = perSchool.filter { return it.use_date != null }['use_date'][0]

        def joinedData = perSchool.fullJoin(firstSortedData, 'term', 'year')
        def modifyRecords = joinedData.modify {
            set {
                it.school_name = schoolName
                it.code = schoolCode
                it.type = schoolType
                it.dayStudents = 0
                it.boardingStudents = 0
                it.use_date = schoolOperationDate
                it.teacher = 0
                it.non_teacher = 0
            }
            where {
                it.school_name in [null]
            }
        }.printTable().toMapList()

        int idx = 0
        modifyRecords.collect { row ->
            def temp = row
            temp.dayStudents = getPrev(modifyRecords, idx, "dayStudents")
            temp.boardingStudents = getPrev(modifyRecords, idx, "boardingStudents")
            temp.teacher = getPrev(modifyRecords, idx, "teacher")
            temp.non_teacher = getPrev(modifyRecords, idx, "non_teacher")
            idx++
            return temp
        }
    }

    static def calculateCarbon(def modifiedRecords,def type, def opDate) {
        def runningTotal = 0
        def result = FuzzyCSVTable.toCSV(modifiedRecords)
        def filterNulls = result.filter { it.days != null }
        def carbon_credit = filterNulls.addColumn(
                fx('opdate') { return opDate },
                fx('total') {
                    def noPupils
                    switch (type) {
                        case 'day':
                            def dayPupils = 0.102 * (it.dayStudents + it.teacher + it.non_teacher)
                            noPupils = dayPupils
                            break
                        case 'boarding':
                            def boardingPupils = 0.204 * (it.boardingStudents + it.teacher + it.non_teacher)
                            noPupils = boardingPupils
                            break
                        case 'day,boarding':
                            def dayPupils = 0.102 * (it.dayStudents + it.teacher + it.non_teacher)
                            def boardingPupils = 0.204 * it.boardingStudents
                            noPupils = dayPupils + boardingPupils
                            break
                        default:
                            def dayPupils = 0.102 * (it.dayStudents + it.teacher + it.non_teacher)
                            def boardingPupils = 0.204 * it.boardingStudents
                            noPupils = dayPupils + boardingPupils
                            break
                    }
                    def carbon_credit_cal =  noPupils * it.days * 0.82 * 0.015 * 81.6
                    return Math.round(carbon_credit_cal)
                },
                fx('period') { return it.year + ' ' + it.term })
        def sortedData = carbon_credit.sort { "$it.year $it.term" }

        sortedData.addColumn(fx('runningTotal') {
            if (it.idx() == 1) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        })

        def removeZeros = sortedData.filter { it.runningTotal != 0 }
        return removeZeros.toMapList()
    }
}

//static def fetchDate() {
//    def query = """
//                    select DATE_FORMAT(min(use_date), "%Y-%m-%d") as date
//                    from `_15_xxx_kitchen_monitoring_form`;
//                    """
//    def date = AppHolder.withMisSqlNonTx { rows(query) }
//    return date
//}
//
//def fetchSchools() {
//    def date = params.date
//    def query = """
//                    select school_name,use_date
//                    from `_15_xxx_kitchen_monitoring_form`
//                    where use_date is not null
//                    having use_date >= '${date}'
//                    order by school_name
//                    """.toString()
//
//    def schools = AppHolder.withMisSqlNonTx { rows(query) }
//    api.render(ctl, [contentType: 'text/json', text: JsonOutput.toJson(schools)])
//}
//
//def fetchData() {
//    def oodate = params.date
//    def queryDate = """
//                    select replace(school_name,"'","''") as school_name,DATE_FORMAT(use_date, "%Y-%m-%d") as use_date
//                    from `_15_xxx_kitchen_monitoring_form`
//                    where use_date is not null
//                    group by school_name
//                    having use_date >= '${oodate}'
//                    order by school_name
//                    """.toString()
//    def schoolDate = AppHolder.withMisSqlNonTx { rows(queryDate) }
//    def schoolValue = params.school
//    def type = params.type
//
//    def schoolArray = []
//    def filterGroup
//    if (schoolValue == 'all') {
//        schoolDate.each {
//            schoolArray << it.school_name as String
//        }
//        def firstJoin = schoolArray.join(",")
//        def split = firstJoin.split(',').toList()
//        def addCommas = split.collect { "'$it'" }
//        filterGroup = addCommas.join(",")
//
//    } else {
//        def split = schoolValue.split(',').toList()
//        def addCommas = split.collect { "'$it'" }
//        filterGroup = addCommas.join(",")
//    }
//
//    def query = """
//                   select
//                  concat_ws('', t.schol_name, t.school_name) as school_name,
//                  `__code`                                   as code,
//                  day as dayStudents,
//                  boarding as boardingStudents,
//                  teacher_no as teacher,
//                  non_teacher_no as non_teacher,
//                  year(date_of_visit)                        as year,
//                  replace(term, '_', ' ')                    as term,
//                  (case
//                   when (day != 0 && boarding != 0)
//                     then 'day/boarding'
//                   when day != 0 && boarding = 0
//                     then 'day'
//                   when day = 0 && boarding != 0
//                     then 'boarding'
//                   else ''
//                   end)                                      as type
//                from `_13_xxx_school_questionnaire` t
//                  inner join
//                  (select
//                     concat_ws('', schol_name, school_name) as school_name2,
//                     max(openxdata_form_data_date_created)  as maxdate
//                   from `_13_xxx_school_questionnaire`
//                   group by concat_ws('', schol_name, school_name), term
//                  ) t1
//                    on concat_ws('', t.schol_name, t.school_name) = t1.school_name2 and t.openxdata_form_data_date_created = t1.maxdate
//                where concat_ws('', t.schol_name, t.school_name) in (${filterGroup})
//                    """.toString()
//
//    def schooldata = AppHolder.withMisSqlNonTx { rows(query) }
//
//    def queryOpDate = """
//                            select DATE_FORMAT(min(use_date), "%Y-%m-%d") as date
//                            from `_15_xxx_kitchen_monitoring_form`
//                            where school_name in (${filterGroup})
//                            """.toString()
//
//    def schoolOpDate = AppHolder.withMisSqlNonTx { rows(queryOpDate) }
//    def schoolOpdatte = schoolOpDate['date'][0]
//    //Declare Variables
//    def finalResult = []
//    def term1Days
//    def term2Days
//    def term3Days
//    def term1MonthRange = [0, 1, 2, 3]
//    def term2MonthRange = [4, 5, 6, 7]
//    def term3MonthRange = [8, 9, 10, 11]
//    def term1StartDate = '01-01'
//    def term1EndDate = '04-30'
//    def term2StartDate = '05-01'
//    def term2EndDate = '08-31'
//    def term3StartDate = '09-01'
//    def term3EndDate = '12-01'
//    def term
//    def daysInLastTerm
//    def daysInCurrentTerm
//    def opDate = schoolOpdatte
//    def currDate = new Date()
//
//    //Get the number of days per term
//    term1Days = Date.parse('MM-dd', term1EndDate.toString()) - Date.parse('MM-dd', term1StartDate.toString())
//    term2Days = Date.parse('MM-dd', term2EndDate.toString()) - Date.parse('MM-dd', term2StartDate.toString())
//    term3Days = Date.parse('MM-dd', term3EndDate.toString()) - Date.parse('MM-dd', term3StartDate.toString())
//
////Get years of starting operational and current year
//    //Determine the current year
//    def currentYear = currDate[Calendar.YEAR]
//
//    //Determine the start year
//    def startYear = Date.parse('yyyy-MM-dd', opDate.toString())[Calendar.YEAR]
//
//    //Force the dates not to exceed the last date fo the term
//    def newEndDateCurrentTerm3 = currentYear + '-' + term3EndDate
//    def newEndDateLastTerm3 = startYear + '-' + term3EndDate
//    def term3endscurrent = Date.parse('yyyy-MM-dd', newEndDateCurrentTerm3)
//    if (currDate > term3endscurrent) {
//        currDate = term3endscurrent
//    }
//    if (opDate > newEndDateLastTerm3) {
//        opDate = newEndDateLastTerm3
//    }
//
////Get the first results using the the operational date
//    if (startYear != currentYear) {
//        //Get the month number to get determine the start term of the operational
//        def getMonthNumber = Date.parse('yyyy-MM-dd', opDate.toString()).month
//
//        //Get the first term of operational
//        if (term1MonthRange.contains(getMonthNumber)) {
//            term = 'term 1'
//        } else if (term2MonthRange.contains(getMonthNumber)) {
//            term = 'term 2'
//        } else if (term3MonthRange.contains(getMonthNumber)) {
//            term = 'term 3'
//        } else {
//            term = ''
//        }
//
//        //Determine the Remaining days in the term of start date
//
//        if (getMonthNumber <= 3) {
//            def newStartDate = startYear + '-' + term1StartDate
//            def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInLastTerm = (term1Days - daysUsed) / term1Days
//        } else if (getMonthNumber >= 4 && getMonthNumber <= 7) {
//            def newStartDate = startYear + '-' + term2StartDate
//            def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInLastTerm = (term2Days - daysUsed) / term2Days
//        } else if (getMonthNumber >= 8) {
//            def newStartDate = startYear + '-' + term3StartDate
//            def daysUsed = Date.parse('yyyy-MM-dd', opDate.toString()) - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInLastTerm = (term3Days - daysUsed) / term3Days
//        } else {
//            daysInLastTerm = 'Nothing'
//        }
//        //Results per term per year
//        def result1 = [year: startYear, term: term, days: daysInLastTerm, date: opDate]
//        finalResult << result1
//
////Determine the last results
//
//        //Get the month number to get determine the start term of the operational
//        def getCurrentMonthNumber = currDate.month
//
//        //Get the first term of operational
//        if (term1MonthRange.contains(getCurrentMonthNumber)) {
//            term = 'term 1'
//        } else if (term2MonthRange.contains(getCurrentMonthNumber)) {
//            term = 'term 2'
//        } else if (term3MonthRange.contains(getCurrentMonthNumber)) {
//            term = 'term 3'
//        } else {
//            term = ''
//        }
//
//        //Determine the Remaining days in the term of start date
//        if (getCurrentMonthNumber <= 3) {
//            def newStartDate = currentYear + '-' + term1StartDate
//            def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInCurrentTerm = daysUsed / term1Days
//        } else if (getCurrentMonthNumber >= 4 && getCurrentMonthNumber <= 7) {
//            def newStartDate = currentYear + '-' + term2StartDate
//            def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInCurrentTerm = daysUsed / term2Days
//        } else if (getCurrentMonthNumber >= 8) {
//            def newStartDate = currentYear + '-' + term3StartDate
//            def daysUsed = currDate - Date.parse('yyyy-MM-dd', newStartDate.toString())
//            daysInCurrentTerm = daysUsed / term3Days
//        } else {
//            daysInCurrentTerm = 'Nothing'
//        }
//
//        //Results per term per year
//
//        def result2 = [year: currentYear, term: term, days: daysInCurrentTerm, date: currDate.format("yyyy-MM-dd")]
//        finalResult << result2
//
////Determine the terms and years in between
//        def betweenYears = (currentYear - startYear) - 1
//        for (def i = betweenYears; i > 0; i--) {
//            def allTerms = ['term 1': (term1Days / term1Days), 'term 2': (term2Days / term2Days), 'term 3': (term3Days / term3Days)]
//            def yearCounter = currentYear - i
//            allTerms.each { k, v ->
//                def date
//                switch (k) {
//                    case 'term 1':
//                        def newTerm1Ends = yearCounter + '-' + term1EndDate
//                        date = newTerm1Ends
//                        break
//                    case 'term 2':
//                        def newTerm2Ends = yearCounter + '-' + term2EndDate
//                        date = newTerm2Ends
//                        break
//                    case 'term 3':
//                        def newTerm3Ends = yearCounter + '-' + term3EndDate
//                        date = newTerm3Ends
//                        break
//                    default:
//                        date = "No term has ended"
//                        break
//                }
//                def results3 = [year: yearCounter, term: k, days: v, date: date]
//                finalResult << results3
//            }
//        }
//
////Determine the terms with in the start date and the current date
//        //Other terms from the starting date in the same year
//        def endOfTerm3StartYear = startYear + '-' + term3EndDate
//        def endOfTerm2StartYear = startYear + '-' + term2EndDate
//        def daysBetween = (Date.parse('yyyy-MM-dd', endOfTerm3StartYear.toString()) - Date.parse('yyyy-MM-dd', opDate.toString())) - 1
//        if (daysBetween > term3Days) {
//            finalResult << [year: startYear, term: 'term 3', days: (term3Days / term3Days), date: endOfTerm3StartYear]
//        }
//
//        if ((daysBetween - term3Days) > term2Days) {
//            finalResult << [year: startYear, term: 'term 2', days: (term2Days / term2Days), date: endOfTerm2StartYear]
//        }
//
//        //Other terms before the current date in the same year
//        def startOfCurrentYearTerm
//        startOfCurrentYearTerm = Date.parse('yyyy-MM-dd', (currentYear + '-' + term1StartDate).toString())
//        def endOfTerm1CurrentYear = currentYear + '-' + term1EndDate
//        def endOfTerm2CurrentYear = currentYear + '-' + term2EndDate
//        def daysWorkingInCurrentYear = (currDate - startOfCurrentYearTerm) - 1
//
//
//        if (daysWorkingInCurrentYear > term1Days) {
//            finalResult << [year: currentYear, term: 'term 1', days: (term1Days / term1Days), date: endOfTerm1CurrentYear]
//        }
//
//        if ((daysWorkingInCurrentYear - term1Days) > term2Days) {
//            finalResult << [year: currentYear, term: 'term 2', days: (term2Days / term2Days), date: endOfTerm2CurrentYear]
//        }
//
//    } else {
//        def newEndDateTerm1 = currentYear + '-' + term1EndDate
//        def newEndDateTerm2 = currentYear + '-' + term2EndDate
//        def newStartDateTerm2 = currentYear + '-' + term2StartDate
//        def newStartDateTerm3 = currentYear + '-' + term3StartDate
//        def term1ends = Date.parse('yyyy-MM-dd', newEndDateTerm1.toString())
//        def term2ends = Date.parse('yyyy-MM-dd', newEndDateTerm2.toString())
//        def term2starts = Date.parse('yyyy-MM-dd', newStartDateTerm2.toString())
//        def term3starts = Date.parse('yyyy-MM-dd', newStartDateTerm3.toString())
//        def startDate = Date.parse('yyyy-MM-dd', opDate.toString())
//
//        if (term1ends >= startDate) {
//            def daystaken = (term1ends - startDate) / term1Days
//            finalResult << [year: currentYear, term: 'term 1', days: daystaken, date: opDate]
//        }
//        if (term2starts <= startDate && term2ends >= startDate) {
//            def daystaken = (term2ends - startDate) / term2Days
//            finalResult << [year: currentYear, term: 'term 2', days: daystaken, date: opDate]
//        }
//        if (term2starts > startDate && term2ends >= startDate) {
//            def firstDiff = (term1ends - startDate)
//            def daystaken = ((term2ends - startDate) - firstDiff - 1) / term2Days
//            finalResult << [year: currentYear, term: 'term 2', days: daystaken, date: newEndDateTerm2]
//        }
//        if (term3starts <= startDate && currDate >= startDate) {
//            def daystaken = (currDate - startDate) / term3Days
//            finalResult << [year: currentYear, term: 'term 3', days: daystaken, date: currDate.format("yyyy-MM-dd")]
//        }
//        if (term3starts > startDate && currDate >= startDate) {
//            def firstDiff = term1ends - startDate
//            def secondDiff = term2ends - term2starts
//            def daystaken = ((currDate - startDate) - firstDiff - secondDiff - 2) / term3Days
//            finalResult << [year: currentYear, term: 'term 3', days: daystaken, date: currDate.format("yyyy-MM-dd")]
//        }
//    }
//
////Combine all the results
//    def returnData
//    def toFuzzy = FuzzyCSVTable.toCSV(finalResult)
//    def filterZeros = toFuzzy.filter { it.days != 0 }
//    def firstSortedData = filterZeros.sort { "$it.year $it.term" }
//    def schoolInfo = FuzzyCSVTable.toCSV(schooldata)
//    def schoolUseDate = FuzzyCSVTable.toCSV(schoolDate)
//    def joinOpDate = schoolInfo.fullJoin(schoolUseDate, 'school_name')
//    def filterAll = joinOpDate.filter { it.dayStudents != null && it.boardingStudents != null }
//
//
//    if (schoolValue == 'all') {
//        def joinAllRecords = []
//        def output = filterAll.toMapList().groupBy { it.school_name }.each { t ->
//            def tempArray
//            tempArray = t.value
//            def perSchool = FuzzyCSVTable.toCSV(tempArray)
//            def perSchoolRecords = perSchoolModifier(perSchool, firstSortedData)
//            def carbonPerSchool = calculateCarbon(perSchoolRecords, type, opDate)
//            carbonPerSchool.each {
//                joinAllRecords << it
//            }
//        }
//        returnData = joinAllRecords
//    } else {
//        def perSchoolRecords = perSchoolModifier(filterAll, firstSortedData)
//        returnData = calculateCarbon(perSchoolRecords, type, opDate)
//    }
//    api.render(ctl, [contentType: 'text/json', text: JsonOutput.toJson(returnData)])
//}
//
//static def getPrev(ArrayList<Map> data, int currentRow, def column) {
//    if (currentRow == 0)
//        return data.get(currentRow)."$column"
//
//    if (data.get(currentRow)."$column" != 0) {
//        return data.get(currentRow)."$column"
//    }
//    getPrev(data, currentRow - 1, column)
//
//}
//
//static def perSchoolModifier(perSchool, firstSortedData) {
//
//    def schoolType = perSchool.filter { return it.type != null }['type'][0]
//    def schoolName = perSchool.filter { return it.school_name != null }['school_name'][0]
//    def schoolCode = perSchool.filter { return it.code != null }['code'][0]
//    def schoolOperationDate = perSchool.filter { return it.use_date != null }['use_date'][0]
//
//
//    def joinedData = firstSortedData.fullJoin(perSchool, 'year', 'term')
//    def modifyRecords = joinedData.modify {
//        set {
//            it.school_name = schoolName
//            it.code = schoolCode
//            it.type = schoolType
//            it.dayStudents = 0
//            it.boardingStudents = 0
//            it.use_date = schoolOperationDate
//            it.teacher = 0
//            it.non_teacher = 0
//        }
//        where {
//            it.school_name in [null]
//        }
//    }
//    def sortAgain = modifyRecords.sort { "$it.year $it.term" }.toMapList()
//    int idx = 0
//    sortAgain.collect { row ->
//        def temp = row
//        temp.dayStudents = getPrev(sortAgain, idx, "dayStudents")
//        temp.boardingStudents = getPrev(sortAgain, idx, "boardingStudents")
//        temp.teacher = getPrev(sortAgain, idx, "teacher")
//        temp.non_teacher = getPrev(sortAgain, idx, "non_teacher")
//        idx++
//        return temp
//    }
//}
//
//static def calculateCarbon(def modifiedRecords, def type, def opDate) {
//    def runningTotal = 0
//    def result = FuzzyCSVTable.toCSV(modifiedRecords)
//    def filterNulls = result.filter { it.days != null }
//    def carbon_credit = filterNulls.addColumn(
//            fx('opdate') { return opDate },
//            fx('total') {
//                def total_carbon
//                switch (type) {
//                    case 'day':
//                        def dayCarbon = 0.102 * (it.dayStudents + it.teacher + it.non_teacher) * 0.82 * 0.015 * 81.60
//                        total_carbon = dayCarbon
//                        break
//                    case 'boarding':
//                        def boardingCarbon = 0.204 * (it.boardingStudents) * 0.82 * 0.015 * 81.60
//                        total_carbon = boardingCarbon
//                        break
//                    case 'day,boarding':
//                        def dayCarbon = 0.102 * (it.dayStudents + it.teacher + it.non_teacher) * 0.82 * 0.015 * 81.60
//                        def boardingCarbon = 0.204 * (it.boardingStudents) * 0.82 * 0.015 * 81.60
//                        total_carbon = dayCarbon + boardingCarbon
//                        break
//                    default:
//                        def dayCarbon = 0.102 * (it.dayStudents + it.teacher + it.non_teacher) * 0.82 * 0.015 * 81.60
//                        def boardingCarbon = 0.204 * (it.boardingStudents) * 0.82 * 0.015 * 81.60
//                        total_carbon = dayCarbon + boardingCarbon
//                        break
//                }
//                def carbon_credit_cal = total_carbon * it.days
//                return Math.round(carbon_credit_cal)
//            },
//            fx('period') { return it.year + ' ' + it.term })
//    def sortedData = carbon_credit.sort { "$it.year $it.term" }
//
//    sortedData.addColumn(fx('runningTotal') {
//        if (it.idx() == 1) {
//            runningTotal = it.total
//        } else {
//            runningTotal += it.total
//        }
//        return runningTotal
//    })
//    def removeZeros = sortedData.filter { it.runningTotal != 0 }
//    return removeZeros.toMapList()
//}