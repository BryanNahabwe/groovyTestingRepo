package SNVTide2

import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonSlurper
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.sum

class TestSnvTide2 {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def indicator = "No. of Farmers utilising services including financial and PDTF training"
        def gender = "male,female"
        def district = "Kiruhura,Isingiro"
        def genderFilter
        def districtFilter
        if (gender) {
            def genderList = gender.split(',').toList()
            if (genderList.size() > 1) {
                genderFilter = ""
            } else {
                genderFilter = gender as String
            }
        } else {
            genderFilter = ""
        }

        if (district) {
            def districtList = district.split(',').toList()
            districtFilter = districtList
        } else {
            districtFilter = ""
        }
        println(districtFilter)

        def queryCalculations = """
                    select stit.id, stit.indicator, calc.script_value, calc.percentage_based
                    from snv_tide_indicators_table stit
                             left join (select stict.indicator_id, stict.percentage_based, stict.gender_based, stict.script_value
                                        from snv_tide_indicator_calculations_table stict
                                                 inner join (select indicator_id, max(date_created) as max_date
                                                             from snv_tide_indicator_calculations_table
                                                             group by indicator_id) tm on tm.indicator_id = stict.indicator_id and tm.max_date = stict.date_created) calc on calc.indicator_id = stit.id
                    where indicator = '${indicator}';
                    """.toString()
        def queryTargets = """
                            select stiqtt.indicator_id, stit.indicator, quarter, quarterly_target, year
                            from snv_tide_indicators_table stit
                                     left join snv_tide_indicator_quarterly_targets_table stiqtt on stiqtt.indicator_id = stit.id
                            where indicator = '${indicator}';
                            """.toString()
        def calculationData = []
        def resultsCalculationData = FuzzyCSVTable.toCSV(sql, queryCalculations).collect { item ->
            def jsonSluper = new JsonSlurper()
            def script_data = jsonSluper.parseText(item['script_value'] as String) as List
            script_data.each {
                if (genderFilter) {
                    if (genderFilter == it['gender']) {
                        calculationData << [indicator: item['indicator'], indicator_id: item['id'], district: it['district'], quarter: it['quarter'], year: it['year'], actual: it['actual'], percentage: item['percentage_based']]
                    }
                } else {
                    calculationData << [indicator: item['indicator'], indicator_id: item['id'], district: it['district'], quarter: it['quarter'], year: it['year'], actual: it['actual'], percentage: item['percentage_based']]
                }
            }
        }
        def newListOfMaps = []
        calculationData.each {
            if (districtFilter) {
                if (it['district'] in districtFilter) {
                    newListOfMaps << it
                }
            } else {
                newListOfMaps << it
            }
        }
        println(newListOfMaps)
        def calculationDataToFuzzy = FuzzyCSVTable.toCSV(newListOfMaps).printTable()
                .autoAggregate('indicator_id', 'indicator', 'quarter', 'year', 'percentage', sum('actual').az('actual')).printTable()
//        def resultsQuarterlyTargets = FuzzyCSVTable.toCSV(sql, queryTargets)
//        def quarterlyTargetsToFuzzy = resultsQuarterlyTargets
//        def joinTables = calculationDataToFuzzy.join(quarterlyTargetsToFuzzy, 'indicator_id', 'indicator', 'quarter', 'year').toMapList()
//        def categories = []
//        def inner_categories = []
//        def actual_values = []
//        def target_values = []
//        def percentage_based = ""
//        def groupData = joinTables.groupBy { (it.quarter + ' ' + it.year) }.collect { t ->
//            t.value.collect {
//                target_values << ["value": "${it.quarterly_target}"]
//                actual_values << ["value": "${it.actual}"]
//                percentage_based = it['percentage']
//            }
//            inner_categories << ["label": t.key.toString()]
//        }
//        categories << ["category": inner_categories]
//        def final_bar_chart_data = [categories: categories, percentage: percentage_based, dataset: [["seriesname": "Actual", "data": actual_values], ["seriesname": "Target", "data": target_values]]]
    }
}
