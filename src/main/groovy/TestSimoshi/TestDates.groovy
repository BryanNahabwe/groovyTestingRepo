package TestSimoshi

import java.util.Calendar
class TestDates {
    static void main(def args){
        def allDates = []
        def to = new Date("1/1/2018")
        def from = new Date('1/31/2018')
        (from..to).each {
            def dates = [:]
            println "${it.format('dd/MM/yyyy').toUpperCase()}"
            dates.put('date',it.format('dd/MM/yyyy').toUpperCase())
            allDates << dates
        }
        println(allDates)
    }
}
