package Nutec

class ReturnPartners {
    static void main(def args) {
        def subRegionParams = 'all'
        def lango = ["alebtong", "amolatar", "apac", "dokolo", "kole", "kwania", "lira", "otuke", "oyam"]
        def acholi = ["agago", "amuru", "gulu", "kitgum", "lamwo", "nwoya", "omoro", "pader"]
        def west_nile = ["adjumani", "arua", "koboko", "maracha", "moyo", "nebbi", "pakwach", "yumbe", "zombo"]


        def subRegionDistricts
        def defaultWhereClause

        switch (subRegionParams) {
            case 'acholi':
                subRegionDistricts = listToString(acholi)
                defaultWhereClause = "where district_a in (${subRegionDistricts})"
                break
            case 'lango':
                subRegionDistricts = listToString(lango)
                defaultWhereClause = "where district_a in (${subRegionDistricts})"
                break
            case 'west nile':
                subRegionDistricts = listToString(west_nile)
                defaultWhereClause = "where district_a in (${subRegionDistricts})"
                break
            default:
                defaultWhereClause = "where district_a is not null"
                break
        }

        println(defaultWhereClause)

        def queryPartner = """select distinct  agribusiness
from `_181_xxx_kpi_monitoringtool`
${defaultWhereClause}
order by agribusiness asc""".toString()
    }



    static def listToString(listToChange){
        def addCommas = listToChange.collect { "'$it'" }
        def asJoined = addCommas.join(",")
        return asJoined
    }
}
