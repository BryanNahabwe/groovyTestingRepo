package Nutec

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx

class NumberOfPalladium {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.cj.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Number of Palladium supported businesses agreeing high quality and gendered business plans"
        })


        def query = """
                    select name_business, district,replace(substring_index(intervention,'_skip',1),'_',' ') as intervention,year(date_onboarded) as year
                    from `_185_xxx_agribusiness_partner`
                    group by name_business
                    order by name_business
                    """

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query.toString())
        def groupByDistrict = fuzzyData.autoAggregate('district', count('name_business').az('total')).toMapList()
        def icon = "fa fa-building"
        def color = "bg-orange"
        def title = "Number of Palladium supported businesses agreeing high quality and gendered business plans"
        def sum_total = (groupByDistrict.sum { it.total } != null) ? groupByDistrict.sum { it.total } : 0
        println(sum_total)

        def groupByIntervention = fuzzyData.autoAggregate('intervention',count('name_business').az('total'))
        def interArray = [:]
        interArray.put('name','Number of Palladium supported businesses agreeing high quality and gendered business plans')
        def collectData = []
        groupByIntervention.toMapList().each {
            collectData << [it.intervention,it.total]
        }
        interArray.put('data',collectData)

        println(interArray)


        def aggregateYears = fuzzyData.autoAggregate('year',count('name_business').az('total')).printTable()
        def targets = targetData.fullJoin(aggregateYears,'year').printTable()
        def modifyTargets = targets.modify {
            set {
                it.total = 0
            }
            where {
                it.total in [null]
            }
        }.filter{ it.year != '2016'}.printTable()

        def runningTotal
        runningTotal = 0
        def innerRadius
        innerRadius = 0
        def raduis
        raduis = 0

        modifyTargets.addColumn(fx('running_tt') {
            if (it.idx() == 0) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        }, fx('raduis') {
            if (it.idx() == 1) {
                raduis = 62
            } else {
                raduis += 16
            }
            return raduis
        }, fx('innerRadius') {
            if (it.idx() == 1) {
                innerRadius = 47
            } else {
                innerRadius += 16
            }
            return innerRadius
        }).printTable()

        def finalResults = []
        def finalTargets
        modifyTargets.toMapList().each { item ->
            def outcome = item.Outcome
            def indicator = item.Indicator
            def interArrayy = interArray
            def target_year =  item.year

            finalResults << ['outcome': outcome, 'indicator': indicator, 'targets': target_year, 'data': data,'interdata':interArrayy]
        }
        finalTargets = ["icon": icon, "sum": sum_total, "color": color, "districts": groupByDistrict,"title":title,"finalTargets": finalResults]
        println(finalTargets)
    }
}
