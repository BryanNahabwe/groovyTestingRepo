import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql

import static fuzzycsv.RecordFx.*

class TestCraftMileStones {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryMilestoneTargets = """
select bct.name_partner,
       bct.partner_id,
       bct.country,
       replace(bct.year,'_', '') as year,
       concat_ws('_', 'target', replace(bct.year, '_', '')) as target_year,
        'No. of farmer group representatives trained in financial management' as milestone,
       sum(ch.training_bc_year_target) as total_target
from _412_xxx_business_champion_targets bct
inner join _412_select_the_training ch
on bct.Id = ch.parentId
where training_type like '%financial_mgt%'
group by bct.partner_id, replace(bct.year, '_', '');
                                    """

        def queryMileStoneValues = """
select
    replace(f.name_partner,'_', ' ') as name_partner,
    f.partner_id,
    f.country,
    year(tt.training_date) as year,
    concat_ws('_', 'value',   year(tt.training_date)) as value_year,
     'No. of farmer group representatives trained in financial management' as milestone,
    count(*) as total_value
from _416_xxx_training_tool_v2 tt
         inner join (select f.openxdata_form_data_id,
                            farmer_details.name_partner,
                            farmer_details.partner_id,
                            farmer_details.country
                     from _416_select_farmer f
                              left join craft_farmer_entity farmer_details on f.farmer_id = farmer_details.farmer_id
                     group by f.openxdata_form_data_id, farmer_details.name_partner, farmer_details.partner_id) f
                    on f.openxdata_form_data_id = tt.openxdata_form_data_id
where type_intervention like '%financial_mgt%'
group by f.partner_id,year(tt.training_date);
                                    """


        def fuzzyMileStoneTargets = FuzzyCSVTable.toCSV(sql, queryMilestoneTargets).printTable()
        def fuzzyMileStonesValues = FuzzyCSVTable.toCSV(sql, queryMileStoneValues).printTable()

        def pivotTargets = fuzzyMileStoneTargets.pivot('target_year', 'total_target', 'milestone', 'name_partner', 'partner_id', 'country', 'year').printTable()
        def pivotValue = fuzzyMileStonesValues.pivot('value_year', 'total_value', 'milestone', 'name_partner', 'partner_id', 'country', 'year').printTable()
        def joinTables = pivotTargets.join(pivotValue, 'partner_id', 'year', 'milestone').printTable()
    }
}
