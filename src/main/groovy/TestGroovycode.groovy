class TestGroovycode {
    static void main(def args) {
        def loggedInUserPartners = ["TANZANIA", "Three sisters oil mills company limited"]

        if (loggedInUserPartners.contains('TANZANIA') && !loggedInUserPartners.contains('Three sisters oil mills company limited')) {
            println("False")
        }
        else {
            println("True")
        }
    }
}
