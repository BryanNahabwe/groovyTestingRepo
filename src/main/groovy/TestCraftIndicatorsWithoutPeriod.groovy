import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.reduce
import static fuzzycsv.FuzzyStaticApi.reduce
import static fuzzycsv.FuzzyStaticApi.reduce
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestCraftIndicatorsWithoutPeriod {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def queryData = """
                        select bc.openxdata_user_name,
                               if(bc.age = '_18_hyphen_24_years' or bc.age = '_25_hyphen_34_years', 'youth', bc.gender) as gender,
                               bc.country,
                               replace(bc.name_partner, '_', ' ')                                                                     as name_partner,
                               period,
                                case
                                           when period = 'review_2020' then '2020'
                                           when period = 'review_2021' then '2021'
                                           when period = 'review_2022' then '2022'
                                           when period = 'review_2023' then '2023'
                                           when period = 'review_2024' then '2024'
                                           else 'baseline'
                                        end as year,
                               season,
                               crops_purchased as crops_produced,
                               1 as total
                        from _391_xxx_business_champion_form bc
                                 left join business_partner bp on bc.partner_id = bp.partner_id
                        where bc.partner_id is not null
                          group by bc.partner_id, period, season, crops_purchased, gender
                        having (CHAR_LENGTH(group_concat(distinct apply_csa_yes separator ' ')) -
                                CHAR_LENGTH(REPLACE(group_concat(distinct apply_csa_yes separator ' '), ' ', ''))) + 1 >= 3
                        """

        def queryTargetData = """
                        select pt.target,
                               pt.year as year_id,
                               si.project_indicators as indicator_id,
                               si.overall_target,
                               si.project_year_target
                        from _413_xxx_project_targets pt
                                 inner join _413_select_the_indicator si on pt.Id = si.parentId
                        """

        def optionTextIndicator = """
                                    select option_id as indicator_id, option_text as indicator from _413_project_indicators group by option_id
                                    """
        def optionTextYear = """
                                    select option_id as year_id, option_text as year from _413_year group by option_id
                                    """

        def fetchData = FuzzyCSVTable.toCSV(sql, queryData)
        def finalTable = fetchData.autoAggregate('country', 'name_partner', 'year',
                sum('total').az('total'))
                .sort { it.name_partner }
                .addColumn(fx('indicator') {
                    return "Number of agribusiness SMEs and cooperatives that have worked to improve key manageable climate risks"
                }).printTable()

        def targetData = FuzzyCSVTable.toCSV(sql, queryTargetData)
        def optionTextIndicatorFuzzy = FuzzyCSVTable.toCSV(sql, optionTextIndicator)
        def optionTextYearFuzzy = FuzzyCSVTable.toCSV(sql, optionTextYear)

        def clearTargetTables = targetData.join(optionTextIndicatorFuzzy, 'indicator_id')
                .join(optionTextYearFuzzy, 'year_id')
                .filter { it.indicator_id == 'indicator16' }
                .renameHeader('indicator', 'given_name')
                .addColumn(fx('indicator') {
                    return "Number of agribusiness SMEs and cooperatives that have worked to improve key manageable climate risks"
                })

        def running_sum = 0
        def groupByIndicator = clearTargetTables.autoAggregate('indicator',
                reduce { group -> group['year'] }.az('year'),
                reduce { group -> group['project_year_target'] }.az('ranges'),
                reduce { group -> group['overall_target'] }.az('markers')).toMapList()
        def targetDataRemoveNulls = groupByIndicator.collect {
            def ranges = []
            ((it.ranges) as List).findAll { it != null }.eachWithIndex { def entry, int i ->
                if (i == 0) {
                    running_sum = entry
                    ranges << running_sum
                } else {
                    running_sum += entry
                    ranges << running_sum
                }
            }
            [
                    'indicator': it.indicator,
                    'year'     : ((it.year) as List).findAll { it != null }.unique { a, b -> a <=> b },
                    'ranges'   : ranges,
                    'markers'  : ((it.markers) as List).findAll { it != null },
            ]
        }
        def targetDataDD = FuzzyCSVTable.toCSV(targetDataRemoveNulls)

        def finalData = finalTable.autoAggregate('indicator', sum('total_indicator').az('total'))
                .autoAggregate('indicator',
                reduce { group -> group['total'] }.az('measures'))
                .join(targetDataDD, 'indicator')
                .renameHeader('indicator', 'title')
                .addColumn(fx('subtitle') { return '' }).printTable() toMapList()
    }
}
