package ImportData

import fuzzycsv.FuzzyCSVTable
import groovy.xml.MarkupBuilder
import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.XML
import static groovyx.net.http.ContentType.XML


def url = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ImportData\\sironko.csv")
def zomboCsv = FuzzyCSVTable.parseCsv(url.text).toMapList()
def zomboVillageData = zomboCsv.each {
    it.each { k, v ->
        it[k] = v.toString().tokenize(" ")*.toLowerCase()*.capitalize().join(" ")
    }
}

println(zomboVillageData)
//def fuzzyData = FuzzyCSVTable.toCSV(zomboVillageData).printTable()

def counter
counter = 1
for (villageData in zomboVillageData) {
    println(villageData)
//
//    def writer = new StringWriter()
//    def xml = new MarkupBuilder(writer)
//    xml.mkp.xmlDeclaration(version: '1.0', encoding: 'UTF-8')
//    xml.sanitation_fund_program_villages_v1(formKey: 'sanitation_fund_program_villages_v1', id: 11, name: 'Village Form', 'xmlns:xsd': "http://www.w3.org/2001/XMLSchema") {
//        district "${villageData.district}"
//        subcounty "${villageData.subcounty}"
//        parish "${villageData.parish}"
//        village "${villageData.village}"
//        unique_id "uuid:${UUID.randomUUID()}"
//    }
//
//    println(writer)
//    println(counter)
//    counter++
//    def lastWriter = new StringWriter()
//    def markupBuilder = new MarkupBuilder(lastWriter)
//    markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8", standalone: "yes")
//    markupBuilder.FormData {
//        data "$writer"
//    }
//
//    def oxd = new HTTPBuilder('http://sanitationug.net:8080/mohsanitationdata/rest/studies/2/forms/11/versions/11/data', 'application/xml')
//    oxd.auth.basic('dho59', 'gamata')
//    oxd.post(contentType: XML, requestContentType: XML, body: lastWriter) { response ->
//        println response.statusLine
//    }
}
