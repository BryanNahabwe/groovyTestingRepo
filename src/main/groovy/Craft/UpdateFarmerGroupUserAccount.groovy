package Craft

import groovy.sql.Sql

class UpdateFarmerGroupUserAccount {
    static void main(def args) {
        def dbName = 'oxdsnv'

        def db = [url     : "jdbc:mysql://localhost:3306/$dbName?autoReconnect=true&useUnicode=true&characterEncoding=UTF8&zeroDateTimeBehavior=convertToNull",
                  user    : "root",
                  password: "root",
                  driver  : "com.mysql.cj.jdbc.Driver"]

        def sql = Sql.newInstance(db.url, db.user, db.password, db.driver)

        def query = "SELECT * FROM `form_data` WHERE form_definition_version_id = 477 AND voided = 0 and creator = 534;"
        sql.eachRow(query) { formData ->
            def creator = formData.creator as String
            println(creator)

            def data = formData.form_data_id as String
            println(data)
        }
    }
}
