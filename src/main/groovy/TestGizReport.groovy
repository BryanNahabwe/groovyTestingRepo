import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.count
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class TestGizReport {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryWater = """
                        select institution_name,
                               municipality,
                               division,
                               total_enrollment,
                               case
                                   when if(two_star = 1 and three_star = 1, 1, 0) = 1 then '3 star'
                                   when if(two_star = 1 and three_star != 1, 1, 0) = 1 then '2 star'
                                   when if(one_star = 1, 1, 0) = 1 then '1 star'
                                 else '0 star'
                               end as water,
                               case
                                   when if(two_star = 1 and three_star = 1, 1, 0) = 1 then 3
                                   when if(two_star = 1 and three_star != 1, 1, 0) = 1 then 2
                                   when if(one_star = 1, 1, 0) = 1 then 1
                                 else 0
                               end as water_star
                        from (select municipality,
                                     division,
                                     institution_name,
                                     sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0)) as total_enrollment,
                                     if(b06_drinking_water = 'no', 1, 0)                     as three_star,
                                     if((b01_water_source in ('spring',
                                                              'dug_well',
                                                              'borehole',
                                                              'public',
                                                              'connection',
                                                              'yard',
                                                              'rainwater',
                                                              'packaged_water',
                                                              'delivered_water')) and
                                        (b04_unsafe_source not like '%no_treatment%') and (b03_source_available = 'yes'), 1, 0) as two_star,
                                     if(b05_no_water_source = 'yes', 1, 0)                   as one_star
                              from _46_xxx_institutions_survey 
                              where institution_type like '%school%' and year(date_entry) like :year
                                and date_entry IN (SELECT max(d2.date_entry) FROM _46_xxx_institutions_survey d2
                                                   where institution_type like '%school%' and year(date_entry) like :year group by  d2.institution_name)
                              group by Id) main
                        where municipality like '%%' and division like '%%'
                        """

        def querySanitation = """
                            select institution_name,
                                   municipality,
                                   division,
                                   total_enrollment,
                                   case
                                       when if(one_star = 1 and two_star = 1 and three_star = 1, 1, 0) = 1 then '3 star'
                                       when if(one_star = 1 and two_star = 1, 1, 0) = 1 then '2 star'
                                       when if(one_star = 1, 1, 0) = 1 then '1 star'
                                       else '0 star'
                                       end as sanitation,
                                   case
                                       when if(one_star = 1 and two_star = 1 and three_star = 1, 1, 0) = 1 then 3
                                       when if(one_star = 1 and two_star = 1, 1, 0) = 1 then 2
                                       when if(one_star = 1, 1, 0) = 1 then 1
                                       else 0
                                       end as sanitation_star
                            from (select municipality,
                                         division,
                                         institution_name,
                                         sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0)) as total_enrollment,
                                         if(c07_male_no > 0 and c08_female_no > 0 and
                                            c11_toilets like
                                            '%clean_toilets_do_not_have_a_strong_smell_or_significant_numbers_of_flies_or_mosquitos_and_there_is_no_visible_faeces_on_the_floor_walls_seat_or_pan_or_around_the_facility%'
                                                and c12_open_defecation = 'no', 1, 0) as one_star,
                                         if((c02_toilet_type like '%lined%' or
                                             c02_toilet_type like '%unlined%' or
                                             c02_toilet_type like '%eco_san%' or
                                             c02_toilet_type like '%pour_flush%' or
                                             c02_toilet_type like '%flush%') and
                                            (d10_sanitary_pads = 'both_emergency_uniforms_and_pads') and
                                            (e01_plan = 'yes') and
                                            (e04_implement = 'yes'), 1, 0)            as two_star,
                                         if((ifnull((sum(ifnull(e05_female, 0)) / (sum(ifnull(c04_female_no_, 0)))), 0) <= 40 and
                                             ifnull((sum(ifnull(e05_female, 0)) / (sum(ifnull(c04_female_no_, 0)))), 0) > 0) and
                                            (ifnull((sum(ifnull(e06_male, 0)) / (sum(ifnull(c05_male_no_, 0)))), 0) <= 40 and
                                             ifnull((sum(ifnull(e06_male, 0)) / (sum(ifnull(c05_male_no_, 0)))), 0)) and
                                            (d09_incinerator = 'yes') and
                                            (d08_change_room = 'yes') and
                                            (c03_disabled = 'yes'), 1, 0)             as three_star                            
                                  from _46_xxx_institutions_survey
                                  where institution_type like '%school%' and year(date_entry) like :year
                                                                  and date_entry IN (SELECT max(d2.date_entry) FROM _46_xxx_institutions_survey d2
                                                   where institution_type like '%school%' and year(date_entry) like :year group by  d2.institution_name)
                                  group by Id) main
                            where municipality like '%%' and division like '%%'
                            """

        def queryHygiene = """
                            select institution_name,
                                  municipality,
                                   division,
                                   total_enrollment,
                                   case
                                       when if(one_star = 1 and two_star = 1 and three_star = 1, 1, 0) = 1 then '3 star'
                                       when if(one_star = 1 and two_star = 1, 1, 0) = 1 then '2 star'
                                       when if(one_star = 1, 1, 0) = 1 then '1 star'
                                       else '0 star'
                                       end   as hygeine,
                                   case
                                       when if(one_star = 1 and two_star = 1 and three_star = 1, 1, 0) = 1 then 3
                                       when if(one_star = 1 and two_star = 1, 1, 0) = 1 then 2
                                       when if(one_star = 1, 1, 0) = 1 then 1
                                       else 0
                                       end   as hygeine_star                                     
                            from (select institution_name,
                                         municipality,
                                         division,
                                         sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0)) as total_enrollment,
                                         if(d06_bathing = 'yes', 1, 0)           as one_star,
                                         if(d03_group_activity = 'at_least_once_per_school_day', 1, 0) as two_star,
                                         if(ifnull(((sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0))) / (sum(ifnull(d02_hand_wash_no, 0)))), 0) <=
                                            40 and
                                            ifnull(((sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0))) / (sum(ifnull(d02_hand_wash_no, 0)))), 0) > 0,
                                            1, 0)                                as three_star
                                  from _46_xxx_institutions_survey
                                  where institution_type like '%school%' and year(date_entry) like :year
                                                                  and date_entry IN (SELECT max(d2.date_entry) FROM _46_xxx_institutions_survey d2
                                                   where institution_type like '%school%' and year(date_entry) like :year group by  d2.institution_name)
                                  group by Id) main
                            where municipality like '%%' and division like '%%'
                            """

        def queryOthers = """
                             select institution_name,
                                    municipality,
                                    division,
                                    total_enrollment,
                                   handwash,
                                   stance,
                                   sanitation_facilities,
                                   menstrual_hygiene
                            from (select institution_name,
                                         municipality,
                                         division,
                                         sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0)) as total_enrollment,
                                         if(d05_water_available like '%both_water_and_soap%', 'Yes', 'X')                                     as handwash,
                                          if(ifnull(((sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0))) /
                                                    (sum(ifnull(c04_female_no_, 0)) + sum(ifnull(c05_male_no_, 0)) +
                                                     sum(ifnull(c06_gender_shared, 0)))), 0) <= 40 and
                                            ifnull(((sum(ifnull(e05_female, 0)) + sum(ifnull(e06_male, 0))) /
                                                    (sum(ifnull(c04_female_no_, 0)) + sum(ifnull(c05_male_no_, 0)) +
                                                     sum(ifnull(c06_gender_shared, 0)))), 0) > 0, 'Yes', 'X')             as stance,
                                         if(((c01_toilet like '%yes%') or
                                             if((c04_female_no_ != 0 or c05_male_no_ != 0 or c06_gender_shared != 0), 1, 0) = 1), 'Yes',
                                            'X')                                                                                              as sanitation_facilities,
                                         if(((d08_change_room like '%yes%') and (d09_incinerator like '%yes%')
                                             and (d10_sanitary_pads like '%pads_only%' or
                                                  d10_sanitary_pads like '%both_emergency_uniforms_and_pads%' or
                                                  d10_sanitary_pads like '%emergency_uniforms_only%')), 'Yes', 'X')                                        as menstrual_hygiene
                                  from _46_xxx_institutions_survey
                                  where institution_type like '%school%' and year(date_entry) like :year
                                                                  and date_entry IN (SELECT max(d2.date_entry) FROM _46_xxx_institutions_survey d2
                                                   where institution_type like '%school%' and year(date_entry) like :year group by  d2.institution_name)
                                  group by Id) main
                            where municipality like '%%' and division like '%%'
                            """


        def fuzzyWater = FuzzyCSVTable.toCSV(sql, queryWater)
        def fuzzySanitation = FuzzyCSVTable.toCSV(sql, querySanitation)
        def fuzzyHygiene = FuzzyCSVTable.toCSV(sql, queryHygiene)
        def fuzzyOthers = FuzzyCSVTable.toCSV(sql, queryOthers)


        def data = fuzzyWater.join(fuzzySanitation, 'institution_name', 'municipality', 'division', 'total_enrollment')
                .join(fuzzyHygiene, 'institution_name', 'municipality', 'division', 'total_enrollment')
                .join(fuzzyOthers, 'institution_name', 'municipality', 'division', 'total_enrollment')
                .addColumn(fx('overall_summation') {
                    return (it.water_star + it.sanitation_star + it.hygeine_star)
                })
                .addColumn(fx('overall_average') {
                    return Math.round(((it.water_star + it.sanitation_star + it.hygeine_star) / 3) as Float)
                })
                .addColumn(fx('overall') {
                   def star_level
                    if (it.overall_average == 3) {
                        star_level = '3 star'
                    }
                    else if (it.overall_average == 2) {
                        star_level = '2 star'
                    }
                    else if (it.overall_average == 1) {
                        star_level = '1 star'
                    }
                    else {
                        star_level = '0 star'
                    }
                    return star_level
                }).delete('water_star', 'sanitation_star', 'hygeine_star', 'overall_summation', 'overall_average').printTable()
    }
}
