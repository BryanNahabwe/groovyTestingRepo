import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonOutput
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class CltsFormData {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "", 'com.mysql.jdbc.Driver')

        def districtParams = 'Dokolo'
        def split = districtParams.split(',').toList()
        def addCommas = split.collect { "'$it'" }

        def asJoined = addCommas.join(",")

        def query1 = """select t2.period,sum(t2.total) as total
from `_21_xxx_clts_and_odf_verification_form` t1
  inner join (
    select distinct village,count(distinct village) as total,openxdata_form_data_id,year(trigger_date) as period,activity as activity
    from `_21_xxx_clts_and_odf_verification_form`
    where activity='trigger' and trigger_date is not null and district in (${asJoined})
    group by village) as t2 on t1.openxdata_form_data_id = t2.openxdata_form_data_id
group by t2.period
order by t2.period asc""".toString()

        def query2 = """select t2.period,sum(t2.total) as total
from `_21_xxx_clts_and_odf_verification_form` t1
  inner join (
    select distinct village,count(distinct village) as total,openxdata_form_data_id,year(trigger_date) as period,activity as activity
    from `_21_xxx_clts_and_odf_verification_form`
    where activity='verify_odf_village' and trigger_date is not null and district in (${asJoined})
    group by village) as t2 on t1.openxdata_form_data_id = t2.openxdata_form_data_id
group by t2.period
order by t2.period asc""".toString()

        def query3 = """select count(distinct village) as total
from `_21_xxx_clts_and_odf_verification_form`
where district in (${asJoined})
""".toString()

        def triggerData = FuzzyCSVTable.toCSV(sql, query1).printTable()
        def odfData = FuzzyCSVTable.toCSV(sql, query2)
        def totalVillages = FuzzyCSVTable.toCSV(sql, query3)

        def totalHousehold = totalVillages['total'][0]
        def results = []
        def runningTotal = 0
        def data1 = []
        def data3 = []
        def data2 = []
        def trigger
        def odf
        def villages

        //Villages Triggered
        triggerData.addColumn(fx('runningTotal') {
            if (it.idx() == 1) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        })
        triggerData.each {
            data1 << [it.period, it.runningTotal]
        }

        trigger = [name: 'Triggered', data: data1]
        results << trigger

        //ODF DECLARED
        odfData.addColumn(fx('runningTotal') {
            data3 << [it.period, totalHousehold]
            if (it.idx() == 1) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        })
        odfData.each {
            data2 << [it.period, it.runningTotal]
        }

        odf = [name: 'ODF', data: data2]
        results << odf

        //All distinct villages which are triggered and declared odf
        villages = [name: "Total Villages", data: data3]
        results << villages
        println(JsonOutput.toJson(results))
    }
}
