package TestIssdTargets

import fuzzycsv.FuzzyCSVTable

static def getCropGroup(){
    def url = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ISSD\\crop_group.csv")
    return FuzzyCSVTable.parseCsv(url.text)
}