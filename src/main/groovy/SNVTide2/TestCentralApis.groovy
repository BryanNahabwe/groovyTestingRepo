package SNVTide2

import groovy.json.JsonOutput
import groovy.xml.XmlUtil

class TestCentralApis {
    static void main(def args) {
        def requestHeaders = [:]
        requestHeaders."Content-Type" = "application/xml"
        try {
            def centralService = new CentralService()
            centralService.auth()
            def token = centralService.get()
            def centralSubmissionXml = RestHelper.withCentral {
                requestHeaders."Authorization" = "Bearer ${token}"
                def response = client.get path: "/v1/projects/3/forms/acf__activity_form/submissions/uuid:70fbfdee-9cde-44dd-b83b-8f6fd256837a.xml", headers: requestHeaders
                if (response.data) {
                    println("Content Type: " + response.contentType)
                    def person = XmlUtil.serialize(response.data)
                    println(person)
                }
            }

        }
        catch (Exception e) {
            println("Exception: " + e.getMessage())
        }

    }
}
