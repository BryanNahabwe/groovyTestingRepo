import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class TestEntityUpdateer {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/abimis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def query = """
                            select
                                   fg.openxdata_user_name,
                                   openxdata_form_data_date_created,
                                   unique_id,
                                   fg.reference,
                                   fg.farmer_grp,
                                   fg.district,
                                   fg.sub_county,
                                   fg.village,
                                   fg.ip_name,
                                   fg.value_chain
                            from _4_xxx_farmer_group_register fg
                     """

        def queryCheckIfIdAlreadyExists = """
                                        select farmer_group_id from abi_farmer_group_entity_codes
                                     """

        def fuzzy_data = FuzzyCSVTable.toCSV(sql, query).printTable().toMapList()
        def checkIfIdAlreadyExists = FuzzyCSVTable.toCSV(sql, queryCheckIfIdAlreadyExists)
        fuzzy_data.each {
            def reference = it['reference']
            def fg_name = it['farmer_grp'].toString().replace(' ', '').replace('_', '').toUpperCase()
            println(fg_name)
            def first_letter = fg_name.charAt(0)
            def positions = (fg_name.substring(1, fg_name.length()).split("(?!^)") as List).unique()
            def newStringGenerated = generateRandomString(positions)
            def returned_string = newStringGenerated.toString().toUpperCase()
            def farmer_group_id = (reference + '/' + first_letter.toString().concat(returned_string.toString()))
            println(farmer_group_id.toString())

        }
    }
    static def generateRandomString(def positions) {
        def random = new Random()
        def newString = ""
        for (def i = 1; i <= 3; i++) {
            def randomPositionOfLetter
            randomPositionOfLetter = random.nextInt(positions.size())
            newString = newString + positions.get(randomPositionOfLetter)
            positions.remove(randomPositionOfLetter)
        }
        return newString.toString().toUpperCase()
    }
}
