package TestYoba

import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*
import groovy.sql.Sql

class IncidenceReport {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "", 'com.mysql.jdbc.Driver')

        def school = ''
        def query = """
                        select p.child_name,p.child_class,p.`__code` as code,replace(p.school_name,'_',' ') as school_name,ch.disease_diagnosis,p.medicine_use,date_format(p.date_,'%Y/%m/%d') as date
                        from `_352_xxx_incidence_of_disease_form` p
                               inner join (select id,
                                                  child_name,
                                                  `__code`                 as code,
                                                  SUBSTRING_INDEX(SUBSTRING_INDEX(`_352_xxx_incidence_of_disease_form`.disease_diagnosis, ' ', numbers.n),
                                                                  ' ', -1) as disease_diagnosis
                                           from (select 1 n
                                                 union all
                                                 select 2
                                                 union all select 3
                                                 union all
                                                 select 4
                                                 union all select 5) numbers
                                                  INNER JOIN `_352_xxx_incidence_of_disease_form`
                                                    on CHAR_LENGTH(`_352_xxx_incidence_of_disease_form`.disease_diagnosis)
                                                         - CHAR_LENGTH(REPLACE(`_352_xxx_incidence_of_disease_form`.disease_diagnosis, ' ', '')) >=
                                                       numbers.n - 1
                                           order by disease_diagnosis, n) ch on p.`id` = ch.id
                        where replace(school_name,'_',' ') like '%${school}%'
                        order by 1 asc
                        """

        def query2  =   """
                        select `__code` as code,replace(measure_count,'_',' ') as measure
                        from `_351_xxx_anthropometric_measurement_form`
                        """
        def query3 =   """
                        select school_name,replace(type,'_',' ') as type
                        from `_356_xxx_school_register`
                        """

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query)
        def aggregateData = fuzzyData.autoAggregate('disease_diagnosis', 'date', count('disease_diagnosis').az('count'))
        def sortedData = aggregateData.sort { "$it.date $it.disease_diagnosis" }
        def transposedData = sortedData.transpose('date', 'count', 'disease_diagnosis').printTable()
        def transfromdata = transposedData.transform { it == null ? 0 : it }.printTable()

    }
}
