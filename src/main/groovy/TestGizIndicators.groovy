import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import fuzzycsv.Reducer
import groovy.json.JsonSlurper
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.count
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.*

class TestGizIndicators {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def query = """
                    select municipality,
                           division,
                           parish,
                           cell,
                           c04_toilet_type,
                           'household' as form,
                           if((c04_toilet_type like '%unlined%' or c04_toilet_type like '%pit_latrine%'), 1, 0) as count
                    from _44_xxx_household_survey;
                    """
        def queryLocations = """
                    SELECT lower(h.municipality) as municipality,
                           lower(h.division) as division,
                            h.a10_location as location,
                           cast(substring_index(substring_index(REPLACE( h.a10_location, ' ', ',' ), ',', 1 ), ',', - 1 ) AS DECIMAL ( 20, 15 ))AS `latitude`,
                           cast(substring_index(substring_index(REPLACE( h.a10_location, ' ', ',' ), ',', 2 ), ',', - 1 ) AS DECIMAL ( 20, 15 )) AS `longitude`,
                           if((c04_toilet_type like '%unlined%' or c04_toilet_type like '%pit_latrine%'), 1, 0) as count
                    FROM
                        `_44_xxx_household_survey` h
                    """

        def data = FuzzyCSVTable.toCSV(sql, query)
        def townData = data

        def groupByForm = townData.addColumn(fx('tths'){ return 1 })
                .autoAggregate('form', sum('count').az('count_records'),sum('tths').az('tths'))
                .addColumn(fx('y') {
                    return (((it.count_records / it.tths) * 100) as double).round(2)
                }).delete('count_records', 'tths')
        def groupByMunicipalityForm = townData.addColumn(fx('tths'){ return 1 })
                .autoAggregate('municipality','division', 'form', sum('count').az('count_records'),sum('tths').az('tths'))
                .addColumn(fx('percentage') {
                    return (((it.count_records / it.tths) * 100) as double).round(2)
                })
                .addColumn(fx('indicator') {
                    return "Percentage of HHs using safely managed sanitation services"
                })
                .join(groupByForm, 'form').toMapList()

        def result = [townDataAnalysis: groupByMunicipalityForm]
    }
}
