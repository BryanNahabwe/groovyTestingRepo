package ISSD

import fuzzycsv.Fuzzy
import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql

class TestPlantingRequest {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')


        def query1 = """
                        select t1.name,
                               t1.`__code`                                                  as lsbId,
                               t2.contact_person,
                               t2.contact,
                               t2.zone,
                               t1.district,
                               replace(t1.subcounty,'_',' ') as subcounty,
                               DATE_FORMAT(t1.request_date_, '%Y-%m-%d')                    as date,
                               replace(t1.seasons, '_', '')                                 as season,
                               t1.crop,
                               replace(
                                   concat_ws('', t1.variety_rice, t1.variety_beans, t1.variety_climb, t1.variety_groundnut, t1.variety_cassava,
                                             t1.variety_simsim, t1.variety_soybean, t1.variety_sorghum, t1.variety_irish_potato, t1.variety_maize,
                                             t1.variety_sweet_potato, t1.variety_finger_millet, t1.variety_green_gram, t1.variety_pasture,
                                             t1.variety_cowpea, t1.variety_pigeon), '_', ' ') as variety,
                               t1.seed_class,
                               case crop
                                 when 'cassava' then t1.quantity_needed_bags * 40
                                 when 'irishpotato' then t1.quantity_needed_bags * 80
                                 when 'groundnut' then t1.quantity_needed_bags * 42
                                 when 'sweetpotato' then t1.quantity_needed_bags * 30
                                 else t1.quantity_needed_kg
                                 end                                                      as quantity_ordered
                        from `_15_xxx_egs_request_form` t1
                               inner join (select name_lsb, contact_person, contact, `__code`, zone, location
                                           from `_3_xxx_lsb_profile_form`) as t2
                                          on t1.`__code` = t2.`__code`
                        """

        def query2 = """
                        select
                               lsb_code as lsbId,
                               crop as crop,
                               replace(crop_variety,'_',' ') as variety,
                               case crop
                                      when 'cassava' then quantity_acessed_bag * 40
                                      when 'irishpotato' then quantity_acessed_bag * 80
                                      when 'groundnut' then quantity_acessed_bag * 42
                                      when 'sweetpotato' then quantity_acessed_bag * 30
                                      else quantity_acessed_kg
                                      end                                                      as quantity_purchased,
                               case crop
                                      when 'cassava' then price_paid_bag / 40
                                      when 'irishpotato' then price_paid_bag / 80
                                      when 'groundnut' then price_paid_bag / 42
                                      when 'sweetpotato' then price_paid_bag / 30
                                      else price_paid_bag_kg
                                      end                                                      as priceperkg
                        from `_22_xxx_egs_purchase_form`
                        order by lsb_code
                        """


        def fuzzy1 = FuzzyCSVTable.toCSV(sql,query1)
        def fuzzy2 =  FuzzyCSVTable.toCSV(sql,query2)

        def fulljoin = fuzzy1.leftJoin(fuzzy2,'lsbId','crop','variety')
        def addColumnMet = fulljoin.addColumn(RecordFx.fx ('fs_met') {
           def value =  (it.quantity_purchased == null) ? null :   (it.quantity_purchased/ it.quantity_ordered) * 100
            return value
        }).printTable()
    }
}
