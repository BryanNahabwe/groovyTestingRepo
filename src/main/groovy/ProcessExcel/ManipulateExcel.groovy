package ProcessExcel

import fuzzycsv.FuzzyCSV
import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql
import org.apache.poi.ss.usermodel.WorkbookFactory
import fuzzycsv.Excel2Csv
import fuzzycsv.CSVToExcel

class ManipulateExcel {

    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mirpmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def query = """
                    select mhe.household_name,
                           mhe.hh_id
                    from mirp_household_tag mht
                             left join mirp_user_tag mut on mht.tag_id = mut.tag_id
                             left join mirp_household_entity mhe on mhe.hh_id = mht.hh_id
                    where tag_type = 'VLSA Group';
                    """
        def fuzzyData = FuzzyCSVTable.toCSV(sql, query).renameHeader('household_name', 'Beneficiary Name').renameHeader('hh_id', 'HH ID')
        def inputStream = new FileInputStream("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ProcessExcel\\Input Distribution template.xlsx")
        def wb = WorkbookFactory.create(inputStream)
        def sheet = wb.getSheetAt(0)
        def fuzzycsv = Excel2Csv.sheetToCsv(sheet).printTable()
        def processed_data = fuzzyData.leftJoin(fuzzycsv, 'Beneficiary Name', 'HH ID').printTable()
        CSVToExcel.exportToExcelFile("data": processed_data, "D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ProcessExcel\\Input Distribution template.xlsx")
    }
}
