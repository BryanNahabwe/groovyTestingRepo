package TestSimoshi

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.RecordFx.fx


class CarbonCreditCalculation {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/simoshimis', 'root', "", 'com.mysql.jdbc.Driver')

        def querySchools = """
                            select distinct concat_ws('',school_name,schol_name) as school_name
                            from `_13_xxx_school_questionnaire`
                            where concat_ws('',school_name,schol_name) is  not null 
                            """

        def school = FuzzyCSVTable.toCSV(sql, querySchools)
        def eachSchool = 'Agro Links Academy Namasuba'

        def querySchoolInfo = """
                        select concat_ws('',school_name,schol_name) as school_name,
                               `__code` as code,
                               boarding,
                               day,
                               teacher_no,
                               non_teacher_no,
                               date_format(date_of_visit,'%Y-%m-%d') as date
                        from `_13_xxx_school_questionnaire`
                        where concat_ws('',school_name,schol_name) like "%${eachSchool}%"
                        order by date_format(date_of_visit,'%Y-%m-%d') asc
                        """.toString()

        def queryOpDate = """
                            select DATE_FORMAT(min(use_date), '%Y-%m-%d') as date
                            from `_15_xxx_kitchen_monitoring_form`
                            where school_name like "%${eachSchool}%"
                            """.toString()

        def schoolInfo = FuzzyCSVTable.toCSV(sql, querySchoolInfo).printTable()
        def schoolOpDate = FuzzyCSVTable.toCSV(sql,queryOpDate)
        def schoolOpDateValue = new Date().parse('yyyy-MM-dd', (schoolOpDate['date'][0]).toString())
        def dateto = new Date()

        def allDates = []
        (schoolOpDateValue..dateto).each {
            def dates = [:]
            dates.put('date', it.format('yyyy-MM-dd').toUpperCase())
            allDates << dates
        }

        def firstDate = new Date().parse('yyyy-MM-dd', (schoolInfo['date'][0]).toString())

        if (schoolOpDateValue < firstDate){
            def defaultSchoolName = schoolInfo['school_name'][0]
            def defaultSchoolCode = schoolInfo['code'][0]
            def defaultDayValue = schoolInfo['day'][0]
            def defaultBoardingValue = schoolInfo['boarding'][0]
            def defaultTeacherNo = schoolInfo['teacher_no'][0]
            def defaultNonTeacherNo = schoolInfo['non_teacher_no'][0]
            def newDate = schoolOpDateValue.format('yyyy-MM-dd')
            def addNewEmptyRecord = schoolInfo.copy().appendEmptyRecord()
            def modifyRecords = addNewEmptyRecord.modify {
                set {
                    it.school_name = defaultSchoolName
                    it.code = defaultSchoolCode
                    it.day = defaultDayValue
                    it.boarding = defaultBoardingValue
                    it.teacher_no = defaultTeacherNo
                    it.non_teacher_no = defaultNonTeacherNo
                    it.date = newDate
                }
                where {
                    it.school_name in [null]
                }
            }
            def sortData = modifyRecords.sort { "$it.date" }.printTable().toMapList()
            calculateCarbonCredit(sortData,allDates)
        }
        else if (schoolOpDateValue > firstDate){
            def newDate = schoolOpDateValue.format('yyyy-MM-dd')
            def addNewEmptyRecord = schoolInfo.copy().appendEmptyRecord()
            def modifyRecords = addNewEmptyRecord.modify {
                set {
                    it.school_name = 0
                    it.code = 0
                    it.day = 0
                    it.boarding = 0
                    it.teacher_no = 0
                    it.non_teacher_no = 0
                    it.date = newDate
                }
                where {
                    it.school_name in [null]
                }
            }
            def sortData = modifyRecords.sort { "$it.date" }.toMapList()
            //Return the previous values for the current date
            int idx = 0
            sortData.collect { row ->
                def temp = row
                temp.school_name = getPrev(sortData, idx, "school_name")
                temp.code = getPrev(sortData, idx, "code")
                temp.day = getPrev(sortData, idx, "day")
                temp.boarding = getPrev(sortData, idx, "boarding")
                temp.teacher_no = getPrev(sortData, idx, "teacher_no")
                temp.non_teacher_no = getPrev(sortData, idx, "non_teacher_no")
                idx++
                return temp
            }
            def fuzz= FuzzyCSVTable.toCSV(sortData)
            def filterOtherDates = fuzz.filter { it.date >= newDate}.toMapList()
            calculateCarbonCredit(filterOtherDates,allDates)
        }
        else {
            def fuzzyInfo = schoolInfo.toMapList()
            calculateCarbonCredit(fuzzyInfo,allDates)
        }
    }

    static def calculateCarbonCredit(def fuzzyCsv,def datesFuzzy){
        //Calculate Carbon Credit
        def fuzzyInfo = FuzzyCSVTable.toCSV(fuzzyCsv)
        def calcCarbon = fuzzyInfo.addColumn(fx('carbon_credit') {
            def dayPupils = 0.102 * (it.day + it.teacher_no + it.non_teacher_no) * 0.82 * 0.015 * 81.6
            def boardingPupils = 0.204 * it.boarding * 0.82 * 0.015 * 81.6
            def noOfPupils = dayPupils + boardingPupils
            def carbon_credit_cal = (noOfPupils) / 365
            return carbon_credit_cal
        })

//        def runningTotal = 0
         def fuzzyDates = FuzzyCSVTable.toCSV(datesFuzzy)
         def joinTables = fuzzyDates.fullJoin(calcCarbon,'date')
         def modifyRecords = joinTables.modify {
            set {
                it.school_name = 0
                it.code = 0
                it.day = 0
                it.boarding = 0
                it.teacher_no = 0
                it.non_teacher_no = 0
                it.carbon_credit = 0
            }
            where {
                it.school_name in [null]
            }
        }.toMapList()

        //Return the previous values for the current date
        int idx = 0
        modifyRecords.collect { row ->
            def temp = row
            temp.school_name = getPrev(modifyRecords, idx, "school_name")
            temp.code = getPrev(modifyRecords, idx, "code")
            temp.day = getPrev(modifyRecords, idx, "day")
            temp.boarding = getPrev(modifyRecords, idx, "boarding")
            temp.teacher_no = getPrev(modifyRecords, idx, "teacher_no")
            temp.non_teacher_no = getPrev(modifyRecords, idx, "non_teacher_no")
            temp.carbon_credit = getPrev(modifyRecords, idx, "carbon_credit")
            idx++
            return temp
        }
        def runningTotal = 0
        def fillPreviousData = FuzzyCSVTable.toCSV(modifyRecords)
        def addRunningSum = fillPreviousData.addColumn(fx('running_sum') {
            if (it.idx() == 0) {
                runningTotal = it.carbon_credit
            } else {
                runningTotal += it.carbon_credit
            }
            return runningTotal
        })
    }

    static def getPrev(ArrayList<Map> data, int currentRow, def column) {
        if (currentRow == 0)
            return data.get(currentRow)."$column"

        if (data.get(currentRow)."$column" != 0) {
            return data.get(currentRow)."$column"
        }
        getPrev(data, currentRow - 1, column)

    }
}
