package Nutec

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import java.text.DecimalFormat
import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class ChangeInSales {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Change in sales of AgDevCo and Palladium supported businesses attributed to project interventions"
        }).modify { set { it.year_month = '16-Sep' } where { it.year_month in ['Sep-16'] } }
                .modify { set { it.year_month = '17-Mar' } where { it.year_month in ['Mar-17'] } }
                .modify { set { it.year_month = '18-Mar' } where { it.year_month in ['Mar-18'] } }
                .modify { set { it.year_month = '19-Mar' } where { it.year_month in ['Mar-19'] } }
                .modify { set { it.year_month = '20-Mar' } where { it.year_month in ['Mar-20'] } }
                .modify { set { it.year_month = '21-Mar' } where { it.year_month in ['Mar-21'] } }
                .modify { set { it.year_month = '21-Dec' } where { it.year_month in ['Dec-21'] } }
        def query = """
                    select intervention                                      as inter,
                            lower(case
                                  when agribusiness = 'East Africa Seeds Ltd (Sunflower )' then 'East African Seeds'
                                  when agribusiness = 'Latyeng Commercial Farm Ltd (49ers Farms Ltd)' then 'Latyeng Commercial Farm Ltd'
                                  when agribusiness = "Mount Meru Miller's  (Uganda) Ltd" then 'Mount Meru Millers Ltd'
                                  when agribusiness = 'Sun Stand Still Limited' then 'Sun Stand Still'
                                  when agribusiness = 'Umba Veterinary Services' then 'Umber Veterinary'
                                  when agribusiness = 'Lira Resort Enterprises Limited' then 'lira Resort'
                                  when agribusiness = "Mount Meru Miller's  (Uganda) Ltd" then 'Mount Meru Millers Ltd'
                                  when agribusiness = 'Xclusive Farm Nam amuru Ltd' then 'xclusive stevia'
                            else agribusiness end) as partner,
                           district_a                                        as district,
                           substring_index(reporting_year, '_', -1)          as year,
                                case
                                     when  date(openxdata_form_data_date_created) < date('2016-09-01') then '16-Sep'
                                     when  date(openxdata_form_data_date_created) between date('2016-09-01') and date('2017-03-31') and reporting_year != 'base' then '17-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2017-04-01') and date('2018-03-31') and reporting_year != 'base' then '18-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2018-04-01') and date('2019-03-31') and reporting_year != 'base' then '19-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2019-04-01') and date('2020-03-31') and reporting_year != 'base' then '20-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2020-04-01') and date('2021-03-31') and reporting_year != 'base' then '21-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2021-04-01') and date('2021-12-31') and reporting_year != 'base' then '21-Dec'
                                     when  reporting_year = 'base'  then 'base'
                                     else 'no year month' end as year_months,
                           reporting_quarter                                 as quarter_year,
                           concat_ws('_', reporting_quarter, reporting_year) as period,
                           ((((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(sunflower_0il_sold, 0)) * sum(ifnull(sunflower_0il_sold_price, 0))) +
                               (sum(ifnull(sunflower_cake_sold, 0)) * sum(ifnull(sunflower_cake_sold_price, 0))) +
                               (sum(ifnull(sunflower_seed_sold_villagemodel, 0)) * sum(ifnull(sunflower_seed_sold_price_villagemodel, 0))) +
                               (sum(ifnull(sunflower_grain_sold, 0)) * sum(ifnull(sunflower_grain_sold_price, 0))) +
                               (sum(ifnull(cotton_0il_sold, 0)) * sum(ifnull(cotton_0il_sold_price, 0))) +
                               (sum(ifnull(cotton_cake_sold, 0)) * sum(ifnull(cotton_cake_sold_price, 0))) +
                               (sum(ifnull(cotton_grain_sold, 0)) * sum(ifnull(cotton_grain_sold_price, 0))) +
                               (sum(ifnull(cotton_lint_sold, 0)) * sum(ifnull(cotton_lint_sold_price, 0))) +
                               (sum(ifnull(soybean_0il_sold, 0)) * sum(ifnull(soybean_0il_sold_price, 0))) +
                               (sum(ifnull(soybean_cake_sold, 0)) * sum(ifnull(soybean_cake_sold_price, 0))) +
                               (sum(ifnull(soybean_grain_sold, 0)) * sum(ifnull(soybean_grain_sold_price, 0))) +
                               (sum(ifnull(sorghum_grain_sold, 0)) * sum(ifnull(sorghum_grain_sold_price, 0))) +
                               (sum(ifnull(simsim_grain_sold, 0)) * sum(ifnull(simsim_grain_sold_price, 0))) +
                               (sum(ifnull(maize_grain_sold, 0)) * sum(ifnull(maize_grain_sold_price, 0))) +
                               (sum(ifnull(millet_grain_sold, 0)) * sum(ifnull(millet_grain_sold_price, 0))) +
                               (sum(ifnull(bean_grain_sold, 0)) * sum(ifnull(bean_grain_sold_price, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(sunflower_seed_sold, 0)) * sum(ifnull(sunflower_seed_sold_price, 0))) +
                               (sum(ifnull(soybean_seed_sold, 0)) * sum(ifnull(soybean_seed_sold_price, 0))) +
                               (sum(ifnull(inoculants_vol, 0)) * sum(ifnull(inoculants_price, 0))) +
                               (sum(ifnull(tsp_ertilizer_vol, 0)) * sum(ifnull(tsp_ertilizer_price, 0))) +
                               (sum(ifnull(others_vol, 0)) * sum(ifnull(others_price, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(namache_vol, 0)) * sum(ifnull(namaches_price, 0))) +
                               (sum(ifnull(namache_vol_grain, 0)) * sum(ifnull(namaches_price_grain, 0))) +
                               (sum(ifnull(namache_vol_husk, 0)) * sum(ifnull(namaches_price_husk, 0))) +
                               (sum(ifnull(namache_vol_bran, 0)) * sum(ifnull(namaches_price_bran, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(chicken_vol_rawmaterialsupply, 0)) * sum(ifnull(chicken_price_rawmaterialsupply, 0))) +
                               (sum(ifnull(feed_vol_rawmaterialsupply, 0)) * sum(ifnull(feed_price_rawmaterialsupply, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(hematic_bags_sold, 0)) * sum(ifnull(hematic_bags_price, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1, (sale_discount / 100))) *
                              ((sum(ifnull(ploughing_acres, 0)) * sum(ifnull(ploughing_price, 0))) +
                               (sum(ifnull(planting_acres, 0)) * sum(ifnull(planting_price, 0))) +
                               (sum(ifnull(weeding_acres, 0)) * sum(ifnull(weeding_price, 0))) +
                               (sum(ifnull(spraying_acres, 0)) * sum(ifnull(spraying_price, 0))) +
                               (sum(ifnull(harvesting_acres, 0)) * sum(ifnull(harvesting_price, 0))))) / 4500)+
                            (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * ((sum(ifnull(whitesorghum_bought_008, 0)) * sum(ifnull(whitesorghum_bought_009, 0))) +
                                (sum(ifnull(groundnuts_bought_008, 0)) * sum(ifnull(groundnuts_bought_009, 0))) +
                                (sum(ifnull(sesameseed_bought_008, 0)) * sum(ifnull(sesameseed_bought_009, 0))) +
                                (sum(ifnull(rice_bought_008_mar, 0)) * sum(ifnull(rice_bought_009_mar, 0))))) / 4500) +
                             (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * (((sum(ifnull(host_4, 0)) + sum(ifnull(refugees_4, 0))) * sum(ifnull(land_ploughed, 0))) +
                               (sum(ifnull(planting_1, 0)) * sum(ifnull(planting_2, 0))) +
                               (sum(ifnull(weeding_1, 0)) * sum(ifnull(weeding_2, 0))) +
                               (sum(ifnull(spraying_1, 0)) * sum(ifnull(spraying_2, 0))) +
                               (sum(ifnull(harvesting_1, 0)) * sum(ifnull(harvesting_2, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * ((sum(ifnull(bought_inorganic_volume, 0)) * sum(ifnull(bought_inorganic_price, 0))) +
                                (sum(ifnull(bought_milled_rice_volume, 0)) * sum(ifnull(bought_milled_rice_price, 0))) +
                                (sum(ifnull(bought_paddy_rice_volume, 0)) * sum(ifnull(bought_paddy_rice_price, 0))) +
                                (sum(ifnull(bought_rice_seed_maruptake_volume, 0)) *   sum(ifnull(bought_rice_seed_maruptake_price, 0))) +
                                (sum(ifnull(bought_maize_seed_maruptake_volume, 0)) *  sum(ifnull(bought_maize_seed_maruptake_price, 0))) +
                                (sum(ifnull(bought_maize_grain_maruptake_volume, 0)) *   sum(ifnull(bought_maize_grain_maruptake_price, 0))) +
                                (sum(ifnull(bought_chia_maruptake_volume, 0)) * sum(ifnull(bought_chia_maruptake_price, 0))) +
                                (sum(ifnull(bought_sorghum_seed_maruptake_volume, 0)) *  sum(ifnull(bought_sorghum_seed_maruptake_price, 0))) +
                                (sum(ifnull(bought_sorghum_grain_maruptake_volume, 0)) * sum(ifnull(bought_sorghum_grain_maruptake_price, 0))) +
                                (sum(ifnull(bought_millet_seed_maruptake_volume, 0)) *   sum(ifnull(bought_millet_seed_maruptake_price, 0))) +
                                (sum(ifnull(bought_millet_grain_maruptake_volume, 0)) *   sum(ifnull(bought_millet_grain_maruptake_price, 0))))) / 4500)+
                            (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * ((sum(ifnull(cassava_sold_volume, 0)) * sum(ifnull(cassava_sold_price, 0))) +
                                (sum(ifnull(sorghum_sold_volume, 0)) * sum(ifnull(sorghum_sold_price, 0))) +
                                (sum(ifnull(processed_cassava_sold_volume, 0)) * sum(ifnull(processed_cassava_sold_price, 0))) +
                                (sum(ifnull(volume_sorghum_cleaned, 0)) * sum(ifnull(average_sorghum_clenead, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * ((sum(ifnull(chia_seed_volume_sold, 0)) * sum(ifnull(chia_seed_price, 0))) +
                                (sum(ifnull(volume_processed_chia, 0)) * sum(ifnull(price_processed_chia, 0))) +
                                (sum(ifnull(gooseberry_seedlings_sold, 0)) * sum(ifnull(gooseberry_seedlings_price, 0))) +
                                (sum(ifnull(volume_processed_gooseberry, 0)) * sum(ifnull(price_processed_gooseberry, 0))))) / 4500) +
                            (((if((sale_discount / 100) = 0, 1,(sale_discount / 100))) * ((sum(ifnull(stevia_sold, 0)) * sum(ifnull(stevia_sold_price, 0))) +
                                (sum(ifnull(stevia_dry_sold, 0)) * sum(ifnull(stevia_dry_sold_price, 0))) +
                                (sum(ifnull(stevia_fresh_sold, 0)) * sum(ifnull(stevia_fresh_sold_price, 0))))) / 4500)
                             )                                               as total
                    from `_189_xxx_kpi_monitoringtool`
                    group by agribusiness, `__code`, district_a, reporting_year, reporting_quarter, intervention, unique_id
                    order by intervention
                   """
        def query1 = """
                        select
                               intervention as inter,
                               lower(p2.option_text) as partner,
                               district,
                               substring_index(year,'_',-1) as year,
                               case
                                    when  substring_index(year,'_',-1) = '2016' then '16-Sep'
                                    when  substring_index(year,'_',-1) = '2017' then '17-Mar'
                                    when  substring_index(year,'_',-1) = '2018' then '18-Mar'
                                    when  substring_index(year,'_',-1) = '2019' then '19-Mar'
                                    when  substring_index(year,'_',-1) = '2020' then '20-Mar'
                                    when  substring_index(year,'_',-1) = '2021' then '21-Mar'
                                    else '16-sep' end as year_months,
                               quater as quarter_year,
                               concat_ws('_', quater, year) as period,
                               sales_change as total
                        from `_182_xxx_lograme_trakers_form` p
                                    inner join `_182_partner` p2 on p.Id = p2.parentId
                        order by substring_index(period, '_', -1), substring_index(period, '_', 2)
                        """

        def query2 = """
                            select distinct option_id  as inter ,
                                            case
                  when option_text = 'Expanding the Village Agent Model in NU' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'expanding the village agent model in nu' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'M4R Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'organic producer out hyphen grower scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Organic Producer Out-grower Scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Private provision of on-farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Multiplication and distribution of improved seeds' then 'Multiplication and Distribution of Improved Seeds'
                  when option_text = 'private provision of on hyphen farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'stevia production for increased employment opportunities especially for women in northern Uganda' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Cassava' then 'M4R Commercial cassava production'
                  when option_text = 'Stevia Women Outgrower Pilot' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Offtake' then 'M4R Seed and Offtake'
                  when option_text = 'M4R Seed' then 'M4R Seed and Offtake'
                                              else option_text end as intervention
                            from `_182_intervention`
                            where option_text is not null
                            union all
                            select distinct option_id  as inter ,
                                            case
                  when option_text = 'Expanding the Village Agent Model in NU' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'expanding the village agent model in nu' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'M4R Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'organic producer out hyphen grower scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Organic Producer Out-grower Scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Private provision of on-farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Multiplication and distribution of improved seeds' then 'Multiplication and Distribution of Improved Seeds'
                  when option_text = 'private provision of on hyphen farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'stevia production for increased employment opportunities especially for women in northern Uganda' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Cassava' then 'M4R Commercial cassava production'
                  when option_text = 'Stevia Women Outgrower Pilot' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Offtake' then 'M4R Seed and Offtake'
                  when option_text = 'M4R Seed' then 'M4R Seed and Offtake'
                                                   else option_text end as intervention
                            from `_189_intervention`
                            where option_text is not null
                        """
        def date = new Date()
        def current_year = date[Calendar.YEAR].toString()


        def fuzzyData2 = FuzzyCSVTable.toCSV(sql, query2).copy().appendEmptyRecord().modify {
            set { it.inter = 'M4R Offtake'; it.intervention = 'M4R Seed and Offtake' }; where { it.inter in [null] }
        }
        def fuzzyData = FuzzyCSVTable.toCSV(sql, query.toString()).join(fuzzyData2, 'inter')
        def fuzzyData1 = FuzzyCSVTable.toCSV(sql, query1.toString()).join(fuzzyData2, 'inter')
        def groupPerIntervention = fuzzyData.autoAggregate('intervention', 'year', sum('total').az('total'))
        def groupPartners = fuzzyData.autoAggregate('partner', 'year', sum('total').az('total'))
        def baselines = groupPartners.filter { it.year == 'base' }.addColumn(fx('baseline') {
            return it.total
        }).delete('total', 'year')
        def notBaseline = groupPartners.filter { it.year != 'base' }.fullJoin(baselines,'partner')
                .modify {set {it.baseline = 0} where{it.baseline in [null]}}
                .addColumn(fx ('new_total'){ return ((it.total ? it.total : 0) - (it.baseline ? it.baseline : 0)) })
                .delete('total','baseline').renameHeader('new_total','total')
        def oldDataFilter = fuzzyData1.autoAggregate('partner', 'year', sum('total').az('total')).sort('partner')

        def joinAllOldData = (notBaseline + oldDataFilter).sort('partner','year')
                .autoAggregate('partner','year',sum('total').az('total'))

        def sum_total_total = joinAllOldData.sum { it.total }
        def summation = sum_total_total
        def df = new DecimalFormat("###,##0")
        def value = df.format(summation)
        def dashboardData = [[indicator : "Change in sales of AgDevCo and Palladium supported businesses attributed to project interventions",
                              short_name: "sales", value: "£ ${value}"]]

        def groupByDistrictAndIntervention = fuzzyData.autoAggregate('district', 'intervention', 'year', sum('total').az('total'))
                .pivot('year', 'total', 'district', 'intervention')
                .addColumn(fx('total') {
                    if (current_year == '2019') {
                        return ((it.'2019') ? it.'2019' : 0) - ((it.base) ? it.base : 0)
                    } else if (current_year == '2020') {
                        return ((it.'2020') ? it.'2020' : 0) - ((it.base) ? it.base : 0)
                    } else if (current_year == '2021') {
                        return ((it.'2021') ? it.'2021' : 0) - ((it.base) ? it.base : 0)
                    } else if (current_year == '2022') {
                        return ((it.'2022') ? it.'2022' : 0) - ((it.base) ? it.base : 0)
                    } else {
                        return 0
                    }
                })
        def group1 = groupByDistrictAndIntervention.autoAggregate('district', sum('total').az('total'))
        def group2 = fuzzyData1.autoAggregate('district', sum('total').az('total'))
        def groupByDistrict = (group1 + group2).filter {
            it.district != null
        }.autoAggregate('district', sum('total').az('total')).toMapList()
        def icon = "fa fa-line-chart"
        def color = "bg-orange"
        def title = "Change in sales of AgDevCo and Palladium supported businesses attributed to project interventions"
        def sum_totals = summation

        def newGroup1 = groupPerIntervention.filter { it.year != 'base' }
        def filterOutBaselines = groupPerIntervention.filter { it.year == 'base' }.addColumn(fx('baseline') { return it.total }).delete('total', 'year')
        def joinTables = newGroup1.fullJoin(filterOutBaselines, 'intervention').modify {
            set { it.baseline = 0 } where { it.baseline in [null] }
        }.addColumn(fx('new_total') { return it.total - it.baseline })
                .delete('total', 'baseline', 'baseline').renameHeader('new_total', 'total')
        def newGroup2 = fuzzyData1.autoAggregate('intervention', 'year', sum('total').az('total'))
        def combine_both_groups = (joinTables + newGroup2).autoAggregate('year', 'intervention', sum('total').az('total'))
        def targets = targetData.fullJoin(combine_both_groups, 'year')
        def modifyTargets = targets.modify {
            set {
                it.total = 0
                it.intervention = 'null'
            }
            where {
                it.total in [null]
            }
        }.filter { it.year != '2016' }.filter { it.year != 'base' }.toMapList()

        // Partner Calculation
        def filterPartnerPerYear = []
        def groupPerPartner = fuzzyData.autoAggregate('partner', 'year_months', 'year', sum('total').az('total'))
        def groupPerPartner1 = fuzzyData1.autoAggregate('partner', 'year_months', 'year', sum('total').az('total')).sort('partner')
        def newGroup3 = groupPerPartner.filter { it.year != 'base' }
        def filterOutPartnerBaselines = groupPerPartner.filter { it.year == 'base' }.addColumn(fx('baseline') {
            return it.total
        }).delete('total', 'year', 'year_months')
        def joinPartnerTables = newGroup3.fullJoin(filterOutPartnerBaselines, 'partner').modify { set { it.baseline = 0 } where { it.baseline in [null] } }
                .modify { set { it.total = 0; it.year_months = '19-Mar'; it.year = '2019' } where { it.total in [null] } }
                .addColumn(fx('new_total') { return (it.total - it.baseline) })
                .delete('total', 'baseline').renameHeader('new_total', 'total')
        def joinOldData = (joinPartnerTables + groupPerPartner1).sort('partner', 'year_months')
                .autoAggregate('partner', 'year_months', sum('total').az('total')).renameHeader('year_months', 'year_month')
        def newpartList = joinOldData.printTable().toMapList().collect { it.partner }.unique()

        def combineAllPartners = []
        newpartList << 'All Partners'
        newpartList.each { partner ->
            def eachPartner = partner.toString()
            def createSampleMap = [[year_month: '16-Sep'], [year_month: '17-Mar'],
                                   [year_month: '18-Mar'], [year_month: '19-Mar'],
                                   [year_month: '20-Mar'], [year_month: '21-Mar'],
                                   [year_month: '21-Dec']]
            createSampleMap.eachWithIndex { record, index ->
                record.put('partner', eachPartner)
                combineAllPartners << record
            }
        }

        def changeToFuzzy = FuzzyCSVTable.toCSV(combineAllPartners).join(targetData, 'year_month').delete('Outcome')
        def mergeAllPartnerData = changeToFuzzy.fullJoin(joinOldData, 'year_month', 'partner').modify {
            set { it.total = 0 } where { it.total in [null] }
        }.printTable().toMapList()
        newpartList.each { part ->
            def perPartner = mergeAllPartnerData.findAll { it.partner == part }
            def runningtotal = 0
            perPartner.eachWithIndex { record, index ->
                if (index == 0) {
                    runningtotal = record.total
                    record.put('runningtotal', runningtotal)
                } else {
                    runningtotal += record.total
                    record.put('runningtotal', runningtotal)
                }
            }
            perPartner.each {
                filterPartnerPerYear << it
            }
        }
        def changePartnerMapToFuzzy = FuzzyCSVTable.toCSV(filterPartnerPerYear).delete('total').renameHeader('runningtotal', 'total')
        def aggregateAllPartners = changePartnerMapToFuzzy.autoAggregate('year_month', sum('total').az('alltotal'))
        def addColumnPartner = aggregateAllPartners.addColumn(fx('partner') { return 'All Partners' })
        def joinAllPartnerData = changePartnerMapToFuzzy.fullJoin(addColumnPartner, 'year_month', 'partner')
                .modify { set { it.total = it.alltotal } where { it.partner in ['All Partners'] } }
                .addColumn(fx('indicator_index') { return 4 }).delete('alltotal').toMapList()
    }
}