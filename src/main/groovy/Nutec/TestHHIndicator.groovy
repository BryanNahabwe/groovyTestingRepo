package Nutec

import fuzzycsv.FuzzyCSV
import fuzzycsv.FuzzyCSVTable
import fuzzycsv.CSVToExcel
import fuzzycsv.Excel2Csv
import groovy.sql.Sql

import java.text.DecimalFormat

import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx

class TestHHIndicator {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Number of households in northern Uganda with increase in agricultural income"
        }).modify { set { it.year_month = '16-Sep' } where { it.year_month in ['Sep-16'] } }
                .modify { set { it.year_month = '17-Mar' } where { it.year_month in ['Mar-17'] } }
                .modify { set { it.year_month = '18-Mar' } where { it.year_month in ['Mar-18'] } }
                .modify { set { it.year_month = '19-Mar' } where { it.year_month in ['Mar-19'] } }
                .modify { set { it.year_month = '20-Mar' } where { it.year_month in ['Mar-20'] } }
                .modify { set { it.year_month = '21-Mar' } where { it.year_month in ['Mar-21'] } }
                .modify { set { it.year_month = '21-Dec' } where { it.year_month in ['Dec-21'] } }

        def query = """
                    select upper(farmer_name)          as farmer_name,
                           `__code`                    as code,
                           date,
                           case
                             when date(date) between date('2018-04-01') and date('2019-03-31') then '2019'
                             when date(date) between date('2019-04-01') and date('2020-03-31') then '2020'
                             when date(date) between date('2020-04-01') and date('2021-03-31') then '2021'
                             when date(date) between date('2021-04-01') and date('2021-12-31') then '2022'
                             when date(date) between date('2017-01-01') and date('2018-03-31') then 'base'
                             else 'no year'
                             end                       as year,
                           case
                             when date(date) between date('2018-04-01') and date('2018-09-30') then 'season_a_2019'
                             when date(date) between date('2018-10-01') and date('2019-03-31') then 'season_b_2019'
                             when date(date) between date('2017-01-01') and date('2018-03-31') then 'base'
                             else 'no season'
                             end                       as period,
                           (sum(ifnull(sunflower22, 0)) +
                            sum(ifnull(soybeans22, 0)) +
                            sum(ifnull(groundnuts22, 0)) +
                            sum(ifnull(sorghum22, 0)) +
                            sum(ifnull(rice22, 0)) +
                            sum(ifnull(beans22, 0)) +
                            sum(ifnull(cassava22, 0)) +
                            sum(ifnull(chia22, 0)) +
                            sum(ifnull(maize22, 0)) +
                            sum(ifnull(simsim22, 0)) +
                            sum(ifnull(cotton22, 0)) +
                            sum(ifnull(tobacco22, 0))) as income
                    from `_179_xxx_farmers_record_book`
                    where partner is not null
                     group by  farmer_name, `__code`, district, parish ,partner,crop_grown,date,agent,name_interviewer,unique_id
                    order by farmer_name
                    """

        def query1 = """
                        select intervention                                                                            as inter,
                               lower(case
                                  when agribusiness = 'East Africa Seeds Ltd (Sunflower )' then 'East African Seeds'
                                  when agribusiness = 'Latyeng Commercial Farm Ltd (49ers Farms Ltd)' then 'Latyeng Commercial Farm Ltd'
                                  when agribusiness = "Mount Meru Miller's  (Uganda) Ltd" then 'Mount Meru Millers Ltd'
                                  when agribusiness = 'Sun Stand Still Limited' then 'Sun Stand Still'
                                  when agribusiness = 'Umba Veterinary Services' then 'Umber Veterinary'
                                  when agribusiness = 'Lira Resort Enterprises Limited' then 'lira Resort'
                                  when agribusiness = "Mount Meru Miller's  (Uganda) Ltd" then 'Mount Meru Millers Ltd'
                                  when agribusiness = 'Xclusive Farm Nam amuru Ltd' then 'xclusive stevia'
                            else agribusiness end) as partner,
                           district_a                                        as district,
                           substring_index(reporting_year, '_', -1)          as year,
                                case
                                     when  date(openxdata_form_data_date_created) < date('2016-09-01') then '16-Sep'
                                     when  date(openxdata_form_data_date_created) between date('2016-09-01') and date('2017-03-31') and reporting_year != 'base' then '17-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2017-04-01') and date('2018-03-31') and reporting_year != 'base' then '18-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2018-04-01') and date('2019-03-31') and reporting_year != 'base' then '19-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2019-04-01') and date('2020-03-31') and reporting_year != 'base' then '20-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2020-04-01') and date('2021-03-31') and reporting_year != 'base' then '21-Mar'
                                     when  date(openxdata_form_data_date_created) between date('2021-04-01') and date('2021-12-31') and reporting_year != 'base' then '21-Dec'
                                     when  reporting_year = 'base'  then 'base'
                                     else 'no year month' end as year_months,
                               substring_index(reporting_year, '_', -1)                                                as year,
                               reporting_quarter                                                                       as quarter_year,
                               concat_ws('_', reporting_quarter, reporting_year)                                       as period,
                               (((sum(ifnull(sunflowergrain_buyers_females, 0)) + sum(ifnull(sunflowergrain_buyers_males, 0))) +(0.5 * (sum(ifnull(sunflower_buyers_females, 0)) + sum(ifnull(sunflower_buyers_males, 0))))) +
                                (sum(ifnull(sunflower_soybean_seed_buy_total, 0))) +
                               ((sum(ifnull(namche_buyers_females, 0)) + sum(ifnull(namche_buyers_males, 0))) +(0.5 * (sum(ifnull(rice_grain_sell_females, 0)) + sum(ifnull(rice_grain_sell_males, 0))))) +
                                ((sum(ifnull(feed_females, 0)) + sum(ifnull(feed_buyers_males, 0))) +(0.5 * (sum(ifnull(feed_sell_rawmaterialsupply_females, 0)) + sum(ifnull(feed_sell_rawmaterialsupply__males, 0))))) +
                                (sum(ifnull(farmers_bags_buying, 0))) +
                                (sum(ifnull(farmers_using_tractors, 0))) +
                                ((sum(ifnull(buyers_females_mar, 0)) + sum(ifnull(buyers_males_mar, 0))) +(0.5 * (sum(ifnull(crop_purchased_female_mar, 0)) + sum(ifnull(crop_purchased_male_mar, 0))))) +
                                (sum(ifnull(host_2, 0)) + sum(ifnull(refugees_2, 0))) +
                                ((sum(ifnull(bought_inorganic_female, 0)) + sum(ifnull(bought_inorganic_male, 0))) +(0.25 * ((sum(ifnull(bought_maize_female, 0)) + sum(ifnull(bought_maize_male, 0))) +
                                                                                                                             (sum(ifnull(bought_chia_seed_female, 0)) + sum(ifnull(bought_rice_seed_male, 0)))+
                                                                                                                             (sum(ifnull(bought_chia_seed_crop_female, 0)) + sum(ifnull(bought_chia_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_sorghum_seed_crop_female, 0)) + sum(ifnull(bought_sorghum_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_millet_seed_crop_female, 0)) + sum(ifnull(bought_millet_seed_crop_male, 0))) +
                                                                                                                             (sum(ifnull(bought_other_farm_inputs_female, 0)) + sum(ifnull(bought_other_farm_inputs_male, 0)))))) +
                                ((sum(ifnull(farmers_buying_cassava_female, 0)) + sum(ifnull(farmers_buying_cassava_male, 0))) +(0.5 * (sum(ifnull(farmers_buying_sorghum_marcassava_female, 0)) + sum(ifnull(farmers_buying_sorghum_marcassava_male, 0))))) +
                                ((sum(ifnull(chia_seed_purchased_female, 0)) + sum(ifnull(chia_seed_purchased_male, 0))) +(0.5 * (sum(ifnull(gooseberry_seed_purchased, 0)) + sum(ifnull(gooseberry_seed_purchased_female, 0))))) +
                                ((sum(ifnull(stevia_buyers_females, 0)) + sum(ifnull(stevia_buyers_males, 0))) +(0.5 * (sum(ifnull(stevia_fresh_females, 0)) + sum(ifnull(stevia_fresh_males, 0)))))   ) as total
                        from `_189_xxx_kpi_monitoringtool`
                        group by agribusiness, `__code`, district_a, reporting_year, reporting_quarter, intervention,unique_id
                        order by intervention
                        """

        def query2 = """
                    select intervention                   as inter,
                           lower(replace(partner, '_', ' '))   as partner,
                           district,
                           substring_index(year, '_', -1) as year,
                            case
                                    when  substring_index(year,'_',-1) = '2016' then '16-Sep'
                                    when  substring_index(year,'_',-1) = '2017' then '17-Mar'
                                    when  substring_index(year,'_',-1) = '2018' then '18-Mar'
                                    when  substring_index(year,'_',-1) = '2019' then '19-Mar'
                                    when  substring_index(year,'_',-1) = '2020' then '20-Mar'
                                    when  substring_index(year,'_',-1) = '2021' then '21-Mar'
                                    else '16-sep' end as year_months,
                           quater                         as quarter_year,
                           concat_ws('_', quater, year)   as period,
                           sum(ifnull(non_youth, 0))      as total
                    from `_182_xxx_lograme_trakers_form`
                    group by intervention, partner, district, year, quater, unique_id
                     """

        def date = new Date()
        def current_year = date[Calendar.YEAR].toString()

        def fuzzyData = FuzzyCSVTable.toCSV(sql, query.toString())
        def fuzzyData1 = FuzzyCSVTable.toCSV(sql, query1.toString())
        def fuzzyData2 = FuzzyCSVTable.toCSV(sql, query2.toString())
        def filterOutTotal = fuzzyData2.filter { it.total != 0 && it.total != null }
        def groupPerIntervention = fuzzyData1.autoAggregate('inter', 'year', sum('total').az('total'))
        def basetotal = groupPerIntervention.filter { it.year == 'base' }.sum { it.total }
        def current_year_total = groupPerIntervention.filter { it.year == current_year }.sum { it.total }
        def total1 = current_year_total - basetotal
        def total2 = filterOutTotal.sum { it.total }
        def summation = total1 + total2
        def filterOut = fuzzyData.filter { it.year != 'no year' }
        def groupbyPartner = filterOut.autoAggregate('farmer_name', 'code', 'year', 'period', sum('income').az('total_income'))
        def distinctFarmers = groupbyPartner.pivot('period', 'total_income', 'code')
        def replacebaseNulls = distinctFarmers.modify { set { it.base = 0 } where { it.base in [null] } }
        def replaceSeasonANulls = replacebaseNulls.modify {
            set { it.'season_a_2019' = 0 } where { it.'season_a_2019' in [null] }
        }
        def replaceSeasonBNulls = replaceSeasonANulls.modify {
            set { it.'season_b_2019' = 0 } where { it.'season_b_2019' in [null] }
        }

        def changeInIncomeData = replaceSeasonBNulls.addColumn(fx('income_2019') {
            return ((it.season_a_2019 as double) + (it.season_b_2019 as double))
        })
                .addColumn(fx('change_in_income') {
                    return (it.'income_2019' - it.base)
                }).renameHeader('base', 'baseline_income')
        def filterFarmerswithzero = changeInIncomeData.filter { it.change_in_income > 0 }.toMapList()

        def changeToFuzzy = FuzzyCSVTable.toCSV(filterFarmerswithzero).printTable()
        println(filterFarmerswithzero)
//        def farmerswithincome = filterFarmerswithzero.count { it.code }
////        def totalfarmers = distinctFarmers.count { it.code }
////        def df = new DecimalFormat("#")
////        def round = (df.format(summation)) as int
////        def calc = ((farmerswithincome / totalfarmers) * round)
////        def df1 = new DecimalFormat("###,##0")
////        def finalvalue = df1.format(calc)
////        println(round)
////        println(farmerswithincome)
////        println(totalfarmers)
////        println(finalvalue)

        CSVToExcel.exportToExcelFile("data": changeToFuzzy, "data.xlsx")
        FuzzyCSV.writeCSV()

    }
}
