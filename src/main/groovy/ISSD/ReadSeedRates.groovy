package ISSD

import fuzzycsv.FuzzyCSVTable

static def getSeedRates(){
    def url = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\ISSD\\seed_rate.csv")
    return FuzzyCSVTable.parseCsv(url.text)
}