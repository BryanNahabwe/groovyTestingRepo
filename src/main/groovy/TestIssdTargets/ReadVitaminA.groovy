package TestIssdTargets

import fuzzycsv.FuzzyCSVTable

static def getVitaminA(){
    def url = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\TestIssdTargets\\vitamin_a.csv")
    return FuzzyCSVTable.parseCsv(url.text)
}