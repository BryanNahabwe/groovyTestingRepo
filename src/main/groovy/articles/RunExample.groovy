package articles

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class RunExample {
    static void main(String[] args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "", 'com.mysql.jdbc.Driver')
        def query = """SELECT period, district,sum(total_employed) as total,count(*) as records
FROM annual_review_form
  WHERE district IS NOT NULL"""

        def csv = FuzzyCSVTable.toCSV(sql, query.toString())
        def record = [:]
        csv.collect {
            record.uid = UUID.randomUUID()
            record.indicator = 'Increase in net farm income'
            record.period = it.period
            record.district = it.district
            record.total = (it.total != null && it.total != 0) ? (it.total) : 0
            record.noOfRecords = it.records
            record.avgtotal = (it.total != null && it.total != 0) ? (it.total / it.records) : 0
            println(record)
        }

    }
}
