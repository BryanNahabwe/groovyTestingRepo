package TestSupplyChain

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class TestHHReport {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "", 'com.mysql.jdbc.Driver')
        def query = """
select p.partner as partner, ch.improved as product
from `_358_xxx_household_form` p
inner join (select * from `_358_supplier`) ch
on p.Id = ch.parentId
"""

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query).printTable().toMapList()
        println(fuzzyData)
    }
}
