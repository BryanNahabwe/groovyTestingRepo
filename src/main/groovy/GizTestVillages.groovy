import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonSlurper
import groovy.sql.Sql

class GizTestVillages {
    static void main(def args) {
        def sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryMappingCells = """
                                select *
                                from giz_mapping_villages_form_shapefile
                                where lower(municipality) like '%Gulu%'
                                order by cell_shape_file
                                """.toString()
        def mapping_cells_data = sql.rows(queryMappingCells).collect {
            [
                    'id'                 : it.id,
                    'municipality'       : it.municipality,
                    'division_form'      : it.division_form,
                    'parish_form'        : it.parish_form,
                    'cell_form_unique_id': it.cell_form_unique_id,
                    'cell_form'          : it.cell_form,
                    'object_id'          : it.object_id,
                    'cell_shape_file'    : it.cell_shape_file,
                    'flag'               : it.flag
            ]
        } as List
        def fuzzy_mapping = FuzzyCSVTable.toCSV(mapping_cells_data).sort {"${it['cell_form']} ${it['cell_shape_file']}"}.printTable()
    }
}
