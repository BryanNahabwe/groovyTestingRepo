import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import java.text.DecimalFormat
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestTableData {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryTableData = """
                            select indicator,
                                   target,
                                   concat('Target ', year) as year,
                                   year as target_year,
                                   project_year_target,
                                   overall_target
                            from craft_indicators_target_data
                            """

        def querySampledData = """
                                select indicator,
                                       period,
                                       case
                                           when period = 'review_2020' then '2020'
                                           when period = 'review_2021' then '2021'
                                           when period = 'review_2022' then '2022'
                                           when period = 'review_2023' then '2023'
                                           else 'baseline'
                                        end as year,
                                       business_champion,
                                       country,
                                       total_indicator,
                                       total_surveyed
                                from craft_indicators_sampled_data
                                where country like '%%' and business_champion like '%%'
                                """
        def queryTotalPpln = """
                            select case
                                       when country = 'uganda' then 'Uganda'
                                       when country = 'tanzania' then 'Tanzania'
                                       when country = 'kenya' then 'Kenya'
                                       else 'Uganda'
                                       end                   as country,
                                   name_partner as business_champion,
                                   count(distinct farmer_id) as total_reached
                            from craft_farmers_reached
                            where name_partner != ''
                            group by country, name_partner;
                            """

        def date = new Date()
        def current_year = date[Calendar.YEAR].toString()

        def targetDataFuzzy = FuzzyCSVTable.toCSV(sql, queryTableData)
        def filterOveralls = targetDataFuzzy.filter { it.target == 'overall' }
                .delete('year', 'project_year_target', 'target_year')
                .renameHeader('target', 'year')
                .renameHeader('overall_target', 'target')
        def filterAnnuals = targetDataFuzzy.filter { it.target == 'annual' }
                .delete('overall_target', 'target', 'target_year')
                .renameHeader('project_year_target', 'target')
        def targetData = (filterAnnuals + filterOveralls).sort { it.indicator }

        def getYears = targetDataFuzzy.toMapList().collect { it.target_year }.unique()

        def sampleDataFuzzy = FuzzyCSVTable.toCSV(sql, querySampledData)

        def totalPplnFuzzy = FuzzyCSVTable.toCSV(sql, queryTotalPpln)

        // Country Calculation
        def totalPplnCountry = totalPplnFuzzy.autoAggregate('country',
                sum('total_reached').az('total_reached'))

        def sampledDataFuzzyCountry = sampleDataFuzzy.autoAggregate('indicator', 'country', 'period', 'year',
                sum('total_indicator').az('total_indicator'),
                sum('total_surveyed').az('total_surveyed'))
                .addColumn(fx('total_value') {
                    return (it.total_indicator / it.total_surveyed)
                }).delete('total_indicator', 'total_surveyed')
        def filterBaselinesCountry = sampledDataFuzzyCountry.filter { it.period == 'baseline' }
                .delete('period', 'year')
                .renameHeader('total_value', 'baseline_total')
        def filterOutBaselinesCountry = sampledDataFuzzyCountry.filter { it.period != 'baseline' }
                .filter { it.year == current_year }
                .autoAggregate('indicator', 'country', sum('total_value').az('review_total'))
                .delete('period')
                .join(filterBaselinesCountry, 'indicator', 'country')
                .join(totalPplnCountry, 'country')
                .addColumn(fx('actual') {
                    def cal = ((it.review_total - it.baseline_total) as Float) * (it.total_reached as Float)
                    def changeToPositive = Math.abs(cal)
                    def num
                    if (changeToPositive < 1 && changeToPositive > 0) {
                        num = 1
                    } else {
                        num = changeToPositive
                    }
                    return num
                })
                .delete('review_total', 'baseline_total', 'total_reached')
                .sort { it.indicator }.printTable()
        def new_map_country = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampledDataFuzzyCountry.filter { it.period != 'baseline' }
                    .filter { it.year == each_review }
                    .autoAggregate('indicator', 'country', 'year', sum('total_value').az('review_total'))
                    .sort { it.indicator }
                    .join(filterBaselinesCountry, 'indicator', 'country')
                    .join(totalPplnCountry, 'country')
                    .addColumn(fx('actual_' + each_review) {
                        def cal = ((it.review_total - it.baseline_total) as Float) * (it.total_reached as Float)
                        def changeToPositive = Math.abs(cal)
                        def num
                        if (changeToPositive < 1 && changeToPositive > 0) {
                            num = 1
                        } else {
                            num = changeToPositive
                        }
                        return num
                    })
                    .delete('review_total', 'baseline_total', 'total_reached')
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_country << it
            }
        }

        def my_map_country = new_map_country.collect {
            [
                    'indicator'  : it.indicator,
                    'country'    : it.country,
                    'period'     : it.period,
                    'actual_2020': (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021': (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022': (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023': (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024': (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_country = FuzzyCSVTable.toCSV(my_map_country)
                .delete('period')

        def finalDataCountry = filterOutBaselinesCountry
                .join(reviews_to_fuzzy_country, 'indicator', 'country')
                .fullJoin(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }
                .filter { it.country != null }
                .renameHeader('country', 'level')


        // Partner Calculation
        def totalPplnPartner = totalPplnFuzzy.autoAggregate('business_champion',
                sum('total_reached').az('total_reached'))
        def sampledDataFuzzyPartner = sampleDataFuzzy.autoAggregate('indicator', 'business_champion', 'period', 'year',
                sum('total_indicator').az('total_indicator'),
                sum('total_surveyed').az('total_surveyed'))
                .addColumn(fx('total_value') {
                    return (it.total_indicator / it.total_surveyed)
                }).delete('total_indicator', 'total_surveyed')

        def filterBaselinesPartner = sampledDataFuzzyPartner.filter { it.period == 'baseline' }
                .delete('period', 'year')
                .renameHeader('total_value', 'baseline_total')

        def filterOutBaselinesPartner = sampledDataFuzzyPartner.filter { it.period != 'baseline' }
                .filter { it.year == current_year }
                .autoAggregate('indicator', 'business_champion',
                        sum('total_value').az('review_total'))
                .delete('period')
                .join(filterBaselinesPartner, 'indicator', 'business_champion')
                .join(totalPplnPartner, 'business_champion')
                .addColumn(fx('actual') {
                    def cal = ((it.review_total - it.baseline_total) as Float) * (it.total_reached as Float)
                    def changeToPositive = Math.abs(cal)
                    def num
                    if (changeToPositive < 1 && changeToPositive > 0) {
                        num = 1
                    } else {
                        num = changeToPositive
                    }
                    return num
                })
                .delete('review_total', 'baseline_total', 'total_reached')
                .sort { it.indicator }
        def new_map_partner = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampledDataFuzzyPartner.filter { it.period != 'baseline' }
                    .filter { it.year == each_review }
                    .autoAggregate('indicator', 'business_champion', 'year', sum('total_value').az('review_total'))
                    .sort { it.indicator }
                    .join(filterBaselinesPartner, 'indicator', 'business_champion')
                    .join(totalPplnPartner, 'business_champion')
                    .addColumn(fx('actual_' + each_review) {
                        def cal = ((it.review_total - it.baseline_total) as Float) * (it.total_reached as Float)
                        def changeToPositive = Math.abs(cal)
                        def num
                        if (changeToPositive < 1 && changeToPositive > 0) {
                            num = 1
                        } else {
                            num = changeToPositive
                        }
                        return num
                    })
                    .delete('review_total', 'baseline_total', 'total_reached')
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_partner << it
            }
        }

        def my_map_partner = new_map_partner.collect {
            [
                    'indicator'        : it.indicator,
                    'business_champion': it.business_champion,
                    'period'           : it.period,
                    'actual_2020'      : (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021'      : (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022'      : (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023'      : (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024'      : (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_partner = FuzzyCSVTable.toCSV(my_map_partner)
                .delete('period')

        def finalDataPartner = filterOutBaselinesPartner
                .join(reviews_to_fuzzy_partner, 'indicator', 'business_champion')
                .fullJoin(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }
                .filter { it.business_champion != null }
                .renameHeader('business_champion', 'level')

        // Project Calculation
        def totalPplnSummation = totalPplnFuzzy.sum { it.total_reached }
        def sampledDataFuzzy = sampleDataFuzzy.autoAggregate('indicator', 'period', 'year',
                sum('total_indicator').az('total_indicator'),
                sum('total_surveyed').az('total_surveyed'))
                .addColumn(fx('total_value') {
                    return (it.total_indicator / it.total_surveyed)
                }).delete('total_indicator', 'total_surveyed')
        def filterBaselines = sampledDataFuzzy.filter { it.period == 'baseline' }
                .delete('period', 'year')
                .renameHeader('total_value', 'baseline_total')
        def filterOutBaselines = sampledDataFuzzy.filter { it.period != 'baseline' }
                .filter { it.year == current_year }
                .autoAggregate('indicator', sum('total_value').az('review_total'))
                .delete('period')
                .join(filterBaselines, 'indicator')
                .addColumn(fx('level') {
                    return 'Project'
                })
                .addColumn(fx('actual') {
                    def cal = ((it.review_total - it.baseline_total) as Float) * (totalPplnSummation as Float)
                    def changeToPositive = Math.abs(cal)
                    def num
                    if (changeToPositive < 1 && changeToPositive > 0) {
                        num = 1
                    } else {
                        num = changeToPositive
                    }
                    return num
                })
                .delete('review_total', 'baseline_total')
                .sort { it.indicator }

        def new_map_project = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampledDataFuzzy.filter { it.period != 'baseline' }
                    .filter { it.year == each_review }
                    .autoAggregate('indicator', 'year', sum('total_value').az('review_total'))
                    .sort { it.indicator }
                    .join(filterBaselines, 'indicator')
                    .addColumn(fx('actual_' + each_review) {
                        def cal = ((it.review_total - it.baseline_total) as Float) * (totalPplnSummation as Float)
                        def changeToPositive = Math.abs(cal)
                        def num
                        if (changeToPositive < 1 && changeToPositive > 0) {
                            num = 1
                        } else {
                            num = changeToPositive
                        }
                        return num
                    })
                    .delete('review_total', 'baseline_total', 'total_reached')
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_project << it
            }
        }

        def my_map_project = new_map_project.collect {
            [
                    'indicator'        : it.indicator,
                    'period'           : it.period,
                    'actual_2020'      : (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021'      : (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022'      : (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023'      : (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024'      : (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_project = FuzzyCSVTable.toCSV(my_map_project)
                .delete('period')

        def finalDataProject = filterOutBaselines
                .join(reviews_to_fuzzy_project, 'indicator')
                .fullJoin(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }
                .filter { it.level != null }

        def finalData = (finalDataProject + finalDataCountry + finalDataPartner)
                .pivot('year', 'target', 'indicator', 'level', 'actual', 'actual_2020')
                .addColumn(fx('Cummulative Actual') { return new DecimalFormat("#,###").format(it.actual) })
                .addColumn(fx('LOP') { return new DecimalFormat("#,###").format(it.overall) })
                .delete('actual', 'overall')
                .sort('indicator').printTable().toMapList()

    }
}
