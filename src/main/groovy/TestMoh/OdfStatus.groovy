package TestMoh

import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*
import fuzzycsv.RecordFx
import groovy.sql.Sql

import static fuzzycsv.RecordFx.fx

class OdfStatus {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "", 'com.mysql.cj.jdbc.Driver')
        def query = """SELECT district,reporting_quarter,replace(reporting_year,'_','') as reporting_year,sum(people_living_odf_areas) AS total
                        FROM `_16_xxx_indicator_form`
                        where district is not null
                        GROUP BY district,reporting_quarter,reporting_year"""
        def query1 = """
                    select hhead,lower(district) as district,
                    lower(subcounty) as subcounty,
                    lower(parish) as parish,
                    lower(village) as village,surround as base,
                    CASE
                         WHEN quarter(date_entry) = 1 THEN CONCAT('quarter_', quarter(date_entry) + 2)
                         WHEN quarter(date_entry) = 2 THEN CONCAT('quarter_', quarter(date_entry) + 2)
                         ELSE CONCAT('quarter_', quarter(date_entry) - 2) END AS reporting_quarter,
                       YEAR(date_entry)  as reporting_year
                    from `_27_xxx_baseline_survey_form` 
                    where surround LIKE '%absent%' and (latrine='has_latrine' or latrine = 'yes') 
                    order by hhead
                    """

        def query2 = """
                    select t1.hhead,lower(t1.district) as district,
                    lower(t1.subcounty) as subcounty,
                    lower(t1.parish) as parish,
                    lower(t1.village) as village,t1.surround as follow_up,
                      CASE
                         WHEN quarter(t2.maxdate) = 1 THEN CONCAT('quarter_', quarter(t2.maxdate) + 2)
                         WHEN quarter(t2.maxdate) = 2 THEN CONCAT('quarter_', quarter(t2.maxdate) + 2)
                         ELSE CONCAT('quarter_', quarter(t2.maxdate) - 2) END AS reporting_quarter,
                       YEAR(t2.maxdate)  as reporting_year
                    from `_28_xxx_household_followup_form`  t1
                           INNER JOIN
                         (
                           SELECT max(activity_date) maxdate, hhead
                           FROM `_28_xxx_household_followup_form`
                           WHERE surround LIKE '%absent%' AND district is not null
                           GROUP BY hhead,district,subcounty
                         ) t2
                         ON t1.activity_date = t2.maxdate
                           AND t1.hhead = t2.hhead
                    WHERE  district is not null
                    GROUP BY hhead,district,subcounty,parish,village
                    order by t1.hhead
                    """


        def fuzzy1 = FuzzyCSVTable.toCSV(sql,query1)
        def fuzzy2 = FuzzyCSVTable.toCSV(sql,query2)

        def fullJoin = fuzzy1.fullJoin(fuzzy2, 'hhead', 'district', 'subcounty', 'parish', 'village','reporting_quarter')
        def addCommonColumn = fullJoin.addColumn(fx('surround') {
            if (it.base == it.follow_up) {
                return it.base
            } else if (it.base == null) {
                return it.follow_up
            } else if (it.follow_up == null) {
                return it.base
            } else {
                return it.base
            }
        })
        def groupBy = addCommonColumn.autoAggregate('district','reporting_quarter','reporting_year', count('surround').az('total'))
        def csv = (FuzzyCSVTable.toCSV(sql, query.toString()) + groupBy)
        csv.addColumn(fx('quarter_year'){it.reporting_quarter+"_"+it.reporting_year}).printTable()

    }
}
