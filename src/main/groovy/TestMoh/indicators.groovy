package TestMoh

import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql

import java.text.NumberFormat

import static fuzzycsv.FuzzyStaticApi.fx
import static fuzzycsv.FuzzyStaticApi.sum

class indicators {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "", 'com.mysql.cj.jdbc.Driver')
        def query = """SELECT district,reporting_quarter,reporting_year,sum(hh_washing) AS total
                        FROM `_16_xxx_indicator_form`
                        where district is not null
                        GROUP BY district,reporting_quarter,reporting_year"""

        def csv = FuzzyCSVTable.toCSV(sql, query.toString())
        csv.addColumn(RecordFx.fx('quarter_year'){it.reporting_quarter+"_"+it.reporting_year})
        def districtBasedLocation = csv.autoAggregate('district', sum('total').az('total'))
                .addColumn(fx ('icon'){ return "icon-images"},fx ('color'){ return "bg-red"}).toMapList()
        def trendData = csv.toMapList()
        def title = "Number of households hand washing with soap"
        def result = ["title":title, "districts": districtBasedLocation,"trendData":trendData]
        println(result)
    }
}
