package TestMirpIndicators

import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*
import groovy.sql.Sql
Sql sql =  sql
def query =    """
                select beneficiary_name,
                       maidf.hh_id,
                       lower(mhe.gender) as gender,
                       concat_ws(' ', 'Quarter', quarter(distribution_date)) as quarter,
                       year(distribution_date) as year
                from mirp_acf_input_distribution_form maidf
                left join mirp_household_entity mhe on maidf.hh_id = mhe.hh_id
                group by hh_id
                """
def fuzzy_data = FuzzyCSVTable.toCSV(sql, query)
def genderAggregatedData = fuzzy_data.autoAggregate('gender', 'year', 'quarter', count('hh_id').az('actual')).printTable().toMapList()
return [percentage: "false", gender: "true", script_value: genderAggregatedData]