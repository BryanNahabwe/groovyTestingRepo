import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql
import static fuzzycsv.FuzzyStaticApi.*

class TestCraftIndicatorResults {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def queryData = """
                    select fq.openxdata_user_name,
                           fq.farmer_name,
                           fq.farmer_id,
                           if(cfe.age_group = '_18_hyphen_24_years' or cfe.age_group = '_25_hyphen_34_years', 'youth', fq.gender) as gender,
                           lower(fq.country) as country,
                           replace(fq.name_partner, '_', ' ')                                                                     as name_partner,
                           period,
                           number_season as season,
                           crops_produced,
                           if(floods = 'yes' or drought = 'yes' or prolongedrainfal = 'yes' or prolongeddrought = 'yes'
                                  or storms = 'yes' or bushfires = 'yes' or pestswetseason = 'yes' or pestsdryseason = 'yes' or landslides = 'yes', 'yes', 'no') as checks,
                           1 as total
                    from _398_xxx_farmers_questionnaire fq
                             left join craft_farmer_entity cfe on fq.farmer_id = cfe.farmer_id
                    where stress_resilence in ('_0_percent', '_1_percent_hyphen_20_percent' , '_21_percent_hyphen_50_percent')
                    group by Id;
                    """

        def querySamplePpln = """
                        select lower(country) as country,replace(name_partner, '_', ' ') as name_partner, period, count(*) as total_surveyed
                        from _398_xxx_farmers_questionnaire
                        group by country,replace(name_partner, '_', ' '), period
                        """
        // Fetch Farmer questionnaire data and append the sample population and total population based on business champion
        // Add a new column that calculates the farmers reached income
        // Calculation = (total income / sample population) * total population ------------  Note: it can be done per farmer or per business champion
        def fetchData = FuzzyCSVTable.toCSV(sql, queryData)
                .filter { it.checks == 'yes' }
        def samplePpln = FuzzyCSVTable.toCSV(sql, querySamplePpln)

        def finalTable = fetchData.autoAggregate('country', 'name_partner', 'period',
                sum('total').az('total_indicator'))
                .join(samplePpln, 'country', 'name_partner', 'period')
                .sort { it.name_partner }

        println(finalTable.sum { it.total_indicator})
        println(finalTable.sum { it.total_surveyed})


    }
}
