import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonSlurper
import groovy.sql.Sql

class GizMappingCells {
    static void main(def args) {
        def sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryFormCells = """
                            select upper(municipality) as municipality,upper(cell) as cell, upper(cell) as cell_form
                            from _30_xxx_villages_form
                            group by municipality, cell
                            """

        def queryShapefileData = """
                                    select upper(municipality) as municipality, geojson_data,property
                                    from giz_add_new_town
                                    where geojson_data is not null and property is not null
                                    group by municipality
                                    """

        def form_cells = FuzzyCSVTable.toCSV(sql, queryFormCells).sort { "$it.municipality $it.cell $it.cell_form" }
        def shape_file_data = FuzzyCSVTable.toCSV(sql, queryShapefileData).toMapList()
        def shape_file_cells_list = []
        def jsonSlurper = new JsonSlurper()
        shape_file_data.each { town_data ->
            def geojson_data = jsonSlurper.parseText(town_data.geojson_data.toString())
            geojson_data['features']['properties'].each {
                def cell_property = town_data.property.toString()
                shape_file_cells_list << [
                        'municipality'   : town_data.municipality,
                        'object_id'      : it['OBJECTID'],
                        'cell'           : it["${cell_property}"],
                        'cell_shape_file': it["${cell_property}"]
                ]
            }
        }
        def shape_file_cells = FuzzyCSVTable.toCSV(shape_file_cells_list).sort { "$it.municipality $it.cell $it.cell_shape_file" }

        def joined_cells = shape_file_cells.leftJoin(form_cells, 'municipality', 'cell').printTable()
    }
}
