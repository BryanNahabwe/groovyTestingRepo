import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

class TestMatching {
    static void main(def args){
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "", 'com.mysql.jdbc.Driver')

        def query1 = """
                    select hhead,`__code` as code
                    from `_19_xxx_household_followup_form_`
                    """

        def query2 = """
                    select hh_name,`__code` as code
                    from moh_household
                    """

        def fuzzy1 = FuzzyCSVTable.toCSV(sql,query1)
        def fuzzy2 = FuzzyCSVTable.toCSV(sql,query2)
        def joinTables = fuzzy1.fullJoin(fuzzy2,'code').printTable()


    }
}
