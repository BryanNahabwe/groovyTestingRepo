package Check_csv

import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import com.github.kayr.phrasematcher.PhraseMatcher

class lab {
    static void main(String[] args) {
        def file1 = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\Check_csv\\concepts.csv")
        def csvFile1 = file1.text
        def list1 = FuzzyCSVTable.parseCsv(csvFile1)
        def file2 = new File("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\Check_csv\\martin.csv")
        def csvFile2 = file2.text
        def list2 = FuzzyCSVTable.parseCsv(csvFile2)

        def matcher = PhraseMatcher.train(list1['name'])

        def joinedTables = list1.join(list2) {
            matcher.compare(it.left('name'), it.right('martin_name')) >= 90
        }.addColumn(RecordFx.fx('%') {
            matcher.compare(it['name'], it['martin_name'])
        }).select('name', 'martin_name', '%')
                .sort('%')
                .write("D:\\trials\\com.groovyTestingRepo\\src\\main\\groovy\\Check_csv\\bryan.csv")
        joinedTables.printTable()
    }
}
