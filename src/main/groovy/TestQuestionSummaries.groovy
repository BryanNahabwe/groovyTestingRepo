import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.sql.Sql

class TestQuestionSummaries {
    static void main(def args){
        Sql SQL = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.jdbc.Driver')

        def query = """
                        SELECT 'vals'                                                                                           as 'Values',
                               COUNT(
                                   CASE WHEN `type_of_buyer_rice` like 'other_institutional_buyers' THEN 1 END)                 as `Other institutional buyers`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'large_farmer' THEN 1 END)                             as `Large Farmer`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'seed_company' THEN 1 END)                             as `Seed Company`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'ngo' THEN 1 END)                                      as `NGO`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'research_naro' THEN 1 END)                            as `Research (NARO)`,
                               COUNT(CASE
                                       WHEN `type_of_buyer_rice` like 'education_institute_school_or_university'
                                         THEN 1 END)                                                                            as `Education institute (school or university)`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'processor' THEN 1 END)                                as `Processor`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'naads_slash_owc' THEN 1 END)                          as `NAADS/OWC`,
                               COUNT(CASE WHEN `type_of_buyer_rice` like 'small_farmer' THEN 1 END)                             as `Small Farmer(aggregated)`
                        from _21_sales_
                               INNER JOIN
                             _21_xxx_sales_form ON _21_sales_.openxdata_form_data_id = _21_xxx_sales_form.openxdata_form_data_id
                        """


        def chartType = 'bar'
        def fuzzy = FuzzyCSVTable.toCSV(SQL,query.toString()).printTable().toMapList()
        def result = [chartType: chartType, data: fuzzy]
        println(result)



        def query2 = """
                        SELECT 'vals'                                                             as 'Values',
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'pader' THEN 1 END)     as `Pader`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'otuke' THEN 1 END)     as `Otuke`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'ntoroko' THEN 1 END)   as `Ntoroko`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'maracha' THEN 1 END)   as `Maracha`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'bugiri' THEN 1 END)    as `Bugiri`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'kabarole' THEN 1 END)  as `Kabarole`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'nwoya' THEN 1 END)     as `Nwoya`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'apac' THEN 1 END)      as `Apac`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'kibuku' THEN 1 END)    as `Kibuku`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'tororo' THEN 1 END)    as `Tororo`,
                               COUNT(CASE WHEN `buyer_didtrict_rice` like 'lamwo' THEN 1 END)     as `Lamwo`
                        from _21_sales_
                               INNER JOIN _21_xxx_sales_form ON _21_sales_.openxdata_form_data_id = _21_xxx_sales_form.openxdata_form_data_id
                        """

        def fuzzy2 = FuzzyCSVTable.toCSV(SQL,query2.toString()).toMapList()
        def json2 = new JsonBuilder(fuzzy2)
        println JsonOutput.prettyPrint(json2.toString())

        def outputFile2 = 'questionsummaries.json'
        def fileWriter2 = new FileWriter(outputFile2)
        json2.writeTo(fileWriter2)
        fileWriter2.flush() /* to push the data from  buffer to file */
        println "Question Summaries details are written into the file '${outputFile2}'"
    }
}
