import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql
import static fuzzycsv.FuzzyStaticApi.*

class TestBulletGraphsData {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryTargetData = """
                                select indicator, target, year, overall_target, project_year_target
                                from craft_indicators_target_data
                                """

        def queryTotalPpln = """
                            select count(distinct farmer_id) as total_reached
                            from craft_farmers_reached
                            where name_partner != '';
                            """

        def querySampledData = """
                                select indicator,
                                       period,
                                       total_indicator,
                                       total_surveyed
                                from craft_indicators_sampled_data
                                """


        def running_sum = 0
        def targetDataToFuzzy = FuzzyCSVTable.toCSV(sql, queryTargetData).printTable()
        def groupByIndicator = targetDataToFuzzy.sort { it.year }.autoAggregate('indicator',
                reduce { group -> group['year'] }.az('year'),
                reduce { group -> group['project_year_target'] }.az('ranges'),
                reduce { group -> group['overall_target'] }.az('markers')).toMapList()
        def targetDataRemoveNulls = groupByIndicator.collect {
            def ranges = []
            ((it.ranges) as List).findAll { it != null }.eachWithIndex{ def entry, int i ->
                if (i == 0){
                    running_sum = entry
                    ranges << running_sum
                }
                else {
                    running_sum += entry
                    ranges << running_sum
                }
            }
            [
                    'indicator': it.indicator,
                    'year'     : ((it.year) as List).findAll { it != null }.unique { a, b -> a <=> b },
                    'ranges'   : ranges,
                    'markers'  : ((it.markers) as List).findAll { it != null },
            ]
        }
        def targetData = FuzzyCSVTable.toCSV(targetDataRemoveNulls).printTable()
        def totalPpln = FuzzyCSVTable.toCSV(sql, queryTotalPpln).sum { it.total_reached }
        def sampledDataFuzzy = FuzzyCSVTable.toCSV(sql, querySampledData)
                .autoAggregate('indicator', 'period',
                        sum('total_indicator').az('total_indicator'),
                        sum('total_surveyed').az('total_surveyed'))
                .addColumn(fx('total_value') {
                    return (it.total_indicator / it.total_surveyed)
                }).delete('total_indicator', 'total_surveyed')
        def filterBaselines = sampledDataFuzzy.filter { it.period == 'baseline' }.delete('period').renameHeader('total_value', 'baseline_total')
        def filterOutBaselines = sampledDataFuzzy.filter { it.period != 'baseline' }
                .autoAggregate('indicator', sum('total_value').az('review_total'))
                .delete('period')
                .join(filterBaselines, 'indicator')
                .addColumn(fx('total') {
                    def cal = (it.review_total - it.baseline_total) * totalPpln
                    return Math.abs(cal)
                })
        def sampledData = filterOutBaselines.autoAggregate('indicator',
                reduce { group -> group['total'] }.az('measures'))
                .join(targetData, 'indicator')

    }
}
