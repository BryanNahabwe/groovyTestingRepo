import junit.framework.TestCase;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

public class WorkingTimeTest extends TestCase {

    public void testConvertHoursToDays() throws Exception{
        WorkingTime time = new WorkingTime(8, 0, 17, 0);
        Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-09 08:00:00.0");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-13 17:00:00.0");
        long workingTime = time.calculateWorkingAsDays(startDate, endDate, Collections.emptyList());
        assertEquals(42, workingTime);
    }

    public void testConvertHoursToDaysWithWeekends() throws Exception{
        WorkingTime time = new WorkingTime(8, 0, 17, 0);
        Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-09 08:00:00.0");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-19 17:00:00.0");
        long workingTime = time.calculateWorkingAsDays(startDate, endDate, Collections.emptyList());
        assertEquals(81, workingTime);
    }
    public void testEstimatedTaskCompletionDate() throws Exception {
        WorkingTime time = new WorkingTime(8, 0, 17, 0);
        Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-09 08:00:00.0");
        long days = 2;
        Date actualEstimatedEndDate = time.getEstimatedCompletionDate(Collections.emptyList(), startDate, days);
        Date expectedCompletionDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse("2019-12-11 17:00:00.0");
        assertEquals(expectedCompletionDate,actualEstimatedEndDate);
    }

}
