package SNVTide2

import groovy.util.logging.Log4j
import org.omni.rest.odkcentral.CentralRestClient

/**
 * Created by kay on 4/18/2015.
 */
@Log4j
class RestHelper {

    static CentralRestClient getCentralRestClient() {
        def url = "https://central.omnitech.co.ug"
        def username = "admin@omnitech.co.ug"
        def password = "omnitech123"
        def centralRestClient = new CentralRestClient(username, password,url)
        return centralRestClient

    }

    static def <T> T withCentral(@DelegatesTo(CentralRestClient) Closure<T> code) {
        def rc = getCentralRestClient()
        code.delegate = rc
        return code()

    }


}
