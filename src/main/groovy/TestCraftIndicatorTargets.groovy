import fuzzycsv.FuzzyCSVTable
import fuzzycsv.RecordFx
import groovy.sql.Sql

import static fuzzycsv.RecordFx.*

class TestCraftIndicatorTargets {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')
        def queryTargetData = """
                        select pt.target,
                               pt.year as year_id,
                               si.project_indicators as indicator_id,
                               si.overall_target,
                               si.project_year_target
                        from _413_xxx_project_targets pt
                                 inner join _413_select_the_indicator si on pt.Id = si.parentId
                        """

        def optionTextIndicator = """
                                    select option_id as indicator_id, option_text as indicator from _413_project_indicators group by option_id
                                    """
        def optionTextYear = """
                                    select option_id as year_id, option_text as year from _413_year group by option_id
                                    """
        def targetData = FuzzyCSVTable.toCSV(sql, queryTargetData)
        def optionTextIndicatorFuzzy = FuzzyCSVTable.toCSV(sql, optionTextIndicator)
        def optionTextYearFuzzy = FuzzyCSVTable.toCSV(sql, optionTextYear)
        def runningtotal = 0
        def clearTargetTables = targetData.join(optionTextIndicatorFuzzy, 'indicator_id')
                .join(optionTextYearFuzzy, 'year_id')
                .filter { it.indicator_id == 'indicator3' }
                .addColumn(fx ('given_name') {
                    return "Farmers whose farming enterprise became more resilient to possible stresses and/or shocks  and have lost less than 50% of the crop"
                }).delete('year_id').printTable()

        def filterOutOverallTarget = clearTargetTables.filter { it.target == 'overall' }.toMapList().get(0).overall_target
        def filterOutAnnual = clearTargetTables.filter { it.target == 'annual' }

        def pivotTargetTables = filterOutAnnual.pivot('year','project_year_target', 'indicator')
                .addColumn(fx ('given_name') {
                    return "Farmers whose farming enterprise became more resilient to possible stresses and/or shocks  and have lost less than 50% of the crop"
                })
                .addColumn(fx ('overall') {
                    return filterOutOverallTarget
                }).printTable()

    }
}
