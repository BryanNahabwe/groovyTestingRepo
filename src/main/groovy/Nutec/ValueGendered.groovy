package Nutec

import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import java.text.DecimalFormat

import static fuzzycsv.FuzzyStaticApi.*
import static fuzzycsv.RecordFx.fx

class ValueGendered {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/nutecmis', 'root', "", 'com.mysql.cj.jdbc.Driver')


        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Value of gendered  investment plans developed in collaboration with supported businesses"
        })


        def query = """
                    select
                           intervention as inter,
                           partner,
                           district,
                           substring_index(year, '_', -1)          as year,
                           value_ as total
                    from `_182_xxx_lograme_trakers_form`
                    """


        def query2 = """
select distinct option_id  as inter ,
                case
                  when option_text = 'Expanding the Village Agent Model in NU' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'expanding the village agent model in nu' then 'Expanding the Village Agent Model in Northern Uganda'
                  when option_text = 'M4R Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when option_text = 'organic producer out hyphen grower scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'Organic Producer Out-grower Scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when option_text = 'private provision of on hyphen farm storage' then 'Private Provision of On-Farm Storage'
                  when option_text = 'stevia production for increased employment opportunities especially for women in northern Uganda' then 'Stevia Women Out grower Pilot'
                  when option_text = 'M4R Cassava' then 'M4R Commercial cassava production'
                  when option_text = 'M4R Offtake' then 'M4R Seed and Offtake'
                  when option_text = 'M4R Seed' then 'M4R Seed and Offtake'
                  else option_text end as intervention
from `_182_intervention`
where option_text is not null
union all
select distinct option_id  as inter ,
                case
                  when substring_index(option_text, ' (', 1) like 'Expanding the Village Agent Model in NU' then 'Expanding the Village Agent Model in Northern Uganda'
                  when substring_index(option_text, ' (', 1) like 'expanding the village agent model in nu' then 'Expanding the Village Agent Model in Northern Uganda'
                  when substring_index(option_text, ' (', 1) like 'M4R Improving uptake and usage of crop specific inorganic fertilizers' then 'Improving uptake and usage of crop specific inorganic fertilisers'
                  when substring_index(option_text, ' (', 1) like 'organic producer out hyphen grower scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when substring_index(option_text, ' (', 1) like 'Organic Producer Out-grower Scheme' then 'M4R Organic Producer Out-grower Scheme'
                  when substring_index(option_text, ' (', 1) like 'private provision of on hyphen farm storage' then 'Private Provision of On-Farm Storage'
                  when substring_index(option_text, ' (', 1) like 'stevia production for increased employment opportunities especially for women in northern Uganda' then 'Stevia Women Out grower Pilot'
                  when substring_index(option_text, ' (', 1) like 'M4R Cassava' then 'M4R Commercial cassava production'
                  when substring_index(option_text, ' (', 1) like 'M4R Offtake' then 'M4R Seed and Offtake'
                  when substring_index(option_text, ' (', 1) like 'M4R Seed' then 'M4R Seed and Offtake'
                  else substring_index(option_text, ' (', 1) end as intervention
from `_185_intervention`
where option_text is not null
                        """


        def query3 = """
                    select name_business as partner,
                           district,
                           intervention as inter,
                           date_format(date_onboarded,'%Y')                                         as year,
                           invest_plan as total
                    from `_185_xxx_agribusiness_partner`
                    order by name_business
                    """

        def fuzzyData = FuzzyCSVTable.toCSV(sql,query.toString())
        def fuzzyData2 = FuzzyCSVTable.toCSV(sql,query2).copy().appendEmptyRecord().modify { set {it.inter='M4R Offtake'; it.intervention='M4R Seed and Offtake'}; where {it.inter in [null]}}

        def joinTables = fuzzyData.join(fuzzyData2,'inter').printTable()


    }
}
