package ISSD
import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.count
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class Areaplanted {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/issdmis', 'root', "", 'com.mysql.cj.jdbc.Driver')

        def csvData = new ReadCsvTargetFile()
        def targetData = csvData.getTargetsTable().filter({
            it.Indicator == "Additional agricultural production of grain equivalent as a result of using quality seed (MT)"
        })

        def query = """
                    select
                      p.name as name,
                      p.`__code` as code,
                      lpf.zone,
                      p.district,
                      replace(p.season,'_','') as season,
                      substring(p.season,2,4)              as year,
                      p.crop as crop,
                      replace(concat_ws('', f.variety_rice, f.variety_beans, f.variety_climb, f.variety_groundnut, f.variety_cassava,
                                        f.variety_simsim, f.variety_soybean,
                                        f.variety_sorghum, f.variety_irish_potato, f.variety_maize, f.variety_sweet_potato, f.variety_finger_millet,
                                        f.variety_green_gram, f.variety_pasture,
                                        f.variety_cowpea, f.variety_pigeon),'_',' ') as variety,
                      case p.crop
                        when 'cassava' then f.sell_prod_bag*40
                        when 'irishpotato' then f.sell_prod_bag*80
                        when 'groundnut' then f.sell_prod_bag*42
                        when 'sweetpotato' then f.sell_prod_bag*30
                        else f.sell_prod_kg
                        end as quantity
                    from `_21_xxx_sales_form` p
                           inner join `_21_sales_` f on p.Id = f.parentId
                           inner join `_3_xxx_lsb_profile_form` lpf on p.`__code` = lpf.`__code`
                    union all
                    select hdif.name,
                           hdif.`__code`                                       as code,
                           lpf.zone,
                           lpf.district,
                           lower(replace(hdif.sales_season, '_', ''))          as season,
                           substring(hdif.sales_season, 1, 4)                  as year,
                           case
                                  when lower(crop) like '%pigeon pea%' then 'pigeon'
                                  when lower(crop) like '%finger%' then 'fingermillet'
                                  when lower(crop) = 'potato' then 'irishpotato'
                                  else lower(crop)
                                  end as crop,
                           lower(replace(concat_ws('', variety_rice, case
                                                                            when lower(variety_beans) like '%naro bean%'
                                                                                   then replace(lower(variety_beans), 'naro bean', 'narobean')
                                                                            else lower(variety_beans) end,
                                                   variety_groundnut, variety_cassava,
                                                   variety_simsim, variety_soybean, variety_sorghum, variety_irish_potato,
                                                   substring_index(variety_sweet_potato, '(', 1), variety_finger_millet, variety_green_gram, variety_pasture,
                                                   variety_cowpea), '_', ' ')) as variety,
                           ifnull(sold, 0)                                     as quantity
                    from `_59_xxx_historical_data_import_form` hdif
                                inner join (select distinct `__code`, zone, district from `_3_xxx_lsb_profile_form`) lpf
                                           on lpf.`__code` = hdif.`__code`
                    where sales_season like '%2017%' or sales_season like '%2018%'
                    """


        def query2 = """
select ch.crop as crop,
       (case
               when ch.crop = 'rice' then rice_seedrate
               when ch.crop = 'beans' then bean_seedrate
               when ch.crop = 'bush' then bean_seedrate
               when ch.crop = 'climb' then bean_seedrate
               when ch.crop = 'groundnut' then groundnut_seedrate
               when ch.crop = 'cassava' then cassava_seedrate
               when ch.crop = 'simsim' then simsim_seedrate
               when ch.crop = 'soybean' then soybean_seedrate
               when ch.crop = 'sorghum' then sorghum_seedrate
               when ch.crop = 'irishpotato' then irishpotato_seedrate
               when ch.crop = 'sweetpotato' then sweetpotato_seedrate
               when ch.crop = 'fingermillet' then millet_seedrate
               when ch.crop = 'greengram' then greengram_seedrate
               when ch.crop = 'pasture' then pasture_seedrate
               when ch.crop = 'cowpea' then cowpea_seedrate
               when ch.crop = 'pigeon' then pigeon_seedrate
              end   ) as seedrate,
       (case
               when ch.crop = 'rice' then rice_equivalent
               when ch.crop = 'beans' then beans_equivalent
               when ch.crop = 'bush' then beans_equivalent
               when ch.crop = 'climb' then beans_equivalent
               when ch.crop = 'groundnut' then groundnut_equivalent
               when ch.crop = 'cassava' then cassava_equivalent
               when ch.crop = 'simsim' then simsim_equivalent
               when ch.crop = 'soybean' then soybean_equivalent
               when ch.crop = 'sorghum' then sorghum_equivalent
               when ch.crop = 'irishpotato' then irishpotato_equivalent
               when ch.crop = 'sweetpotato' then sweetpotato_equivalent
               when ch.crop = 'fingermillet' then millet_equivalent
               when ch.crop = 'pasture' then 0
               when ch.crop = 'greengram' then greengram_equivalent
               when ch.crop = 'cowpea' then cowpea_equivalent
               when ch.crop = 'pigeon' then pigeon_equivalent
              end   ) as cereal_eq,
       (case
               when ch.crop = 'rice' then 0.1
               when ch.crop = 'beans' then 0.3
               when ch.crop = 'bush' then 0.3
               when ch.crop = 'climb' then 0.3
               when ch.crop = 'groundnut' then 0.35
               when ch.crop = 'cassava' then 4.2
               when ch.crop = 'simsim' then 0.35
               when ch.crop = 'soybean' then 0.35
               when ch.crop = 'sorghum' then 0.1
               when ch.crop = 'irishpotato' then 4.2
               when ch.crop = 'sweetpotato' then 4.2
               when ch.crop = 'fingermillet' then 0.1
               when ch.crop = 'greengram' then 0.3
               when ch.crop = 'cowpea' then 0.3
               when ch.crop = 'pigeon' then 0.3
               when ch.crop = 'pasture' then 0
              end   ) as yield_diff
from `_54_xxx_logframe_indicators_form` p
            inner join (select id, SUBSTRING_INDEX(SUBSTRING_INDEX(`_54_xxx_logframe_indicators_form`.crop, ' ', numbers.n),
                                                   ' ', -1) as crop
                        from (select 1 n
                              union all
                              select 2
                              union all select 3
                              union all
                              select 4
                              union all select 5
                              union all select 6
                              union all select 7
                              union all select 8
                              union all select 9
                              union all select 10
                              union all select 11
                              union all select 12
                              union all select 13
                              union all select 14
                              union all select 15
                              union all select 16) numbers
                                    INNER JOIN `_54_xxx_logframe_indicators_form` on CHAR_LENGTH(`_54_xxx_logframe_indicators_form`.crop)
                                                                                            -
                                                                                     CHAR_LENGTH(REPLACE(`_54_xxx_logframe_indicators_form`.crop, ' ', '')) >=
                                                                                     numbers.n - 1
                        order by crop, n) ch on p.Id = ch.Id
                        
                        """

        def fuuzyData = FuzzyCSVTable.toCSV(sql, query)
        def fuzzyData1 = FuzzyCSVTable.toCSV(sql, query2).printTable()
        def joinTables = fuuzyData.join(fuzzyData1,'crop')
        def calc = joinTables.addColumn(fx('total') {
            (it.quantity == null || it.quantity == 0) ? 0 : ((((it.quantity as double) / (it.seedrate as double)) * (it.yield_diff as double)) * (it.cereal_eq as double))
        })
    }
}
