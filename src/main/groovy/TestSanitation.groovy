import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

//import filterreport.Filter

class TestSanitation {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "root", 'com.mysql.jdbc.Driver')

        def queryMunicipalities = """
                    select municipality,upper(municipality) as municipal
                    from `_38_xxx_followup_form`
                    group by municipality
                    """

        def queryDivisions = """
                                select division,upper(division) as subcounty
                                from `_38_xxx_followup_form`
                                group by division
                                """

        def municipalityList = FuzzyCSVTable.toCSV(sql, queryMunicipalities).toMapList()
        def municipality_options = []
        municipality_options << new MapEntry('_NONE_', 'NONE')
        municipalityList.each {
            municipality_options << new MapEntry("${it.municipality}".toString(),"${it.municipal}".toString())
        }

        def divisionList = FuzzyCSVTable.toCSV(sql, queryDivisions).toMapList()
        def division_options = []
        division_options << new MapEntry('_NONE_', 'NONE')
        divisionList.each {
            division_options << new MapEntry("${it.division}".toString(),"${it.subcounty}".toString())
        }

        println(division_options)
        println(municipality_options)

//        return [ new Filter(fieldName: 'filter|municipality', fieldDescription: '''Municipality''', possibleValues:  municipality_options, fieldType: Map.Entry),
//                 new Filter(fieldName: 'filter|division', fieldDescription: '''division''', possibleValues:  division_options, fieldType: Map.Entry) ]

        def toilet_type_OPTIONS = [new MapEntry('_NONE_', 'NONE'), new MapEntry("eco_san", "Eco-san"),
                                   new MapEntry("pit_latrine", "Unlined pit latrine without washable slab"),
                                   new MapEntry("pour_flush", "Flush toilet - Pour flush"),
                                   new MapEntry("lined", "lined pit latrine with washable slab"),
                                   new MapEntry("unlined", "Unlined pit latrine with washable slab"),
                                   new MapEntry("flush", "Flush toilet - Water closet")]

        println(toilet_type_OPTIONS)
    }
}
