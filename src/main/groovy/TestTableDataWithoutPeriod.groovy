import fuzzycsv.FuzzyCSVTable
import groovy.sql.Sql

import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.FuzzyStaticApi.sum
import static fuzzycsv.RecordFx.fx

class TestTableDataWithoutPeriod {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/snvmis', 'root', "root", 'com.mysql.cj.jdbc.Driver')

        def queryTableData = """
                            select indicator,
                                   target,
                                   concat('Target ', year) as year,
                                   year as target_year,
                                   project_year_target,
                                   overall_target
                            from craft_indicators_target_data
                            """

        def querySampledWithoutPeriodsData = """
                                            select indicator,
                                                   year,
                                                   business_champion,
                                                   country,
                                                   total_indicator
                                            from craft_indicators_without_periods_sampled_data
                                            """

        def targetDataFuzzy = FuzzyCSVTable.toCSV(sql, queryTableData)
        def sampleDataWithoutPeriodsFuzzy = FuzzyCSVTable.toCSV(sql, querySampledWithoutPeriodsData)


        def filterOveralls = targetDataFuzzy.filter { it.target == 'overall' }
                .delete('year', 'project_year_target', 'target_year')
                .renameHeader('target', 'year')
                .renameHeader('overall_target', 'target')
        def filterAnnuals = targetDataFuzzy.filter { it.target == 'annual' }
                .delete('overall_target', 'target', 'target_year')
                .renameHeader('project_year_target', 'target')
        def targetData = (filterAnnuals + filterOveralls).sort { it.indicator }

        def getYears = targetDataFuzzy.toMapList().collect { it.target_year }.unique()


        // Country Calculation
        def sampledDataWithoutPeriodsFuzzyCountry = sampleDataWithoutPeriodsFuzzy.autoAggregate('indicator', 'country',
                sum('total_indicator').az('actual'))
        def new_map_country_indicators_without_periods = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampleDataWithoutPeriodsFuzzy
                    .filter { it.year == each_review }
                    .autoAggregate('indicator', 'country', sum('total_indicator').az('total_indicator'))
                    .addColumn(fx('actual_' + each_review) {
                        return it.total_indicator
                    })
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_country_indicators_without_periods << it
            }
        }

        def my_map_country_indicators_without_periods = new_map_country_indicators_without_periods.collect {
            [
                    'indicator'  : it.indicator,
                    'country'    : it.country,
                    'actual_2020': (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021': (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022': (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023': (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024': (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_country_without_period = FuzzyCSVTable.toCSV(my_map_country_indicators_without_periods)
                .delete('period')

        def finalDataWithoutPeriodsCountry = sampledDataWithoutPeriodsFuzzyCountry
                .join(reviews_to_fuzzy_country_without_period, 'indicator', 'country')
                .fullJoin(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }
                .filter { it.country != null }
                .renameHeader('country', 'level')


        // Partner Calculation
        def sampledDataWithoutPeriodsFuzzyPartner = sampleDataWithoutPeriodsFuzzy.autoAggregate('indicator', 'business_champion',
                sum('total_indicator').az('actual'))
        def new_map_partner_indicators_without_periods = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampleDataWithoutPeriodsFuzzy
                    .filter { it.year == each_review }
                    .autoAggregate('indicator', 'business_champion',
                            sum('total_indicator').az('total_indicator'))
                    .addColumn(fx('actual_' + each_review) {
                        return it.total_indicator
                    })
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_partner_indicators_without_periods << it
            }
        }

        def my_map_partner_indicators_without_periods = new_map_partner_indicators_without_periods.collect {
            [
                    'indicator'  : it.indicator,
                    'business_champion'    : it.business_champion,
                    'actual_2020': (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021': (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022': (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023': (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024': (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_partner_without_period = FuzzyCSVTable.toCSV(my_map_partner_indicators_without_periods)
                .delete('period')

        def finalDataWithoutPeriodsPartner = sampledDataWithoutPeriodsFuzzyPartner
                .join(reviews_to_fuzzy_partner_without_period, 'indicator', 'business_champion')
                .fullJoin(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }
                .filter { it.business_champion != null }
                .renameHeader('business_champion', 'level')


        // Project Calculation
        def sampledDataWithoutPeriodsFuzzyProject = sampleDataWithoutPeriodsFuzzy
                .addColumn(fx('level') {
                    return 'Project'
                })
                .autoAggregate('indicator', 'level',
                sum('total_indicator').az('actual'))

        def new_map_project_indicators_without_periods = []

        getYears.collect {
            def each_review = it.toString()
            def actualPerYear = sampleDataWithoutPeriodsFuzzy
                    .filter { it.year == each_review }
                    .autoAggregate('indicator',
                            sum('total_indicator').az('total_indicator'))
                    .addColumn(fx('actual_' + each_review) {
                        return it.total_indicator
                    })
                    .sort { it.indicator }.toMapList()
            actualPerYear.each {
                new_map_project_indicators_without_periods << it
            }
        }

        def my_map_project_indicators_without_periods = new_map_project_indicators_without_periods.collect {
            [
                    'indicator'  : it.indicator,
                    'actual_2020': (it.actual_2020 ? it.actual_2020 : null),
                    'actual_2021': (it.actual_2021 ? it.actual_2021 : null),
                    'actual_2022': (it.actual_2022 ? it.actual_2022 : null),
                    'actual_2023': (it.actual_2023 ? it.actual_2023 : null),
                    'actual_2024': (it.actual_2024 ? it.actual_2024 : null)
            ]
        }

        def reviews_to_fuzzy_project_without_period = FuzzyCSVTable.toCSV(my_map_project_indicators_without_periods)
                .delete('period')

        def finalDataWithoutPeriodsProject = sampledDataWithoutPeriodsFuzzyProject
                .join(reviews_to_fuzzy_project_without_period, 'indicator')
                .join(targetData, 'indicator')
                .sort { it.indicator }
                .sort { it.year }

        def finalDataWithoutPeriods = (finalDataWithoutPeriodsProject + finalDataWithoutPeriodsCountry + finalDataWithoutPeriodsPartner).printTable()


    }
}
