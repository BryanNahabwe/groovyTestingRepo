import fuzzycsv.FuzzyCSVTable
import groovy.json.JsonOutput
import groovy.sql.Sql

import static fuzzycsv.RecordFx.fx
import static fuzzycsv.RecordFx.fx

class TestFuzzy {
    static void main(def args) {
        Sql sql = Sql.newInstance('jdbc:mysql://localhost:3306/mohmis', 'root', "", 'com.mysql.jdbc.Driver')



        def query1 = """select 'baseline' as period,count(latrine) as total
from `_17_xxx_baseline_survey_form`
where  latrine='no' 
UNION ALL
select  concat(year(t1.date_entry),"_Quarter_",quarter(t1.date_entry)) as period,count(*) as total
from `_19_xxx_household_followup_form_` t1
  inner join (select count(distinct `__code`)as count,`__code`,openxdata_form_data_id,date_entry,latrine_status,latrine
              from `_19_xxx_household_followup_form_`
              where latrine_status ='household_does_not_have_a_latrine' or latrine = 'household_does_not_have_a_latrine'
              group by `__code`
              order by `__code`) t2 on t1.`openxdata_form_data_id` = t2.`openxdata_form_data_id`
group by year(t1.date_entry),quarter(t1.date_entry);""".toString()

        def query2 = """select 'baseline' as period,count(latrine) as total
from `_17_xxx_baseline_survey_form`
where latrine='yes' 
UNION ALL
select  concat(year(t1.date_entry),"_Quarter_",quarter(t1.date_entry)) as period,count(*) as total
from `_19_xxx_household_followup_form_` t1
  inner join (select count(distinct `__code`)as count,`__code`,openxdata_form_data_id,date_entry,latrine_status,latrine
              from `_19_xxx_household_followup_form_`
              where latrine_status ='latrine' or latrine_status='a_latrine_is_in_the_process_of_being_constructed'
                                                or latrine = 'latrine' or latrine='a_latrine_is_in_the_process_of_being_constructed'
              group by `__code`
              order by `__code`) t2 on t1.`openxdata_form_data_id` = t2.`openxdata_form_data_id`
group by year(t1.date_entry),quarter(t1.date_entry);""".toString()

        def query3 = """select count(*) as total
from `_17_xxx_baseline_survey_form`
""".toString()

        def noLatrineData = FuzzyCSVTable.toCSV(sql, query1)
        def latrineData = FuzzyCSVTable.toCSV(sql, query2)
        def totalHH = FuzzyCSVTable.toCSV(sql, query3)

        def totalHousehold = totalHH['total'][0]
        def results = []
        def runningTotal = 0
        def data1 = []
        def data3 = []
        def data2 = []
        def no_latrine
        def latrine
        def households

        //Household with no latrines
        noLatrineData.addColumn(fx('runningTotal') {
            if (it.idx() == 1) {
                runningTotal = it.total
            } else {
                runningTotal -= it.total
            }
            return runningTotal
        })
        noLatrineData.each {
            data1 << [it.period, it.runningTotal]
        }
        data1 << [y: data1[(data1.size() - 1)][1], dataLabels: [enabled: true], marker: [enabled: true]]

        no_latrine = [name: 'No Latrine', data: data1]
        results << no_latrine

        //Household with latrines
        latrineData.addColumn(fx('runningTotal') {
            data3 << [it.period, totalHousehold]
            if (it.idx() == 1) {
                runningTotal = it.total
            } else {
                runningTotal += it.total
            }
            return runningTotal
        })
        latrineData.each {
            data2 << [it.period, it.runningTotal]
        }
        data2 << [y: data2[(data3.size() - 1)][1], dataLabels: [enabled: true], marker: [enabled: true]]

        latrine = [name: 'Latrine', data: data2]
        results << latrine

        //All household with both latrines and no latrines
        data3 <<  [y: data3[(data3.size() - 1)][1], dataLabels: [enabled: true], marker: [enabled: true]]
        households = [name: "Total HouseHolds", data: data3]
        results << households



        println(JsonOutput.toJson(results))


    }
}
